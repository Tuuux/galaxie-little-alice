import unittest


from nan.ui.nativetypes import Area_t
from nan.ui.nativetypes import Point_t
from nan.ui.nativetypes import Dim_t


class MyAreaType(unittest.TestCase):
    def setUp(self) -> None:
        self.area_type = Area_t(Point_t(0, 0), Dim_t(0, 0))
        self.dim_type = Dim_t(0, 0)
        self.point_type = Point_t(0, 0)

    def tearDown(self):
        self.area_type = None
        self.dim_type = None
        self.point_type = None

    def test_init(self):
        self.assertEqual(self.area_type.pos.x, 0)
        self.assertEqual(self.area_type.pos.y, 0)
        self.assertEqual(self.area_type.size.w, 0)
        self.assertEqual(self.area_type.size.h, 0)

        self.area_type = Area_t(Point_t(42, 42), Dim_t(42, 42))
        self.assertEqual(self.area_type.pos.x, 42)
        self.assertEqual(self.area_type.pos.y, 42)
        self.assertEqual(self.area_type.size.w, 42)
        self.assertEqual(self.area_type.size.h, 42)

        self.area_type = Area_t(Point_t(), Dim_t())
        self.assertEqual(self.area_type.pos.x, 0)
        self.assertEqual(self.area_type.pos.y, 0)
        self.assertEqual(self.area_type.size.w, 0)
        self.assertEqual(self.area_type.size.h, 0)

    def test_pos(self):
        self.assertTrue(isinstance(self.area_type, Area_t))
        self.assertTrue(isinstance(self.area_type.pos, Point_t))

        self.assertEqual(self.area_type.pos.x, 0)
        self.assertEqual(self.area_type.pos.y, 0)

        self.area_type.pos = Point_t(42, 4242)
        self.assertEqual(self.area_type.pos.x, 42)
        self.assertEqual(self.area_type.pos.y, 4242)

        self.assertRaises(TypeError, setattr, self.area_type, "pos", "hello.42")

    def test_size(self):
        self.assertTrue(isinstance(self.area_type, Area_t))
        self.assertTrue(isinstance(self.area_type.size, Dim_t))

        self.assertEqual(self.area_type.size.w, 0)
        self.assertEqual(self.area_type.size.h, 0)

        self.area_type.size = Dim_t(42, 4242)
        self.assertEqual(self.area_type.size.w, 42)
        self.assertEqual(self.area_type.size.h, 4242)

        self.assertRaises(TypeError, setattr, self.area_type, "size", "hello.42")

    def test_w(self):
        self.assertEqual(self.dim_type.w, 0)

        self.dim_type.w = 42
        self.assertEqual(self.dim_type.w, 42)

        self.assertRaises(TypeError, setattr, self.dim_type, "w", "hello.42")

    def test_h(self):
        self.assertEqual(self.dim_type.h, 0)

        self.dim_type.h = 42
        self.assertEqual(self.dim_type.h, 42)

        self.assertRaises(TypeError, setattr, self.dim_type, "h", "hello.42")

    def test_x(self):
        self.assertEqual(self.point_type.x, 0)

        self.point_type.x = 42
        self.assertEqual(self.point_type.x, 42)

        self.assertRaises(TypeError, setattr, self.point_type, "x", "hello.42")

    def test_y(self):
        self.assertEqual(self.point_type.y, 0)

        self.point_type.y = 42
        self.assertEqual(self.point_type.y, 42)

        self.assertRaises(TypeError, setattr, self.point_type, "y", "hello.42")


if __name__ == "__main__":
    unittest.main()
