import os
import unittest
from time import sleep
from nan.ui.application import Application


class Main(Application):
    def paint(self):
        sleep(1)
        self.stop()


class TestApplication(unittest.TestCase):
    def setUp(self) -> None:
        os.environ["TERM"] = "xterm"

    def test_singleton(self):
        app1 = Application()
        app2 = Application()
        self.assertEqual(app1, app2)

    def test_stop(self):
        app2 = Application()
        app2.stop()

    def test_refresh(self):
        app = Application()
        app.refresh()
        app.stop()

    def test_start(self):
        app = Main()
        try:
            app.start()
        finally:
            app.stop()

    def test_handle_resize(self):
        app = Application()
        app.handle_resize()
        app.stop()

    def test_handle_mouse(self):
        app = Application()
        app.handle_mouse((0, 67, 24, 0, 4))
        app.stop()

    def test_handle_curses(self):
        app = Application()
        app.handle_curses(113)
        app.stop()

    def test_paint(self):
        app = Application()
        app.paint()
        app.stop()


if __name__ == "__main__":
    unittest.main()
