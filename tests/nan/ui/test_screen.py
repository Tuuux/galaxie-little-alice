import unittest
import os

import sys
from io import StringIO

from nan.ui.screen import Screen

os.environ["TERMINFO"] = "/etc/terminfo"
os.environ["TERM"] = "xterm-256color"


class TestNCursesScreen(unittest.TestCase):
    def setUp(self):
        if not sys.stdout.isatty():
            sys.stdout = StringIO()
        self.screen = Screen()

    def tearDown(self) -> None:
        self.screen.close()

    def test_cbreak(self):
        self.assertTrue(self.screen.cbreak)

        self.screen.cbreak = False
        self.assertFalse(self.screen.cbreak)

        self.screen.cbreak = True
        self.assertTrue(self.screen.cbreak)

        self.screen.cbreak = None
        self.assertFalse(self.screen.cbreak)

        self.screen.cbreak = True
        self.assertTrue(self.screen.cbreak)

        self.assertRaises(TypeError, setattr, self.screen, "cbreak", 42)

    def test_echo(self):
        self.assertFalse(self.screen.echo)

        self.screen.echo = True
        self.assertTrue(self.screen.echo)

        self.screen.echo = None
        self.assertFalse(self.screen.echo)

        self.screen.echo = False
        self.assertFalse(self.screen.echo)

        self.assertRaises(TypeError, setattr, self.screen, "echo", "42")

    def test_halfdelay(self):
        self.screen.halfdelay = 42
        self.assertEqual(42, self.screen.halfdelay)

        self.screen.halfdelay = -42
        self.assertEqual(1, self.screen.halfdelay)

        self.screen.halfdelay = 420
        self.assertEqual(255, self.screen.halfdelay)

        self.screen.halfdelay = None
        self.assertIsNone(self.screen.halfdelay)

        self.assertRaises(TypeError, setattr, self.screen, "halfdelay", "Hello.42")

    def test_intrflush(self):
        self.assertFalse(self.screen.intrflush)

        self.screen.intrflush = True
        self.assertTrue(self.screen.intrflush)

        self.screen.intrflush = False
        self.assertFalse(self.screen.intrflush)

        self.screen.intrflush = None
        self.assertFalse(self.screen.intrflush)

        self.assertRaises(ValueError, setattr, self.screen, "intrflush", 42)

    def test_keypad(self):
        self.assertTrue(self.screen.keypad)

        self.screen.keypad = None
        self.assertFalse(self.screen.keypad)

        self.screen.keypad = True
        self.assertTrue(self.screen.keypad)

        self.screen.keypad = False
        self.assertFalse(self.screen.keypad)

        self.screen.keypad = True
        self.assertTrue(self.screen.keypad)

        self.assertRaises(ValueError, setattr, self.screen, "keypad", 42)

    def test_meta(self):
        self.assertTrue(self.screen.meta)

        self.screen.meta = None
        self.assertFalse(self.screen.meta)

        self.screen.meta = True
        self.assertTrue(self.screen.meta)

        self.screen.meta = False
        self.assertFalse(self.screen.meta)

        self.screen.meta = True
        self.assertTrue(self.screen.meta)

        self.assertRaises(ValueError, setattr, self.screen, "meta", 42)

    def test_nodelay(self):
        self.assertFalse(self.screen.intrflush)

        self.screen.nodelay = True
        self.assertTrue(self.screen.nodelay)

        self.screen.nodelay = False
        self.assertFalse(self.screen.nodelay)

        self.screen.nodelay = None
        self.assertFalse(self.screen.nodelay)

        self.assertRaises(ValueError, setattr, self.screen, "nodelay", 42)

    def test_raw(self):
        self.assertIsNone(self.screen.raw)

        self.screen.raw = True
        self.assertTrue(self.screen.raw)

        self.screen.raw = False
        self.assertFalse(self.screen.raw)

        self.screen.raw = None
        self.assertFalse(self.screen.raw)

        self.assertRaises(ValueError, setattr, self.screen, "raw", 42)

    def test_qiflush(self):
        self.assertIsNone(self.screen.qiflush)

        self.screen.qiflush = True
        self.assertTrue(self.screen.qiflush)

        self.screen.qiflush = False
        self.assertFalse(self.screen.qiflush)

        self.screen.qiflush = None
        self.assertFalse(self.screen.qiflush)

        self.assertRaises(ValueError, setattr, self.screen, "qiflush", 42)

    def test_timeout(self):
        self.assertIsNone(self.screen.timeout)
        self.screen.timeout = 1
        self.assertEqual(1, self.screen.timeout)

        self.screen.timeout = 0
        self.assertEqual(0, self.screen.timeout)

        self.screen.timeout = -1
        self.assertEqual(-1, self.screen.timeout)

        self.screen.timeout = None
        self.assertEqual(0, self.screen.timeout)

        self.assertRaises(TypeError, setattr, self.screen, "timeout", "Hello.42")

    def test_check_terminal(self):
        del os.environ["TERM"]

        self.assertRaises(EnvironmentError, self.screen.check_terminal)

        # xterm
        os.environ["TERM"] = "xterm"
        self.assertTrue(self.screen.check_terminal())
        del os.environ["TERM"]
        self.assertRaises(EnvironmentError, self.screen.check_terminal)

        # rvt
        os.environ["TERM"] = "konsole"
        self.assertTrue(self.screen.check_terminal())
        del os.environ["TERM"]
        self.assertRaises(EnvironmentError, self.screen.check_terminal)

        # rvt
        os.environ["TERM"] = "rxvt"
        self.assertTrue(self.screen.check_terminal())
        del os.environ["TERM"]
        self.assertRaises(EnvironmentError, self.screen.check_terminal)

        # Eterm
        os.environ["TERM"] = "Eterm"
        self.assertTrue(self.screen.check_terminal())
        del os.environ["TERM"]
        self.assertRaises(EnvironmentError, self.screen.check_terminal)

        # dtterm
        os.environ["TERM"] = "dtterm"
        self.assertTrue(self.screen.check_terminal())
        del os.environ["TERM"]
        self.assertRaises(EnvironmentError, self.screen.check_terminal)

        # screen
        os.environ["TERM"] = "screen"
        os.environ["DISPLAY"] = ":0"
        self.assertTrue(self.screen.check_terminal())
        del os.environ["TERM"]
        self.assertRaises(EnvironmentError, self.screen.check_terminal)
        os.environ["TERM"] = "screen"
        del os.environ["DISPLAY"]
        self.assertIsNone(self.screen.check_terminal())

    def test_refresh(self):
        self.screen.refresh()


if __name__ == "__main__":
    unittest.main()
