import os
import unittest
import curses
import contextlib

from nan.ui.painter import Painter
from nan.ui.nativetypes import Point_t
from nan.core.eventbus import EventBus


class TestNCursesPainter(unittest.TestCase):
    def setUp(self):
        os.environ["TERM"] = "xterm-256color"
        self.stdscr = curses.initscr()
        curses.noecho()
        with contextlib.suppress(curses.error):
            curses.nocbreak()
        curses.curs_set(0)
        self.stdscr.clear()
        self.stdscr.refresh()
        self.painter = Painter(self.stdscr, EventBus())

    def tearDown(self) -> None:
        curses.echo()

        with contextlib.suppress(curses.error):
            curses.cbreak()
        curses.curs_set(1)
        with contextlib.suppress(curses.error):
            curses.endwin()

    def test_pixel(self):
        self.painter.pixel(point=Point_t(0, 0), ch="█")
        self.painter.area.size.w = 0
        self.painter.area.size.h = 0
        self.painter.pixel(point=Point_t(0, 0), ch="█")

    def test_circle(self):
        self.painter.circle(0, 0, 3)

    def test_clear(self):
        self.painter.clear()

    def test_draw_background(self):
        self.painter.draw_background()

    def test_line(self):
        self.painter.line(
            Point_t(x=0, y=0),
            Point_t(x=1, y=1),
            "█",
            colour=None,
        )

    def test_box(self):
        text_to_display = "hello 42"
        self.painter.box(0, 0, len(text_to_display), 1, curses.color_pair(0), "dash")
        self.painter.box(0, 0, len(text_to_display), 1, curses.color_pair(0), "round")
        self.painter.box(0, 0, len(text_to_display), 1, curses.color_pair(0), "round-dash")
        self.painter.box(0, 0, len(text_to_display), 1, curses.color_pair(0), "heavy-dash")
        self.painter.box(0, 0, len(text_to_display), 1, curses.color_pair(0), "heavy")
        self.painter.box(0, 0, len(text_to_display), 1, curses.color_pair(0), "double")

    def test_text(self):
        self.painter.text(0, 0, "hello 42", 0)

    def test_resize(self):
        self.painter.resize()

    def test_refresh(self):
        self.painter.refresh()


if __name__ == "__main__":
    unittest.main()
