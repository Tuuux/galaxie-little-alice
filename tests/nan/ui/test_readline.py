import os
import unittest
from tempfile import NamedTemporaryFile

from nan.ui.nreadline import ReadLine


class TestNanUIReadLine(unittest.TestCase):
    def setUp(self):
        self.readline = ReadLine()
        self.history_path = os.path.realpath(
            os.path.join(os.path.dirname(__file__), "..", "..", "data", "nanuishell_history")
        )
        self.history_length = 100

    def tearDown(self) -> None:
        self.readline.clear_history()

    def test_read_history_file(self):
        self.readline.read_history_file(self.history_path)
        self.assertEqual(self.readline.history_list, ["help", "help help"])

    def test_add_history(self):
        self.assertEqual(self.readline.history_list, [])
        self.readline.add_history("help")
        self.readline.add_history("help")
        self.readline.add_history("help help")
        self.readline.add_history("help help")
        self.assertEqual(self.readline.history_list, ["help", "help help"])

    def test_write_history_file(self):
        with NamedTemporaryFile() as tmp_file:
            self.readline.clear_history()
            self.readline.add_history("help")
            self.readline.add_history("help")
            self.readline.add_history("help help")
            self.readline.add_history("help help")
            self.assertEqual(self.readline.history_list, ["help", "help help"])
            self.readline.write_history_file(tmp_file.name)

            self.readline.clear_history()
            self.assertEqual(self.readline.history_list, [])

            self.readline.read_history_file(tmp_file.name)
            self.assertEqual(self.readline.history_list, ["help", "help help"])

    def test_clear_history(self):
        self.readline.add_history("help")
        self.readline.add_history("help help")
        self.assertEqual(self.readline.history_list, ["help", "help help"])
        self.readline.clear_history()
        self.assertEqual(self.readline.history_list, [])


if __name__ == "__main__":
    unittest.main()
