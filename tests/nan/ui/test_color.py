import unittest

from nan.ui.colors import Color
from nan.ui.colors import ColorBase


class TestDrawingColors(unittest.TestCase):
    def setUp(self) -> None:
        self.color = Color()
        self.color_base = ColorBase()

    def test_ColorBase(self):
        self.assertRaises(ValueError, setattr, self.color_base, "R", 400)
        self.assertRaises(ValueError, setattr, self.color_base, "G", 400)
        self.assertRaises(ValueError, setattr, self.color_base, "B", 400)

        self.assertRaises(ValueError, setattr, self.color_base, "R", -400)
        self.assertRaises(ValueError, setattr, self.color_base, "G", -400)
        self.assertRaises(ValueError, setattr, self.color_base, "B", -400)

        self.assertRaises(TypeError, setattr, self.color_base, "R", "Hello42")
        self.assertRaises(TypeError, setattr, self.color_base, "G", "Hello42")
        self.assertRaises(TypeError, setattr, self.color_base, "B", "Hello42")

    def test_init(self):
        self.assertEqual(self.color.ColorBase.R, 0)
        self.assertEqual(self.color.ColorBase.G, 0)
        self.assertEqual(self.color.ColorBase.B, 0)

        self.color = Color.FromRGB(
            255,
            0,
            0,
        )
        self.assertEqual(self.color.ColorBase.R, 255)
        self.assertEqual(self.color.ColorBase.G, 0)
        self.assertEqual(self.color.ColorBase.B, 0)

    def test_FromRGB(self):
        self.color = Color.FromRGB(0, 255, 255)
        self.assertEqual(self.color.ColorBase.R, 0)
        self.assertEqual(self.color.ColorBase.G, 255)
        self.assertEqual(self.color.ColorBase.B, 255)

        self.color = Color()
        self.assertEqual(self.color.ColorBase.R, 0)
        self.assertEqual(self.color.ColorBase.G, 0)
        self.assertEqual(self.color.ColorBase.B, 0)

        self.color = Color.FromRGB(65535)
        self.assertEqual(self.color.ColorBase.R, 0)
        self.assertEqual(self.color.ColorBase.G, 255)
        self.assertEqual(self.color.ColorBase.B, 255)

        self.assertRaises(SyntaxError, Color.FromRGB, "hello")

    def test_FROMHex(self):
        # self.colors.Colors.Aqua, 0xFF00FFFF
        color = Color.FromHex("#00FFFF")

        self.assertEqual(color.ColorBase.R, 0)
        self.assertEqual(color.ColorBase.G, 255)
        self.assertEqual(color.ColorBase.B, 255)

        self.assertEqual(color.To32bitInt(), 65535)

    def test_ToHEX(self):
        self.assertEqual("#000000", Color.FromRGB(0, 0, 0).ToHEX())
        self.assertEqual("#ffffff", Color.FromRGB(255, 255, 255).ToHEX())

    def test_ToRGB(self):
        self.assertEqual((0, 0, 0), Color.FromRGB(0, 0, 0).ToRGB())
        self.assertEqual((255, 255, 255), Color.FromRGB(255, 255, 255).ToRGB())

    def test_ToNCurses(self):
        self.assertEqual((0, 0, 0), Color.FromRGB(0, 0, 0).ToNCurses())
        self.assertEqual((1000, 1000, 1000), Color.FromRGB(255, 255, 255).ToNCurses())

    def test_ToANSI16(self):
        # Black
        self.assertEqual(0, Color.FromRGB(0, 0, 0).To_ANSI16())
        # Red
        self.assertEqual(1, Color.FromRGB(170, 0, 0).To_ANSI16())
        # Green
        self.assertEqual(2, Color.FromRGB(0, 170, 0).To_ANSI16())
        # YELLOW
        self.assertEqual(3, Color.FromRGB(170, 128, 0).To_ANSI16())
        # BLUE
        self.assertEqual(4, Color.FromRGB(0, 0, 170).To_ANSI16())
        # MAGENTA
        self.assertEqual(5, Color.FromRGB(170, 0, 170).To_ANSI16())
        # CYAN
        self.assertEqual(6, Color.FromRGB(0, 170, 170).To_ANSI16())
        # WHITE
        self.assertEqual(7, Color.FromRGB(170, 170, 170).To_ANSI16())

        # BRIGHT
        self.assertEqual(0, Color.FromRGB(0, 0, 0).To_ANSI16())
        # RED BRIGHT
        self.assertEqual(1, Color.FromRGB(255, 85, 85).To_ANSI16())
        # GREEN BRIGHT
        self.assertEqual(2, Color.FromRGB(85, 255, 85).To_ANSI16())
        # YELLOW BRIGHT
        self.assertEqual(3, Color.FromRGB(255, 255, 85).To_ANSI16())
        # BLUE BRIGHT
        self.assertEqual(4, Color.FromRGB(85, 85, 255).To_ANSI16())
        # MAGENTA BRIGHT
        self.assertEqual(5, Color.FromRGB(255, 85, 255).To_ANSI16())
        # CYAN BRIGHT
        self.assertEqual(6, Color.FromRGB(85, 255, 255).To_ANSI16())
        # WHITE BRIGHT
        self.assertEqual(7, Color.FromRGB(255, 255, 255).To_ANSI16())


if __name__ == "__main__":
    unittest.main()
