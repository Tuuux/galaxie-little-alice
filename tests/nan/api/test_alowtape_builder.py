import unittest
import os
import shutil
import tarfile
from flask import Flask
from tempfile import NamedTemporaryFile
from nan.api.alowtape import AlowTapeBuilder


class TestAlowTape(unittest.TestCase):
    def setUp(self) -> None:
        self.alowtape_builder = AlowTapeBuilder()
        self.app = Flask(__name__)
        self.alowtape_file = os.path.join(os.path.dirname(__file__), "..", "..", "data", "toto.tar")
        self.meta_file = os.path.join(os.path.dirname(__file__), "..", "..", "data", "ALOWTAPE-INFO")
        self.bad_meta_file_1 = os.path.join(os.path.dirname(__file__), "..", "..", "data", "BAD-ALOWTAPE-INFO-1")
        self.bad_meta_file_2 = os.path.join(os.path.dirname(__file__), "..", "..", "data", "BAD-ALOWTAPE-INFO-2")
        self.data_directory = os.path.join(os.path.dirname(__file__), "..", "..", "data")

    def test_add(self):
        entries_numbers = None
        with self.app.app_context(), NamedTemporaryFile(delete=False) as alowtape_tmp_file:
            try:
                # close the temporary fiel for permit to write inside
                alowtape_tmp_file.close()
                # create a copy of the tested alowtape
                shutil.copy2(self.alowtape_file, alowtape_tmp_file.name)
                # keep memory about how many entries are all ready contain inside alowtape
                entries_numbers = len(tarfile.open(alowtape_tmp_file.name).getmembers())

                # Add one time __file__
                self.alowtape_builder.add(alowtape=alowtape_tmp_file.name, sources=[__file__])

                # Add a second time, it should not be added
                alowtape_add_return = self.alowtape_builder.add(alowtape=alowtape_tmp_file.name, sources=[__file__])
                alowtape_add_return_json = alowtape_add_return[0].json
                alowtape_add_return_code = alowtape_add_return[1]

                # __file__ have been added only one time
                self.assertEqual(entries_numbers + 1, len(tarfile.open(alowtape_tmp_file.name).getmembers()))

                # Verify keys name
                for key_name in [
                    "added",
                    "query_time",
                    "when",
                    "message",
                ]:
                    self.assertTrue(key_name in alowtape_add_return_json)
                self.assertEqual(alowtape_add_return_code, 200)

                # Errors
                alowtape_add_return = self.alowtape_builder.add(alowtape="")
                alowtape_add_return_json = alowtape_add_return[0].json
                alowtape_add_return_code = alowtape_add_return[1]
                self.assertEqual(alowtape_add_return_code, 403)

                alowtape_add_return = self.alowtape_builder.add(alowtape="not_existant")
                alowtape_add_return_json = alowtape_add_return[0].json
                alowtape_add_return_code = alowtape_add_return[1]
                self.assertEqual(alowtape_add_return_code, 404)

                alowtape_add_return = self.alowtape_builder.add(alowtape="/root/not_existant")
                alowtape_add_return_json = alowtape_add_return[0].json
                alowtape_add_return_code = alowtape_add_return[1]
                self.assertEqual(alowtape_add_return_code, 405)

            finally:
                if os.path.exists(alowtape_tmp_file.name):
                    os.remove(alowtape_tmp_file.name)

    def test_create(self):
        with self.app.app_context(), NamedTemporaryFile(delete=False) as alowtape_file_to_create:
            try:
                alowtape_file_to_create.close()
                alowtape_create_return = self.alowtape_builder.create(
                    meta=self.meta_file, alowtape=alowtape_file_to_create.name, sources=[__file__, self.data_directory]
                )
                alowtape_create_return_json = alowtape_create_return[0].json
                alowtape_create_return_code = alowtape_create_return[1]

                for key_name in [
                    "query_time",
                    "when",
                    "message",
                ]:
                    self.assertTrue(key_name in alowtape_create_return_json)
                self.assertEqual(alowtape_create_return_code, 200)
            finally:
                if os.path.exists(alowtape_file_to_create.name):
                    os.remove(alowtape_file_to_create.name)

            alowtape_create_return = self.alowtape_builder.create(meta="")
            alowtape_create_return_json = alowtape_create_return[0].json
            alowtape_create_return_code = alowtape_create_return[1]
            self.assertEqual(alowtape_create_return_code, 404)

            alowtape_create_return = self.alowtape_builder.create(meta=__file__)
            alowtape_create_return_json = alowtape_create_return[0].json
            alowtape_create_return_code = alowtape_create_return[1]
            self.assertEqual(alowtape_create_return_code, 405)

            alowtape_create_return = self.alowtape_builder.create(meta=self.bad_meta_file_1)
            alowtape_create_return_json = alowtape_create_return[0].json
            alowtape_create_return_code = alowtape_create_return[1]
            self.assertEqual(alowtape_create_return_code, 406)

            alowtape_create_return = self.alowtape_builder.create(meta=self.bad_meta_file_2)
            alowtape_create_return_json = alowtape_create_return[0].json
            alowtape_create_return_code = alowtape_create_return[1]
            self.assertEqual(alowtape_create_return_code, 407)

    def test_destroy(self):
        with self.app.app_context(), NamedTemporaryFile(delete=False) as alowtape_tmp_file:
            try:
                alowtape_tmp_file.close()
                shutil.copy2(self.alowtape_file, alowtape_tmp_file.name)

                alowtape_destroy_return = self.alowtape_builder.destroy(alowtape=alowtape_tmp_file.name)
                alowtape_destroy_return_json = alowtape_destroy_return[0].json
                alowtape_destroy_return_code = alowtape_destroy_return[1]

                for key_name in [
                    "query_time",
                    "when",
                    "message",
                ]:
                    self.assertTrue(key_name in alowtape_destroy_return_json)
                self.assertEqual(alowtape_destroy_return_code, 200)

                alowtape_destroy_return = self.alowtape_builder.destroy(alowtape=__file__)
                alowtape_destroy_return_json = alowtape_destroy_return[0].json
                alowtape_destroy_return_code = alowtape_destroy_return[1]
                self.assertEqual(alowtape_destroy_return_code, 403)

                alowtape_destroy_return = self.alowtape_builder.destroy(alowtape="not_")
                alowtape_destroy_return_json = alowtape_destroy_return[0].json
                alowtape_destroy_return_code = alowtape_destroy_return[1]
                self.assertEqual(alowtape_destroy_return_code, 404)

                alowtape_destroy_return = self.alowtape_builder.destroy(alowtape=42)
                alowtape_destroy_return_json = alowtape_destroy_return[0].json
                alowtape_destroy_return_code = alowtape_destroy_return[1]
                self.assertEqual(alowtape_destroy_return_code, 405)

            finally:
                if os.path.exists(alowtape_tmp_file.name):
                    os.remove(alowtape_tmp_file.name)

    def test_remove(self):
        entries_numbers = None
        with self.app.app_context(), NamedTemporaryFile(delete=False) as alowtape_tmp_file:
            try:
                alowtape_tmp_file.close()
                shutil.copy2(self.alowtape_file, alowtape_tmp_file.name)
                # keep memory about how many entries are all ready contain inside alowtape
                entries_numbers = len(tarfile.open(alowtape_tmp_file.name).getmembers())

                alowtape_remove_return = self.alowtape_builder.remove(
                    alowtape=alowtape_tmp_file.name, sources=["BAD-ALOWTAPE-INFO-1"]
                )
                alowtape_remove_return_json = alowtape_remove_return[0].json
                alowtape_remove_return_code = alowtape_remove_return[1]

                self.assertEqual(entries_numbers - 1, len(tarfile.open(alowtape_tmp_file.name).getmembers()))

                for key_name in [
                    "query_time",
                    "when",
                    "message",
                ]:
                    self.assertTrue(key_name in alowtape_remove_return_json)
                self.assertEqual(alowtape_remove_return_code, 200)
                self.assertEqual(
                    alowtape_remove_return_json["files_removed"][0], "remove BAD-ALOWTAPE-INFO-1 size 15.00B"
                )

                alowtape_remove_return = self.alowtape_builder.remove(alowtape="")
                alowtape_remove_return_json = alowtape_remove_return[0].json
                alowtape_remove_return_code = alowtape_remove_return[1]
                self.assertEqual(alowtape_remove_return_code, 403)

                alowtape_remove_return = self.alowtape_builder.remove(alowtape="not_existant")
                alowtape_remove_return_json = alowtape_remove_return[0].json
                alowtape_remove_return_code = alowtape_remove_return[1]
                self.assertEqual(alowtape_remove_return_code, 404)

                alowtape_remove_return = self.alowtape_builder.remove(alowtape="/root/not_existant")
                alowtape_remove_return_json = alowtape_remove_return[0].json
                alowtape_remove_return_code = alowtape_remove_return[1]
                self.assertEqual(alowtape_remove_return_code, 405)

            finally:
                if os.path.exists(alowtape_tmp_file.name):
                    os.remove(alowtape_tmp_file.name)


if __name__ == "__main__":
    unittest.main()
