import unittest
import os
import shutil
import tarfile
from flask import Flask
from tempfile import NamedTemporaryFile
from nan.api.alowtape import AlowTapeManager


class TestAlowTape(unittest.TestCase):
    def setUp(self) -> None:
        self.alowtape_builder = AlowTapeManager()
        self.app = Flask(__name__)
        self.alowtape_file = os.path.join(os.path.dirname(__file__), "..", "..", "data", "toto.tar")
        self.meta_file = os.path.join(os.path.dirname(__file__), "..", "..", "data", "ALOWTAPE-INFO")
        self.bad_meta_file_1 = os.path.join(os.path.dirname(__file__), "..", "..", "data", "BAD-ALOWTAPE-INFO-1")
        self.bad_meta_file_2 = os.path.join(os.path.dirname(__file__), "..", "..", "data", "BAD-ALOWTAPE-INFO-2")
        self.data_directory = os.path.join(os.path.dirname(__file__), "..", "..", "data")

    def test_info(self):
        with self.app.app_context():
            alowtape_info_return = self.alowtape_builder.info(alowtape=self.alowtape_file)
            alowtape_info_return_json = alowtape_info_return[0].json
            alowtape_info_return_code = alowtape_info_return[1]

            for key_name in [
                "alowtape_info",
                "alowtape_stats",
                "alowtape_members",
                "query_time",
                "when",
                "message",
            ]:
                self.assertTrue(key_name in alowtape_info_return_json)
            self.assertEqual(alowtape_info_return_code, 200)

            alowtape_info_return = self.alowtape_builder.info(alowtape=__file__)
            alowtape_info_return_json = alowtape_info_return[0].json
            alowtape_info_return_code = alowtape_info_return[1]
            self.assertEqual(alowtape_info_return_code, 403)

            alowtape_info_return = self.alowtape_builder.info(alowtape="not_")
            alowtape_info_return_json = alowtape_info_return[0].json
            alowtape_info_return_code = alowtape_info_return[1]
            self.assertEqual(alowtape_info_return_code, 404)

            alowtape_info_return = self.alowtape_builder.info(alowtape=42)
            alowtape_info_return_json = alowtape_info_return[0].json
            alowtape_info_return_code = alowtape_info_return[1]
            self.assertEqual(alowtape_info_return_code, 405)


if __name__ == "__main__":
    unittest.main()
