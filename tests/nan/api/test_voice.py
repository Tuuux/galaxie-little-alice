import unittest
from flask import Flask

from nan.api.voice import Voice


class TestVoice(unittest.TestCase):
    def setUp(self) -> None:
        self.voice = Voice()
        self.voice.setup()
        self.app = Flask(__name__)

    def tearDown(self) -> None:
        self.voice.teardown()

    def test_speech(self):
        with self.app.app_context():
            self.voice.speech(text="Deux secondes ...")
            self.voice.speech(text="Je fais quelques éssaies")


if __name__ == "__main__":
    unittest.main()
