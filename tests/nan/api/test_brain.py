import unittest
import os
import tempfile
from flask import Flask

from nan.api.brain import Brain


class TestBrain(unittest.TestCase):
    def setUp(self) -> None:
        self.brain = Brain()
        self.app = Flask(__name__)

    def test_setup(self):
        with (
            self.app.app_context(),
            tempfile.NamedTemporaryFile() as brain_file,
            tempfile.NamedTemporaryFile() as session_file,
        ):
            self.brain.storage.brain_file = brain_file.name
            self.brain.storage.session_file = session_file.name
            self.brain.setup()

    def test_alowtape_load(self):
        with (
            self.app.app_context(),
            tempfile.NamedTemporaryFile() as brain_file,
            tempfile.NamedTemporaryFile() as session_file,
        ):
            self.brain.storage.kernel.verbose(False)
            self.brain.storage.brain_file = brain_file.name
            self.brain.storage.session_file = session_file.name

            self.assertEqual(0, self.brain.storage.kernel.numCategories())
            self.assertFalse(self.brain.storage.kernel.numCategories() > 0)

            alowtape_load_return = self.brain.alowtape_load(alowtape_name="alice")
            alowtape_load_return_json = alowtape_load_return[0].json
            alowtape_load_return_code = alowtape_load_return[1]

            for key_name in [
                "server",
                "kernel",
                "query_time",
                "num_categories",
                "learned_file",
                "when",
                "message:",
            ]:
                self.assertTrue(key_name in alowtape_load_return_json)
            self.assertEqual(alowtape_load_return_code, 200)

            self.assertTrue(self.brain.storage.kernel.numCategories() > 0)

            self.assertRaises(TypeError, self.brain.alowtape_load, 42)

    def test_alowtapes_reload(self):
        with (
            self.app.app_context(),
            tempfile.NamedTemporaryFile() as brain_file,
            tempfile.NamedTemporaryFile() as session_file,
        ):
            self.brain.storage.brain_file = brain_file.name
            self.brain.storage.session_file = session_file.name
            self.brain.brain_load()

            alowtapes_reload_return = self.brain.alowtapes_reload()
            alowtapes_reload_return_json = alowtapes_reload_return[0].json
            alowtapes_reload_return_code = alowtapes_reload_return[1]

            for key_name in [
                "server",
                "kernel",
                "query_time",
                "when",
                "message",
            ]:
                self.assertTrue(key_name in alowtapes_reload_return_json)
            self.assertEqual(alowtapes_reload_return_code, 200)

    def test_brain_load(self):
        # Inside application context
        with (
            self.app.app_context(),
            tempfile.NamedTemporaryFile() as brain_file,
        ):
            self.brain.storage.brain_file = brain_file.name

            # supress the tempraraty file before
            if os.path.exists(self.brain.storage.brain_file):
                os.remove(self.brain.storage.brain_file)

            brain_load_return = self.brain.brain_load().json

            self.assertTrue(os.path.isfile(self.brain.storage.brain_file))

            for key_name in [
                "server",
                "kernel",
                "query_time",
                "when",
                "code",
                "message",
            ]:
                self.assertTrue(key_name in brain_load_return)
            self.assertEqual(brain_load_return["code"], 200)

    def test_brain_save(self):
        with self.app.app_context(), tempfile.NamedTemporaryFile() as brain_file:
            self.brain.storage.brain_file = brain_file.name

            if os.path.exists(self.brain.storage.brain_file):
                os.remove(self.brain.storage.brain_file)

            brain_save_return = self.brain.brain_save().json

            self.assertTrue(os.path.isfile(self.brain.storage.brain_file))

            for key_name in [
                "server",
                "kernel",
                "query_time",
                "when",
                "code",
                "message:",
            ]:
                self.assertTrue(key_name in brain_save_return)

            self.assertEqual(brain_save_return["code"], 200)

    def test_session_load(self):
        with (
            self.app.app_context(),
            tempfile.NamedTemporaryFile() as session_file,
        ):
            self.brain.storage.session_file = session_file.name
            # supress the tempraraty file before
            if os.path.exists(self.brain.storage.session_file):
                os.remove(self.brain.storage.session_file)

            self.brain.storage.kernel.respond("Bonjour Alice", self.brain.storage.session_name)
            self.brain.session_save()
            session_load_return = self.brain.session_load().json

            for key_name in [
                "server",
                "kernel",
                "query_time",
                "when",
                "code",
                "message",
            ]:
                self.assertTrue(key_name in session_load_return)
            self.assertEqual(session_load_return["code"], 200)

    def test_session_save(self):
        with (
            self.app.app_context(),
            tempfile.NamedTemporaryFile() as session_file,
        ):
            self.brain.storage.session_file = session_file.name
            # supress the tempraraty file before
            if os.path.exists(self.brain.storage.session_file):
                os.remove(self.brain.storage.session_file)

            session_save_return = self.brain.session_save().json

            self.assertTrue(os.path.isfile(self.brain.storage.session_file))

            for key_name in [
                "server",
                "kernel",
                "query_time",
                "when",
                "code",
                "message",
            ]:
                self.assertTrue(key_name in session_save_return)
            self.assertEqual(session_save_return["code"], 200)

    def test_session_status(self):
        with (
            self.app.app_context(),
            tempfile.NamedTemporaryFile() as session_file,
        ):
            self.brain.storage.session_file = session_file.name
            # supress the tempraraty file before
            if os.path.exists(self.brain.storage.session_file):
                os.remove(self.brain.storage.session_file)

            self.brain.session_load()

            session_status_return = self.brain.session_status()
            session_status_return_json = session_status_return[0].json
            session_status_return_code = session_status_return[1]

            for key_name in [
                "server",
                "kernel",
                "query_time",
                "when",
                "message",
                "session_data",
            ]:
                self.assertTrue(key_name in session_status_return_json)
            self.assertEqual(session_status_return_code, 200)

    def test_ask(self):
        with (
            self.app.app_context(),
            tempfile.NamedTemporaryFile() as session_file,
            tempfile.NamedTemporaryFile() as brain_file,
        ):
            # Set brain and session files to named temporary files
            self.brain.storage.session_file = session_file.name
            self.brain.storage.brain_file = brain_file.name

            # Suppress files all ready by tempfile.NamedTemporaryFile()
            if os.path.exists(self.brain.storage.session_file):
                os.remove(self.brain.storage.session_file)
            if os.path.exists(self.brain.storage.brain_file):
                os.remove(self.brain.storage.brain_file)

            # Let the brain start from scratch
            self.brain.brain_load()
            self.brain.session_load()
            self.brain.alowtapes_reload()

            # Say hello and get return
            say_return = self.brain.ask("Bonjour")
            say_return_json = say_return[0].json
            say_return_code = say_return[1]

            # tests if each key name is present
            for key_name in [
                "server",
                "kernel",
                "query_time",
                "when",
                "code",
                "message",
                "answer",
            ]:
                self.assertTrue(key_name in say_return_json)

            # Test error code from flask
            self.assertEqual(say_return_code, 200)

            # Test error code from json object
            self.assertEqual(say_return_json["code"], 200)


if __name__ == "__main__":
    unittest.main()
