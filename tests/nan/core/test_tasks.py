import unittest

from nan.core.tasks import TaskManager

import asyncio


class TestTaskManager(unittest.TestCase):
    def setUp(self) -> None:
        self.loop = asyncio.get_event_loop()
        self.taskmanager = TaskManager(self.loop)

    def tearDown(self) -> None:
        self.loop.stop()

    def test_add(self):
        async def hello_42():
            await asyncio.sleep(0.01)

        self.taskmanager.add(hello_42, "hello_42")

    def test_cancel(self):
        async def hello_42():
            await asyncio.sleep(0.01)

        self.assertEqual(0, self.taskmanager.cancel())

        self.taskmanager.add(hello_42, "hello_42")
        self.assertEqual(1, self.taskmanager.cancel())


if __name__ == "__main__":
    unittest.main()
