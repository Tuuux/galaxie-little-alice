import unittest
from nan.core.eventbus import EventBus
from nan.core.eventbus import InvalidEventError
from nan.core.eventbus import InvalidCallbackError
from nan.core.eventbus import InvalidPriorityError


class TestEventBus(unittest.TestCase):
    def setUp(self) -> None:
        self.bus = EventBus()
        self.bus.clear()
        self.data = None

    def tearDown(self) -> None:
        self.bus.clear()

    def test_connect(self):
        def super_connect():
            pass

        self.assertEqual(self.bus.Subscribers, {})
        self.bus.connect("test-connect", super_connect, priority=42)

        self.assertTrue("test-connect" in self.bus.Subscribers)
        self.assertEqual(self.bus.Subscribers["test-connect"][0]["callback"], super_connect)
        self.assertEqual(self.bus.Subscribers["test-connect"][0]["priority"], 42)

        self.assertRaises(InvalidEventError, self.bus.connect, 42, 42)
        self.assertRaises(InvalidCallbackError, self.bus.connect, "42", 42)
        self.assertRaises(
            InvalidPriorityError,
            self.bus.connect,
            "42",
            super_connect,
            priority="42",
        )

    def test_remove(self):
        def super_remove1():
            pass

        def super_remove2():
            pass

        self.assertEqual(self.bus.Subscribers, {})
        self.bus.connect("test-remove", super_remove1, priority=42)
        self.bus.connect("test-remove", super_remove2, priority=42)

        self.assertTrue("test-remove" in self.bus.Subscribers)
        self.assertEqual(len(self.bus.Subscribers["test-remove"]), 2)
        self.assertEqual(self.bus.Subscribers["test-remove"][0]["callback"], super_remove1)
        self.assertEqual(self.bus.Subscribers["test-remove"][0]["priority"], 42)

        self.bus.remove("test-remove", super_remove1)
        self.assertTrue("test-remove" in self.bus.Subscribers)
        self.bus.remove("test-remove", super_remove2)
        self.assertFalse("test-remove" in self.bus.Subscribers)

        self.assertRaises(InvalidEventError, self.bus.remove, 42, 42)
        self.assertRaises(InvalidCallbackError, self.bus.remove, "42", 42)

    def test_clear(self):
        self.bus.clear()

        def super_close1():
            pass

        def super_close2():
            pass

        self.assertEqual(self.bus.Subscribers, {})
        self.bus.connect("test-close1", super_close1)
        self.bus.connect("test-close2", super_close2)

        self.assertTrue("test-close1" in self.bus.Subscribers)
        self.assertEqual(len(self.bus.Subscribers["test-close1"]), 1)
        self.assertTrue("test-close2" in self.bus.Subscribers)
        self.assertEqual(len(self.bus.Subscribers["test-close2"]), 1)

        self.bus.clear()
        self.assertEqual(self.bus.Subscribers, {})

    def test_emit(self):
        self.data = 40

        def super_emit1(incr):
            self.data += incr

        def super_emit2(incr):
            self.data += incr

        self.bus.connect("super-emit", super_emit1)
        self.bus.connect("super-emit", super_emit2)

        super_count = self.bus.emit("super-emit", 1)
        self.assertEqual(super_count, 2)
        self.assertEqual(self.data, 42)

        self.assertRaises(InvalidEventError, self.bus.emit, 42, 42)


if __name__ == "__main__":
    unittest.main()
