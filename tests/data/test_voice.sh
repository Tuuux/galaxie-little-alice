#!/usr/bin/env bash
TEXT="Oui,... on dirait"
PLAY_PITCH="pitch -300"
PLAY_BASS="bass +1 treble -5"
PLAY_ECHO="overdrive 2 1 echo 0.8 0.88 10 0.4"
PLAY_CHORUS="chorus 0.5 0.9 50 0.4 0.25 2 -t 60 0.32 0.4 2.3 -t 40 0.3 0.3 1.3 -s"

#PLAY_ARGV="$PLAY_BASS $PLAY_PITCH $PLAY_ECHO"
#PLAY_ARGV="$PLAY_BASS $PLAY_PITCH $PLAY_ECHO"
PLAY_ARGV=""

espeak-ng -v mb/mb-fr4 -q -s175 --pho --stdout "$TEXT" | mbrola -l 22500 -t 1.42 -f 0.93 -e /usr/share/mbrola/fr4/fr4 - -.au | play -t au - $PLAY_ARGV