.. _instalation:

============
Installation
============
You can found the Galaxie Radio Repository here:
https://gitlab.com/Tuuux/galaxie-radio

In any case it consist to copy the package GLXCurses inside you developing project directory.

Before you start, make sure that you already have installed **Python**, **pip** and **git**.

* Then clone Galaxie Radio project from Gitlab:

.. code:: bash

    git clone https://gitlab.com/Tuuux/galaxie-radio.git


It will create a folder name ``galaxie-radio`` it contain the ``GLXRadio`` package:

* Create a directory for you application project:

.. code:: bash

   mkdir ./SuperApplication


* Enter inside ``SuperApplication`` :

.. code:: bash

   cd ./SuperApplication


* Move the ``GLXRadio`` directory inside the ``SuperApplication`` folder:

.. code:: bash

   mv ../galaxie-radio/GLXRadio ./


Now you can import the GLXRadio package

.. code:: python

   #!/usr/bin/env python
   # -*- coding: utf-8 -*-
   import GLXRadio

Next Step:
----------

* Take a look on our example's files
* Enjoy ;-)
