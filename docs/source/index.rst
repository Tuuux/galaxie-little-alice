.. title:: Galaxie Little Alice, your own nanIA

==================================
Galaxie Little Alice Documentation
==================================
.. figure::  images/logo_galaxie.png
   :align:   center

Actual Homepage : https://gitlab.com/Tuuux/galaxie-little-alice

Documentation : https://galaxie-little-alice.readthedocs.io

The Project
-----------
**Galaxie Little Alice** is a free software ToolKit .

The Mission
-----------
...

Example
-------

.. code-block:: python

   #!/usr/bin/env python
   # -*- coding: utf-8 -*-

   import sys

   sys.exit(0)

Output
------
 

More examples can be found here: https://gitlab.com/Tuuux/galaxie-little-alice/tree/master/examples

Features
--------
*  nothing

Contribute
----------

- Issue Tracker: https://gitlab.com/Tuuux/galaxie-little-alice/issues
- Source Code: https://gitlab.com/Tuuux/galaxie-little-alice

Our collaboration model is the Collective Code Construction Contract (C4): https://rfc.zeromq.org/spec:22/C4/

Documentations
--------------
.. toctree::
   :maxdepth: 2

   install
   modules
   Basic-Types

License
-------
GNU GENERAL PUBLIC LICENSE Version 3


See the LICENCE_

.. _LICENCE: https://gitlab.com/Tuuux/galaxie-little-alice/blob/master/LICENSE

All contributions to the project source code ("patches") SHALL use the same license as the project.

Indices and tables
------------------
* :ref:`genindex`
* :ref:`search`