# nai-alowtape

## Notes

* alowtape is a tar
  * the first member add into tar file is name `ALOWTAPE-INFO`
  * `ALOWTAPE-INFO` can be JSON or YAML file format
  * Each keys inside `ALOWTAPE-INFO` file will be preserve
  - `ALOWTAPE-INFO` file have 5 mandatory keys:
      - **name**: `str`
      - **version**: `str`, should comply with https://semver.org/
      - **description**: `str`
      - **license**: `str`
      - **authors**: `list` of `str`
  * any added file is given to root ownership
  * any added file is set to 0640 permissions

## Usage
Alowtape information's are searching inside `ALOWTAPE-INFO` file. The file can use YAML or JSON format, every keys inside it file will be preserve. Each time you modify the `ALOWTAPE` file the `ALOWTAPE-INFO` **version** key will be increase.

* `create --meta <META>  ALOWTAPE [FILE...]`
  1) Look for a file name `ALOWTAPE-INFO` inside current working directory
  2) If the file name `ALOWTAPE-INFO` exist load it
  3) Use the `--meta <META>` syntax
  4) Read the `ALOWTAPE-INFO` fo determine alowtape informations
    - Mandatory keys:
      - **name**: `str`
      - **version**: `str`, should comply with https://semver.org/
      - **description**: `str`
      - **license**: `str`
      - **authors**: `list` of `str`
  5) Create a new `ALOWTAPE` file 
  6) Add first alowtape member named `ALOWTAPE-INFO`, whish is our meta file informations.
  7) If files and/or directory are pass as argument [FILE...], every entry will be add like that.
    - If that is a file add it relative path as alowtape member.
    - If that is a directory explore recursivlly and for each founded files add it relative path as alowtap member. 

* `add ALOWTAPE [FILE...]`
  * adds a new file if not existing inside the nai-alowtape
  * replaces an existing file corresponding to the given file path
  * create a new alowtape file with increased minor version number
  * override given alowtape file with the new alowtape file

* `remove ALOWTAPE [FILE...]`
  * remove an existing given file in the nai-alowtape
  * create a new alowtape file with increased minor version number
  * override given alowtape file with the new alowtape file

* `destroy(was delete) ALOWTAPE`
  * check given file is a alowtape
  * rm given file only if format is alowtape


* `verify ALOWTAPE`

Verify the content of a allowtape file, by check alowtape format integrity. Each time a  test faild, it exit imediatlly with a return code (rc) supp. to 0, follow by a error message from where the tests faild.

  * Verify if the `ALOWTAPE` is a tar file
  * Verify if the `ALOWTAPE` frist member name is `ALOWTAPE-INFO`
  * Verify if `ALOWTAPE-INFO` member contain mandatory keys: (`name`, `version`, `description`, `license`, `authors`)
  * Verify `ALOWTAPE` files hierachie
  * Verify `ALOWTAPE` members files with extension `.aiml` can be learn by AIML engine.
  * If each previus step are OK the return code (`rc`) will be set to `0`, if not `>0` where each trouble cause a rc += 1


* `info ALOWTAPE`
  * display the entire `ALOWTAPE-INFO` member stored inside the `ALOWTAPE`
  * display `ALOWTAPE` size
  * display `ALOWTAPE` date creation
  * display `ALOWTAPE` date modification
  * display `ALOWTAPE` date access
  * display list of `ALOWTAPE` content members (`ls -la` style)



## Inspiration

* https://docs.ansible.com/ansible/latest/dev_guide/collections_galaxy_meta.html
