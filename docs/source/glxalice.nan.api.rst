glxalice.nan.api package
========================

Submodules
----------

.. toctree::
   :maxdepth: 4

   glxalice.nan.api.basics
   glxalice.nan.api.brain
   glxalice.nan.api.chatbot
   glxalice.nan.api.ear
   glxalice.nan.api.voice

Module contents
---------------

.. automodule:: glxalice.nan.api
   :members:
   :undoc-members:
   :show-inheritance:
