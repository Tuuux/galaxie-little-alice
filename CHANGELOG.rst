--------------
NAN AI PROJECT
--------------
CHANGELOGS
----------
**0.1.1 - 20241001**
  * Refactor totally the project
  * Use asyncio Mainloop
  * Use a event bus
  * Migrate to signal event
  * Add AlowTape class

**0.1 - 20210904**
  * Fork ``aiml`` python module and integrate it inside the project for fixe time.clock() usage
  * Add SetupTools support
  * Migrate to vosk
  * Fixe mani unitary tests
  * Use JSON format under configuration file
  * Use GLXAudio python module as main audio library
  * Use GLXViewer python module to display text
  * Use GLXEveLoop python module as MainLoop and EventBus
