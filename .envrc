layout_python3

export DIRENV_TMP_DIR="${PWD}/.direnv"
export DIRENV_BIN_DIR="${DIRENV_TMP_DIR}/bin"
if [ ! -e "${DIRENV_BIN_DIR}" ]; then
    mkdir -p "${DIRENV_BIN_DIR}"
fi
export PATH="${DIRENV_BIN_DIR}:${PATH}"

D2_VERSION="0.6.7"
D2_ARCH="linux-amd64"
D2_PKG_NAME="d2-v${D2_VERSION}-${D2_ARCH}.tar.gz"
D2_PKG_URL="https://github.com/terrastruct/d2/releases/download/v${D2_VERSION}/${D2_PKG_NAME}"
D2_PKG_PATH="${DIRENV_TMP_DIR}/${D2_PKG_NAME}"
if [ ! -e "${DIRENV_BIN_DIR}/d2" ]; then
    echo "===> Getting d2:${D2_VERSION}:${D2_ARCH} (can take a while to execute)"
    curl -s -L "${D2_PKG_URL}" -o "${D2_PKG_PATH}"
    tar xzf ${D2_PKG_PATH} --directory ${DIRENV_TMP_DIR} --warning=no-unknown-keyword --exclude='._*'
    ln -s ../d2-v${D2_VERSION}/bin/d2 ${DIRENV_BIN_DIR}/d2
    chmod 700 ${DIRENV_TMP_DIR}/d2-v${D2_VERSION}/bin/d2
    rm -f ${D2_PKG_PATH}
fi

PIPER_VERSION="2023.11.14-2"
PIPER_ARCH="linux_x86_64"
PIPER_PKG_NAME="piper_${PIPER_ARCH}.tar.gz"
PIPER_PKG_URL="https://github.com/rhasspy/piper/releases/download/${PIPER_VERSION}/${PIPER_PKG_NAME}"
PIPER_PKG_PATH="${DIRENV_TMP_DIR}/${PIPER_PKG_NAME}"
if [ ! -e "${DIRENV_BIN_DIR}/piper" ]; then
    echo "===> Getting piper:${PIPER_VERSION}:${PIPER_ARCH} (can take a while to execute)"
    curl -s -L "${PIPER_PKG_URL}" -o "${PIPER_PKG_PATH}" &&\
    tar xzf ${PIPER_PKG_PATH} --directory ${DIRENV_TMP_DIR} --warning=no-unknown-keyword --exclude='._*' &&\
    mv ${DIRENV_TMP_DIR}/piper ${DIRENV_TMP_DIR}/piper-${PIPER_VERSION} &&\
    ln -s ../piper-${PIPER_VERSION}/piper ${DIRENV_BIN_DIR}/piper &&\
    rm -f ${PIPER_PKG_PATH}
fi

VOSK_MODEL_VERSION="small-fr-0.22"
VOSK_MODEL_PKG_NAME="vosk-model-${VOSK_MODEL_VERSION}.zip"
VOSK_MODEL_PKG_URL="https://alphacephei.com/vosk/models/${VOSK_MODEL_PKG_NAME}"
VOSK_MODEL_PKG_PATH="${DIRENV_TMP_DIR}/${VOSK_MODEL_PKG_NAME}"
if [ ! -e "${PWD}/nan/api/data/vosk/fr-FR/README" ]; then
    echo "===> Getting vosk-model:${VOSK_MODEL_VERSION} (can take a while to execute)"
    curl -s -L "${VOSK_MODEL_PKG_URL}" -o "${VOSK_MODEL_PKG_PATH}" &&\
    unzip ${VOSK_MODEL_PKG_PATH} -d ${DIRENV_TMP_DIR} &&\
    rm -rf ${PWD}/nan/api/data/vosk/fr-FR/* &&\
    mv ${DIRENV_TMP_DIR}/vosk-model-${VOSK_MODEL_VERSION}/* ${PWD}/nan/api/data/vosk/fr-FR/ &&\
    rm -rf ${DIRENV_TMP_DIR}/vosk-model-${VOSK_MODEL_VERSION} &&\
    rm -f ${VOSK_MODEL_PKG_PATH}
fi

