.PHONY: help
help:
	@	echo	'make clean      - Remove every created directory and restart from scratch'
	@	echo  	'make docs       - Install Sphinx and requirements, then build documentations'
	@	echo  	'make prepare    - Easy way for make everything'
	@	echo  	'make tests      - Easy way for make tests over a CI'
	@	echo  	''
	@	echo  	'sudo make install-python   - Easy way to install python3 pip and venv'

.PHONY: install-python
install-python:
	@	echo 	""
	@	echo 	"******************************* INSTALL PYTHON *********************************"
	@				apt-get update && apt install -y python3 python3-pip python3-venv portaudio19-dev

.PHONY: header
header:
	@echo 	"******************************* NAN IA MAKEFILE ********************************"
	@echo 	"HOSTNAME	`uname -n`"
	@echo 	"KERNEL RELEASE `uname -r`"
	@echo 	"KERNEL VERSION `uname -v`"
	@echo 	"PROCESSOR	`uname -m`"
	@echo 	"********************************************************************************"

.PHONY: prepare
prepare: header
	@	pip3 install -U pip --no-cache-dir --quiet &&\
		echo "[  OK  ] PIP3" || \
		echo "[FAILED] PIP3"
	@ 	pip3 install -r $(PWD)/docs/requirements.txt --no-cache-dir --quiet &&\
		echo "[  OK  ] SPHINX" || \
		echo "[FAILED] SPHINX"
	@	pip3 install --quiet -e .[tests] &&\
		echo "[  OK  ] INSTALL NAN IA AS DEVMODE" || \
		echo "[FAILED] INSTALL NAN IA AS DEVMODE"

.PHONY: tests
tests:
	@ 	echo ""
	@	echo "********************************** START TESTS *********************************"
	@	pytest

.PHONY: clean
clean: header
	@ 	echo ""
	@ 	echo "*********************************** CLEAN UP ***********************************"
	@   rm -rf $(PWD)/./venv
	@   rm -rf $(PWD)/./.direnv
	@   rm -rf ~/.cache/pip
	@   rm -rf $(PWD)/./.eggs
	@   rm -rf $(PWD)/./*.egg-info
	@   rm -rf $(PWD)/.coverage
	@   rm -rf $(PWD)/docs/build/*
	@   rm -rf $(PWD)/nan/api/data/vosk/fr-FR/*

.PHONY: docs
docs: header
	@ 	echo ""
	@ 	echo "***************************** BUILD DOCUMENTATIONS *****************************"
	@   rm -rf $(PWD)/docs/build/*
	@   cd $(PWD)/docs &&\
	    sphinx-apidoc -e -f -o source/ ../nan/ &&\
			d2 source/explanations/assets/*.d2 &&\
			make html
