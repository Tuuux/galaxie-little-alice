#!/usr/bin/env python
# -*- coding: utf-8 -*-

import getopt
import random
import sys
import subprocess
import logging

check_internet_hostname = "www.wikipedia.org"

version = "1.0"
verbose = False
check_internet_connection = False

internet_is_down = [
    "La connection internet est morte",
    "Nous sommes coupé du monde extérieur",
    "Il n y a pas de connectivitée à Internet",
    "Il me semble que la connection est morte",
    "La ligne est tombée",
]

internet_is_up = [
    "La connection à Internet est opérationelle.",
    "Tout à l'air de fonctionner normalement.",
    "Les paramètres sont normaux.",
    "Nous sommes à même de joindre le monde extérieur.",
    "Nous avons la connectivitée vers le monde extérieur",
    "Rien à signalé",
    "Wikipédia a répondu à mes solicitations.",
]


options, remainder = getopt.gnu_getopt(
    sys.argv[1:], "o:v", {"output=", "verbose", "version=", "check-internet-connection"}
)

for opt, arg in options:
    if opt in ("-o", "--output"):
        output_filename = arg
    elif opt in ("-v", "--verbose"):
        verbose = True
    elif opt == "--version":
        version = arg
    elif opt == "--check-internet-connection":
        check_internet_connection = True

# No supported by Windows


def print_check_internet_connection():


    command = [
        "ping",
        "-c",
        "1",
        f"{check_internet_hostname}",
    ]
    process = subprocess.Popen(
        command,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
    )
    output = process.communicate()[0]
    logging.debug(f"Module Ping: {output.decode()}")
    process.wait()
    status = process.returncode
    logging.debug(f"Module Ping: done")

    # and then check the response...
    if status == 0:
        print(random.choice(internet_is_up))
    else:
        print(random.choice(internet_is_down))


def main():
    if check_internet_connection:
        print_check_internet_connection()
