LittleAlice development is Cancel due to poor PocketSphinx French support.
When i have start in 2015 French support was pretty good.
In 2019 i consider it haven't any french support for PocketSphinx, i'll back in 5 or 10 years (if i can) and see if PocketSphinx consider other language.
Poor LittleAlice ...

You can use my work , as you want.

Enjoy future genration, one day the Free World will have a true Speech to text and a Text to speech connected to a AMIL Chat bot.

---
Heriosme <2019>

MBROLA (Multi-Band Re-synthesis OverLap Add)

Speech recognition engine/API support
-------------------------------------

* `CMU Sphinx <http://cmusphinx.sourceforge.net/wiki/>`__ (works offline)


International French: /speech_recognition/pocketsphinx-data/fr-FR/:

/speech_recognition/pocketsphinx-data/fr-FR/language-model.lm.bin is fr-small.lm.bin from the Sphinx French language model.
/speech_recognition/pocketsphinx-data/fr-FR/pronounciation-dictionary.dict is fr.dict from the Sphinx French language model.
/speech_recognition/pocketsphinx-data/fr-FR/acoustic-model/ contains all of the files extracted from cmusphinx-fr-5.2.tar.gz in the Sphinx French acoustic model.

To get better French recognition accuracy at the expense of higher disk space and RAM usage:
        Download fr.lm.gmp from the Sphinx French language model.
        Convert from DMP (an obselete Sphinx binary format) to ARPA format: sphinx_lm_convert -i fr.lm.gmp -o french.lm.bin.
        Replace /speech_recognition/pocketsphinx-data/fr-FR/language-model.lm.bin with french.lm.bin created in the previous step.



Requirements
------------

To use all of the functionality of the library, you should have:

* **Python** 2.7, or 3.3+ (required)
* **PocketSphinx** (required to use the Sphinx recognizer)
* **MBrola**
* **espeak-ng or espeak**
* **PyAudio**

The following requirements are optional, but can improve or extend functionality in some situations:

* If using CMU Sphinx, you may want to `install additional language packs take a look here :
<https://github.com/Uberi/speech_recognition/blob/master/reference/pocketsphinx.rst#installing-other-languages>` to support languages like International French or Mandarin Chinese.

Python
------

The first software requirement is `Python 2.7, or Python 3.3+ https://www.python.org/download/releases/` . This is required to use the library.

PyAudio (for microphone users)
------------------------------

`PyAudio <http://people.csail.mit.edu/hubert/pyaudio/#downloads>`__ is required if and only if you want to use microphone input (``Microphone``). PyAudio version 0.2.11+ is required, as earlier versions have known memory management bugs when recording from microphones in certain situations.


* On Debian-derived Linux distributions (like Ubuntu and Mint), install PyAudio using `APT <https://wiki.debian.org/Apt>`__: execute ``sudo apt-get install python-pyaudio python3-pyaudio`` in a terminal.
    * If the version in the repositories is too old, install the latest release using Pip: execute ``sudo apt-get install portaudio19-dev python-all-dev python3-all-dev && sudo pip install pyaudio`` (replace ``pip`` with ``pip3`` if using Python 3).
* On other POSIX-based systems, install the ``portaudio19-dev`` and ``python-all-dev`` (or ``python3-all-dev`` if using Python 3) packages (or their closest equivalents) using a package manager of your choice, and then install PyAudio using `Pip <https://pip.readthedocs.org/>`__: ``pip install pyaudio`` (replace ``pip`` with ``pip3`` if using Python 3).

PocketSphinx-Python
-------------------

`PocketSphinx-Python <https://github.com/bambocher/pocketsphinx-python>` is **required because iot use Sphinx recognizer** (``recognizer_instance.recognize_sphinx``).

On Linux and other POSIX systems, follow the instructions under "Building PocketSphinx-Python from source" in `Notes on using PocketSphinx <https://github.com/Uberi/speech_recognition/blob/master/reference/pocketsphinx.rst>` for installation instructions.

Note that the versions available in most package repositories are outdated and will not work with the bundled language data. Using the bundled wheel packages or building from source is recommended.

See `Notes on using PocketSphinx <https://github.com/Uberi/speech_recognition/blob/master/reference/pocketsphinx.rst>`__ for information about installing languages, compiling PocketSphinx, and building language packs from online resources. This document is also included under ``reference/pocketsphinx.rst``.
