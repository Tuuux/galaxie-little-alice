#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie LittleAlice Team, all rights reserved

import marshal
import os
import logging
import tarfile
from datetime import datetime
from attrs import define
from flask import jsonify
from aiml import Kernel
from io import StringIO
from tempfile import NamedTemporaryFile
from contextlib import redirect_stderr
from contextlib import redirect_stdout

from nan.core.eventbus import EventBus
from nan.core.xdg import XdgBaseDirectory
from nan.api.basics import os_stat_to_dict


@define
class StorageBrain:
    modules_dir: str = os.path.realpath(os.path.join(os.path.dirname(__file__), "data", "alowtapes"))
    lang: str = "fr_FR"
    brain_file: str = os.path.join(XdgBaseDirectory(resource="GLXAlice").config_path, "brains", "brain.br")
    session_file: str = os.path.join(XdgBaseDirectory(resource="GLXAlice").config_path, "brains", "session.ses")
    session_name: str = "Alice"
    alowtape_to_load: list = [
        "default",
    ]
    kernel: Kernel = Kernel()
    version: str = "0.1a"
    debug: bool = False
    run: bool = False
    pause: bool = False


class Brain:
    NANAI_ALOWTAPES_DIRECTORY = os.environ.get(
        "NANAI_ALOWTAPES_DIRECTORY", os.path.join(XdgBaseDirectory(resource="nanai").data_path, "alowtapes")
    )

    def __init__(self, **kwargs):
        self.bus: EventBus = kwargs.get("bus", EventBus())
        self.storage = kwargs.get("storage", StorageBrain())

    def setup(self) -> None:
        # enable the verbose
        self.storage.kernel.verbose(self.storage.debug)

        # create parent directory of the brain file if not exist
        if not os.path.exists(os.path.dirname(self.storage.brain_file)):
            os.makedirs(os.path.dirname(self.storage.brain_file))

        # create parent directory of the session file if not exist
        if not os.path.exists(os.path.dirname(self.storage.session_file)):
            os.makedirs(os.path.dirname(self.storage.session_file))

        # setup operation
        self.brain_load()
        self.session_load()
        if not self.storage.kernel.numCategories() > 0:
            self.alowtapes_reload()

        self.storage.run = True
        self.bus.emit("BRAIN_UP")

    def teardown(self) -> None:
        self.session_save()
        self.brain_save()
        self.storage.run = False
        self.bus.emit("BRAIN_DOWN")

    def alowtape_load(self, alowtape_name: str = None) -> tuple:
        """
        Load a alowtape by name

        :param alowtape_name: The alowtape name to load
        :type alowtape_name: str
        """
        # Default vars
        start_time = datetime.now()
        message = None
        code = 200
        learned_file = []

        if not isinstance(alowtape_name, str):
            raise TypeError("'alowtape' must be a str type")  # noqa: DOC501

        try:
            # Searching
            internal_path = os.path.join(self.storage.modules_dir, alowtape_name, self.storage.lang)
            external_path = os.path.join(self.NANAI_ALOWTAPES_DIRECTORY, f"{alowtape_name}.tar")

            # Default context in inside the python package
            if os.path.exists(internal_path):
                for item in os.listdir(internal_path):
                    # keys sensitive management
                    if item.lower().endswith(".aiml"):
                        start_learn_time = datetime.now()
                        file_to_learn = os.path.realpath(
                            os.path.join(
                                self.storage.modules_dir,
                                alowtape_name,
                                self.storage.lang,
                                item,
                            )
                        )
                        fake_stderr = StringIO()
                        fake_stdout = StringIO()
                        with redirect_stderr(fake_stderr), redirect_stdout(fake_stdout):
                            self.storage.kernel.learn(file_to_learn)
                            parse_stderr = (
                                fake_stderr.getvalue().replace("PARSE ERROR: ", "") if fake_stderr.getvalue() else ""
                            )
                            parse_stdout = fake_stdout.getvalue() if fake_stdout.getvalue() else ""
                        learned_file.append({
                            "path": f"{file_to_learn}",
                            "time": f"{datetime.now() - start_learn_time}",
                            "stderr": f"{parse_stderr.strip('\n')}",
                            "stdout": f"{parse_stdout.strip('\n')}",
                        })

            # External context inside NANAI_ALOWTAPES_DIRECTORY
            elif os.path.exists(external_path):
                tar_file = os.path.join(self.NANAI_ALOWTAPES_DIRECTORY, f"{alowtape_name}.tar")
                if os.path.exists(tar_file) and tarfile.is_tarfile(tar_file):
                    with tarfile.open(tar_file, "r:*") as tf:
                        for member in tf.getmembers():
                            start_learn_time = datetime.now()
                            parse_stderr = ""
                            parse_stdout = ""

                            if member.name.lower().endswith(".aiml"):
                                fake_stderr = StringIO()
                                fake_stdout = StringIO()

                                with (
                                    redirect_stderr(fake_stderr),
                                    redirect_stdout(fake_stdout),
                                    NamedTemporaryFile(delete=False) as extracted_file,
                                ):
                                    try:
                                        extracted_file.write(tf.extractfile(member.name).read())
                                        extracted_file.close()
                                        self.storage.kernel.learn(extracted_file.name)
                                    finally:
                                        if os.path.exists(extracted_file.name):
                                            os.remove(extracted_file.name)

                                    parse_stderr = (
                                        fake_stderr.getvalue().replace("PARSE ERROR: ", "")
                                        if fake_stderr.getvalue()
                                        else ""
                                    )
                                    parse_stdout = fake_stdout.getvalue() if fake_stdout.getvalue() else ""

                                learned_file.append({
                                    "path": f"{tar_file}/{member.name}",
                                    "time": f"{datetime.now() - start_learn_time}",
                                    "stderr": f"{parse_stderr.strip('\n')}",
                                    "stdout": f"{parse_stdout.strip('\n')}",
                                })
            message = f"Load {len(learned_file)} files in {alowtape_name} alowtape with success"

        except Exception as error:
            code = 400
            message = f"{error}"

        # Logging

        self.bus.emit("BRAIN_ALOWTAPE_LOAD", message)

        # Return a dict
        return jsonify({
            "server": f"{self.__class__.__name__} {self.storage.version}",
            "kernel": f"{self.storage.kernel.version()}",
            "num_categories": self.storage.kernel.numCategories(),
            "learned_file": learned_file,
            "query_time": f"{datetime.now() - start_time}",
            "when": f"{datetime.now()}",
            "message:": message,
        }), code

    def alowtapes_reload(self) -> tuple:
        """
        Reload every modules in one time
        """
        if os.path.exists(self.storage.brain_file):
            os.remove(self.storage.brain_file)

        self.session_save()
        # Default vars
        start_time = datetime.now()
        message = None
        code = 200
        loaded_alowtape = []

        # Jobs
        try:
            if os.path.isfile(self.storage.brain_file):
                self.storage.kernel.bootstrap(brainFile=self.storage.brain_file)

            else:
                for alowtape in self.storage.alowtape_to_load:
                    start_time_load = datetime.now()
                    self.alowtape_load(alowtape)

                    loaded_alowtape.append({
                        "name": f"{alowtape}",
                        "time": f"{datetime.now() - start_time_load}",
                        "path": os.path.realpath(
                            os.path.join(
                                self.storage.modules_dir,
                                alowtape,
                                self.storage.lang,
                                ".",
                            )
                        ),
                    })

                self.storage.kernel.setPredicate("master", self.storage.brain_file)

            # In every case it name the bot
            self.storage.kernel.setBotPredicate("name", self.storage.brain_file)

            # Save the fresh brain
            self.storage.kernel.saveBrain(self.storage.brain_file)

            message = "Every modules reload with success"

        except Exception as error:
            code = 400
            message = f"{error}"

        # Logging
        logging.debug(f"{self.__class__.__name__}: {message}")

        # Return a dict
        return jsonify({
            "server": f"{self.__class__.__name__} {self.storage.version}",
            "kernel": f"{self.storage.kernel.version()}",
            "reloaded_alowtapes": loaded_alowtape,
            "query_time": f"{datetime.now() - start_time}",
            "when": f"{start_time}",
            "message": message,
        }), code

    def brain_status(self) -> tuple:
        start_time = datetime.now()
        if os.path.exists(self.storage.brain_file):
            brain_file_info = {
                "file": {
                    "path": self.storage.brain_file,
                    "stats": os_stat_to_dict(self.storage.brain_file),
                }
            }
        else:
            brain_file_info = {
                "file": {
                    "path": self.storage.brain_file,
                    "stats": "File do not exist",
                }
            }

        if os.path.exists(self.storage.session_file):
            session_file_info = {
                "file": {
                    "path": self.storage.session_file,
                    "stats": os_stat_to_dict(self.storage.session_file),
                }
            }
        else:
            session_file_info = {
                "file": {
                    "path": self.storage.session_file,
                    "stats": "File do not exist",
                }
            }
        return jsonify({
            "server": f"{self.__class__.__name__} {self.storage.version}",
            "kernel": f"{self.storage.kernel.version()}",
            "query_time": f"{datetime.now() - start_time}",
            "when": f"{datetime.now()}",
            "files": {"session": session_file_info, "brain": brain_file_info},
        })

    def brain_reset(self) -> tuple:
        # Default vars
        start_time = datetime.now()
        message = None
        code = 200

        # Jobs
        try:
            self.storage.kernel.resetBrain()
            message = "Brain reset with succes"
        except Exception as error:
            code = 400
            message = f"{error}"

        # Logging
        logging.debug(f"{self.__class__.__name__}: code:{code}, message:{message}")

        # Return a dict
        return jsonify({
            "server": f"{self.__class__.__name__} {self.storage.version}",
            "kernel": f"{self.storage.kernel.version()}",
            "query_time": f"{datetime.now() - start_time}",
            "when": f"{datetime.now()}",
            "code": code,
            "message:": message,
        }), code

    def brain_load(self) -> tuple:
        """
        read dictionary and create brain in file
        """
        # Default vars
        start_time = datetime.now()
        message = None
        code = 200

        # Jobs
        try:
            if os.path.isfile(self.storage.brain_file):
                self.storage.kernel.bootstrap(brainFile=self.storage.brain_file)

            else:
                for module in self.storage.alowtape_to_load:
                    self.alowtape_load(module)

                self.storage.kernel.resetBrain()
                self.storage.kernel.setPredicate(
                    "master",
                    self.storage.brain_file,
                )

                self.storage.kernel.saveBrain(self.storage.brain_file)

            # In every case it name the bot
            self.storage.kernel.setBotPredicate(
                "name",
                self.storage.brain_file,
            )

            message = "Brain load with succes"
        except Exception as error:
            code = 400
            message = f"{error}"

        # Logging
        logging.debug(f"{self.__class__.__name__}: code:{code}, message:{message}")

        # Return a dict
        return jsonify({
            "server": f"{self.__class__.__name__} {self.storage.version}",
            "kernel": f"{self.storage.kernel.version()}",
            "query_time": f"{datetime.now() - start_time}",
            "when": f"{datetime.now()}",
            "code": code,
            "message": message,
        })

    def brain_save(self) -> tuple:
        """
        Save brain
        """
        # Default vars
        start_time = datetime.now()
        message = None
        code = 200

        # Jobs
        try:
            self.storage.kernel.saveBrain(self.storage.brain_file)
            message = "Brain saved with succes"

        except Exception as error:
            code = 400
            message = f"{error}"

        # Logging
        logging.debug(f"{self.__class__.__name__}: code:{code}, message:{message}")

        # Return a dict
        return jsonify({
            "server": f"{self.__class__.__name__} {self.storage.version}",
            "kernel": f"{self.storage.kernel.version()}",
            "query_time": f"{datetime.now() - start_time} msec",
            "when": f"{datetime.now()}",
            "code": code,
            "message:": message,
        })

    def session_load(self) -> tuple:
        # Default
        start_time = datetime.now()
        message = None
        code = 200
        predicate_count = 0
        predicate_info = []

        # The job
        try:
            if os.path.isfile(self.storage.session_file):
                with open(self.storage.session_file, "rb") as session_file:
                    session = marshal.load(session_file)
                    for predicate, value in session.items():
                        self.storage.kernel.setPredicate(
                            predicate,
                            value,
                            self.storage.session_name,
                        )
                        predicate_count += 1
                        predicate_info.append({
                            "session_name": self.storage.session_name,
                            "predicate": f"{predicate}",
                            "value": value,
                        })

                    message = f"Session loaded with succes ({predicate_count}) predicates into {self.storage.session_name} session"
        except Exception as error:
            message = f"{error}"
            code = 400

        # Logs
        logging.debug(f"{self.__class__.__name__}.session_load() -> code:{code}, message:{message}")

        # Return dict
        return jsonify({
            "server": f"{self.__class__.__name__} {self.storage.version}",
            "kernel": f"{self.storage.kernel.version()}",
            "predicate_loaded": predicate_count,
            "predicate_info": predicate_info,
            "query_time": f"{datetime.now() - start_time}",
            "when": f"{datetime.now()}",
            "code": code,
            "message": message,
        })

    def session_save(self) -> tuple:
        # Default vars
        start_time = datetime.now()
        message = None
        code = 200

        # The job
        try:
            with open(self.storage.session_file, "wb") as session_file:
                marshal.dump(
                    self.storage.kernel.getSessionData(self.storage.session_name),
                    session_file,
                )
            message = "Session save with success"
        except Exception as error:
            message = f"{error}"
            code = 400

        # Logs
        logging.debug(f"{self.__class__.__name__}.session_load() -> code:{code}, message:{message}")

        # Return dict
        return jsonify({
            "server": f"{self.__class__.__name__} {self.storage.version}",
            "kernel": f"{self.storage.kernel.version()}",
            "query_time": f"{datetime.now() - start_time}",
            "when": f"{datetime.now()}",
            "code": code,
            "message": message,
        })

    def session_status(self) -> tuple:
        # Default vars
        start_time = datetime.now()
        message = None
        code = 200
        session_data = None

        # The job
        try:
            session_data = self.storage.kernel.getSessionData(self.storage.session_name)
            message = "Get session status with success"
        except Exception as error:
            message = f"{error}"
            code = 400

        # Logs
        logging.debug(f"{self.__class__.__name__}.session_status() -> code:{code}, message:{message}")

        # Return dict
        return jsonify({
            "server": f"{self.__class__.__name__} {self.storage.version}",
            "kernel": f"{self.storage.kernel.version()}",
            "query_time": f"{datetime.now() - start_time}",
            "when": f"{start_time}",
            "code": code,
            "message": message,
            "session_data": session_data,
        }), code

    def ask(self, question):
        start_time = datetime.now()
        filtered_question = question.replace("'", " ").replace("-", " ")
        self.bus.emit("BRAIN_ASKING", filtered_question)

        try:
            fake_stderr = StringIO()
            with redirect_stderr(fake_stderr):
                answer = self.storage.kernel.respond(
                    f"{filtered_question}",
                    self.storage.session_name,
                )
            if fake_stderr.getvalue():
                self.bus.emit("BRAIN_NO_RESPONSE", fake_stderr.getvalue())

        except AssertionError as error:
            answer = None
            message = error

        if answer:
            self.bus.emit("BRAIN_RESPONSE", answer)
            answer = answer
            code = 200
            message = "OK"

        else:
            answer = None
            code = 204
            message = "No Content"
        return jsonify({
            "server": f"{self.__class__.__name__} {self.storage.version}",
            "kernel": f"{self.storage.kernel.version()}",
            "query_time": f"{datetime.now() - start_time}",
            "when": f"{datetime.now()}",
            "answer": answer,
            "code": code,
            "message": message,
        }), code
