import os
from attrs import define
import queue
import json
import asyncio
import sounddevice as sd
from vosk import KaldiRecognizer
from vosk import Model

from nan.core.eventbus import EventBus


@define
class StorageEar:
    models_dir: str = os.path.realpath(os.path.join(os.path.dirname(__file__), "data", "vosk"))
    lang: str = "fr-FR"
    model_to_load: str = os.path.join(models_dir, lang)
    queuein: queue.Queue = queue.Queue()
    recognizer: KaldiRecognizer = KaldiRecognizer(
        Model(model_to_load),
        int(sd.query_devices(sd.default.device[0], "input")["default_samplerate"]),
    )
    debug: bool = True
    run: bool = False
    pause: bool = False


class Ear:
    def __init__(self, **kwargs):
        self.bus: EventBus = kwargs.get("bus", EventBus())
        self.storage: StorageEar = kwargs.get("storage", StorageEar())

    def setup(self):
        self.storage.run = False
        self.storage.pause = False
        self.storage.recognizer.SetWords(False)
        self.bus.emit("EAR_UP")

    def teardown(self):
        self.storage.run = False
        self.bus.emit("EAR_DOWN")

    async def earloop(self):
        def record_callback(indata, _, __, status):
            if status:
                self.bus.emit("EAR_STATUS", status)
            if not self.storage.pause:
                self.storage.queuein.put(bytes(indata))

        self.storage.run = True
        self.bus.emit("EAR_PROCESS")

        try:
            with sd.RawInputStream(dtype="int16", channels=1, callback=record_callback):
                while await asyncio.sleep(0, self.storage.run):
                    if self.storage.recognizer.AcceptWaveform(self.storage.queuein.get()):
                        result_dict = json.loads(self.storage.recognizer.Result())
                        if result_dict.get("text", "") != "":
                            self.bus.emit("EAR_RECOGNIZE", result_dict.get("text"))

            self.bus.emit("EAR_FINISH")
        except KeyboardInterrupt:
            self.bus.emit("EAR_KEYBOARD_INTERUPTION")
        except Exception as e:
            self.bus.emit("EAR_EXCEPTION", f"{e}")
