import os
import tempfile
import subprocess
import queue
import threading
import sounddevice as sd
import soundfile as sf
from attrs import define
from flask import jsonify
from datetime import datetime


from nan.core.eventbus import EventBus


class Player:
    def __init__(self, **kwargs):
        self.bus: EventBus = kwargs.get("bus", EventBus())
        self.buffer_size = kwargs.get("buffer_size", 20)
        self.block_size = kwargs.get("block_size", 2048)
        self.device = kwargs.get("device", int(sd.query_devices(sd.default.device[0], "output")["index"]))

        self.queue = None
        self.event = None
        self.verbose = False

    def play(self, filename):
        self.queue = queue.Queue(maxsize=self.block_size)
        self.event = threading.Event()

        def callback(outdata, frames, time, status):
            assert frames == self.block_size
            if status.output_underflow:
                self.bus.emit(
                    "VOICE_PLAYER_OUTPUT_OVERFLOW",
                    f"Increase blocksize? actual block size {self.block_size}",
                )
                raise sd.CallbackAbort
            assert not status
            try:
                data = self.queue.get_nowait()
            except queue.Empty as e:
                self.bus.emit(
                    "VOICE_PLAYER_BUFFER_EMPTY",
                    f"Increase buffersize? actual buffer size: {self.buffer_size}",
                )
                raise sd.CallbackAbort from e
            if len(data) < len(outdata):
                outdata[: len(data)] = data
                outdata[len(data) :] = b"\x00" * (len(outdata) - len(data))
                raise sd.CallbackStop
            outdata[:] = data

        if os.path.exists(filename):
            try:
                with sf.SoundFile(filename) as f:
                    for _ in range(self.buffer_size):
                        data = f.buffer_read(self.block_size, dtype="float32")
                        if not data:
                            break
                        self.queue.put_nowait(data)  # Pre-fill queue
                    stream = sd.RawOutputStream(
                        samplerate=f.samplerate,
                        blocksize=self.block_size,
                        device=self.device,
                        channels=f.channels,
                        dtype="float32",
                        callback=callback,
                        finished_callback=self.event.set,
                    )
                    with stream:
                        timeout = self.block_size * self.buffer_size / f.samplerate
                        self.bus.emit(
                            "VOICE_PLAYER_START",
                            f"filename={filename},  samplerate={f.samplerate}, channels={f.channels}",
                        )
                        while data:
                            data = f.buffer_read(self.block_size, dtype="float32")
                            self.queue.put(data, timeout=timeout)
                        self.event.wait()  # Wait until playback is finished
                        self.bus.emit("VOICE_PLAYER_FINISH")

            except KeyboardInterrupt:
                self.bus.emit(
                    "VOICE_PLAYER_KEYBOARD_INTERRUPT",
                    "Interrupted by user",
                )
            except queue.Full:
                self.bus.emit(
                    "VOICE_PLAYER_QUEUE_FULL",
                    "A timeout occurred, i.e. there was an error in the callback",
                )
            except Exception as e:
                self.bus.emit(
                    "VOICE_PLAYER_EXCEPTION",
                    f"{type(e).__name__} : {e}",
                )


@define
class StorageVoice:
    models_dir: str = os.path.realpath(os.path.join(os.path.dirname(__file__), "..", "api", "data", "piper"))
    model_name: str = "fr_FR-siwis-medium"
    model_to_load: str = os.path.join(models_dir, f"{model_name}.onnx")
    config_to_load: str = f"{model_to_load}.json"
    player: Player = Player()
    version: str = "0.1a"
    debug: bool = False
    run: bool = False
    pause: bool = False


class Voice:
    def __init__(self, **kwargs) -> None:
        self.storage: StorageVoice = kwargs.get("storage", StorageVoice())
        self.bus: EventBus = kwargs.get("bus", EventBus())

    def setup(self) -> None:
        """
        Method for setting up the voice before exercising it.

        When ready the Voice will emit "VOICE_UP"
        """
        self.storage.player.bus = self.bus
        self.storage.player.verbose = self.storage.debug
        self.storage.run = True
        self.bus.emit("VOICE_UP")

    def teardown(self) -> None:
        """
        Method for deconstructing the voice when finish to use it.

        When ready the Voice will emit "VOICE_DOWN"
        """
        self.storage.run = False
        self.bus.emit("VOICE_DOWN")

    def speech(self, text: str) -> tuple:
        """
        Text To Speech

        :param text: The text to say
        :type text: str
        :return: jsonsify of ject follow by error code
        :rtype: tuple
        """
        # Default vars
        start_time = datetime.now()
        message = None
        code = 200
        # The job
        try:
            if len(text) > 0 and self.storage.run:
                #  Start be change ou state to VOICE_PROCESS
                self.bus.emit("VOICE_PROCESS")

                # The wav file will be delete and name totally random
                with tempfile.NamedTemporaryFile(suffix=".wav") as file:
                    command = [
                        "piper",
                        "--model",
                        f"{self.storage.model_to_load}",
                        "--config",
                        f"{self.storage.config_to_load}",
                        "--output_file",
                        f"{file.name}",
                    ]

                    # Generate piper wave file
                    self.bus.emit("VOICE_PIPER_START", f"{' '.join(command)}\n")
                    process = subprocess.Popen(
                        command,
                        stdin=subprocess.PIPE,
                        stdout=subprocess.PIPE,
                        stderr=subprocess.STDOUT,
                    )
                    output = process.communicate(input=text.encode())[0]
                    process.wait()
                    self.bus.emit("VOICE_PIPER_FINISH", f"{output.decode()}")

                    # Playing sound
                    self.storage.player.play(file.name)

                    # Operation finish the voice pass VOICE_FINISH
                    self.bus.emit("VOICE_FINISH")

            message = "Play sound with success"
        except Exception as error:
            message = f"{error}"
            code = 400

        # Return dict
        return jsonify({
            "server": f"{self.__class__.__name__} {self.storage.version}",
            "kernel": "piper v???",
            "query_time": f"{datetime.now() - start_time}",
            "when": f"{datetime.now()}",
            "code": code,
            "message": message,
        }), code


if __name__ == "__main__":
    bus = EventBus()
    voice_object = Voice(bus=bus)
    voice_object.say("Bonjour")
    voice_object.say("Bonjour")
