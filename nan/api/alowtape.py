import os
import json
import yaml
import tarfile
import time
import stat
import semver
import tempfile
from shutil import copy2
from io import StringIO
from contextlib import redirect_stderr
from datetime import datetime
from flask import jsonify
from aiml.AimlParser import create_parser
from aiml.AimlParser import AimlParserError
from xml.etree.ElementTree import ParseError
from xml.sax import SAXParseException
from nan.api.basics import size_of
from nan.api.basics import os_stat_to_dict
from nan.core.errors import error_code_to_text
from nan.core.xdg import XdgBaseDirectory


class AlowTapeBuilder:
    @staticmethod
    def set_permissions(tarinfo: tarfile.TarInfo):
        tarinfo.mode = 0o640
        tarinfo.uid = 0
        tarinfo.gid = 0
        return tarinfo

    def add(self, alowtape: str = None, sources: list = None) -> tuple:
        start_time = datetime.now()
        message = None
        code = 200
        added = []

        try:
            with tarfile.open(alowtape, "r") as input_archive:
                input_archive.close()
        except (tarfile.TarError, ValueError) as error:
            return jsonify({
                "query_time": f"{datetime.now() - start_time}",
                "when": f"{start_time}",
                "message": f"{error}: {alowtape}",
            }), 403
        except FileNotFoundError as error:
            return jsonify({
                "query_time": f"{datetime.now() - start_time}",
                "when": f"{start_time}",
                "message": f"{error_code_to_text(error.errno)}: {alowtape}",
            }), 404
        except OSError as error:
            return jsonify({
                "query_time": f"{datetime.now() - start_time}",
                "when": f"{start_time}",
                "message": f"{error_code_to_text(error.errno)}: {alowtape}",
            }), 405

        # The final output file come from temporaryfile output_tar_file
        # ALOWTAPE-INFO file is modify inside a temporary file temp_allowtape_info_file
        with (
            tempfile.NamedTemporaryFile(delete=False) as output_tar_file,
            tempfile.NamedTemporaryFile(mode="w", delete=False) as temp_allowtape_info_file,
        ):
            # Try is here for permit a finally block with delete of temporary file
            try:
                if len(sources):
                    with tarfile.open(alowtape, "r") as input_archive:
                        # close the temporary file for permit to write inside
                        output_tar_file.close()

                        # get alowtape-info copntent
                        allowtape_content = yaml.safe_load(input_archive.extractfile("ALOWTAPE-INFO").read())
                        # update version
                        allowtape_content["version"] = str(
                            semver.Version.parse(allowtape_content["version"]).next_version("patch")
                        )
                        # write the mew ALOWTAPE-INFO file content into temp_allowtape_info_file
                        json.dump(
                            allowtape_content,
                            temp_allowtape_info_file,
                            ensure_ascii=False,
                            indent=4,
                        )
                        temp_allowtape_info_file.close()

                        # count and print files to remove and write archive inside temporary file
                        with tarfile.open(output_tar_file.name, "w") as output_archive:
                            output_archive.add(
                                temp_allowtape_info_file.name,
                                arcname="ALOWTAPE-INFO",
                                recursive=False,
                                filter=self.set_permissions,
                            )

                            # add all files execpt "ALOWTAPE-INFO" and listed in sources
                            for name in input_archive.getnames():
                                info = input_archive.getmember(name)
                                # file = input_archive.extractfile(name)

                                # Convert all sources to relpath
                                relpath_sources = [os.path.relpath(item) for item in sources]

                                # Filter ALOWTAPE-INFO and all ready present entries consider as doublons
                                if not info.name.endswith("ALOWTAPE-INFO") and info.name not in relpath_sources:
                                    output_archive.addfile(tarinfo=info, fileobj=input_archive.extractfile(name))

                            # Add files in sources after doublons have been remove
                            for source in relpath_sources:
                                if os.path.exists(source) and os.path.isfile(source):
                                    relative_path = os.path.relpath(source)
                                    if not relative_path.endswith("ALOWTAPE-INFO"):
                                        output_archive.add(
                                            source, arcname=relative_path, recursive=False, filter=self.set_permissions
                                        )
                                        added.append(f"{source} {relative_path}")
                                if os.path.exists(source) and os.path.isdir(source):
                                    for root, dirs, files in os.walk(source):
                                        for file in files:
                                            file_path = os.path.join(root, file)
                                            relative_path = os.path.relpath(file_path, source)
                                            if not relative_path.endswith("ALOWTAPE-INFO"):
                                                output_archive.add(
                                                    file_path,
                                                    arcname=relative_path,
                                                    recursive=False,
                                                    filter=self.set_permissions,
                                                )
                                                added.append(f"{file_path} {relative_path}")

                    # copy the temporary archive data into a new archive finaly alowtape
                    with (
                        tarfile.open(output_tar_file.name, "r") as input_archive,
                        tarfile.open(alowtape, "w") as output_archive,
                    ):
                        for name in input_archive.getnames():
                            output_archive.addfile(
                                tarinfo=input_archive.getmember(name), fileobj=input_archive.extractfile(name)
                            )

                    code = 200
                    message = "alowtape regenerate with success"
                else:
                    # here that is the best place to report about nothing have to be remove
                    code = 200
                    message = "nothing to add"

            # block here because tempraryfile have delete disable
            finally:
                if os.path.exists(output_tar_file.name):
                    os.remove(output_tar_file.name)
                if os.path.exists(temp_allowtape_info_file.name):
                    os.remove(temp_allowtape_info_file.name)

            # Return a tuple
        return jsonify({
            "added": added,
            "query_time": f"{datetime.now() - start_time}",
            "when": f"{start_time}",
            "message": message,
        }), code

    def create(self, meta: str = None, alowtape: str = None, sources: list = None) -> tuple:
        """
        Create a empty AlowTape
        """
        start_time = datetime.now()
        message = None
        code = None

        # Check the meta file et exit without do nothing in case of error
        try:
            # test to open meta file
            with open(meta) as meta_file_descriptor:
                # test load meta file
                meta_info = yaml.safe_load(meta_file_descriptor)

                # test mandatory keys
                for tested_key in ["name", "version", "description", "license", "authors"]:
                    _ = meta_info[tested_key]

                # test version
                semver.Version.parse(meta_info["version"])

        except FileNotFoundError as error:
            return jsonify({
                "query_time": f"{datetime.now() - start_time}",
                "when": f"{start_time}",
                "message": f"{error_code_to_text(error.errno)}: {meta}",
            }), 404
        except (yaml.YAMLError, TypeError) as error:
            return jsonify({
                "query_time": f"{datetime.now() - start_time}",
                "when": f"{start_time}",
                "message": f"error during open file: {meta}: {error}",
            }), 405
        except KeyError as error:
            return jsonify({
                "query_time": f"{datetime.now() - start_time}",
                "when": f"{start_time}",
                "message": f"key name must be present: {error}",
            }), 406
        except ValueError as error:
            return jsonify({
                "query_time": f"{datetime.now() - start_time}",
                "when": f"{start_time}",
                "message": f"version error: {error}",
            }), 407

        # create the alowtape
        try:
            with tarfile.open(alowtape, mode="w:") as alowtape_file:
                # First alowtape element is alway a file name ALOWTAPE-INFO
                alowtape_file.add(
                    meta,
                    arcname="ALOWTAPE-INFO",
                    recursive=False,
                    filter=self.set_permissions,
                )

                # Added file come after
                if sources:
                    for source in sources:
                        if os.path.exists(source) and os.path.isfile(source):
                            relative_path = os.path.relpath(source)
                            if not relative_path.endswith("ALOWTAPE-INFO"):
                                alowtape_file.add(
                                    source, arcname=relative_path, recursive=False, filter=self.set_permissions
                                )
                        if os.path.exists(source) and os.path.isdir(source):
                            for root, dirs, files in os.walk(source):
                                for file in files:
                                    file_path = os.path.join(root, file)
                                    relative_path = os.path.relpath(file_path, source)
                                    if not relative_path.endswith("ALOWTAPE-INFO"):
                                        alowtape_file.add(
                                            file_path,
                                            arcname=relative_path,
                                            recursive=False,
                                            filter=self.set_permissions,
                                        )
            code = 200
            message = "alowtape create with success"
        except Exception as error:
            code = 400
            message = f"{error}"

        return jsonify({
            "query_time": f"{datetime.now() - start_time}",
            "when": f"{start_time}",
            "message": message,
        }), code

    def destroy(self, alowtape: str = None) -> tuple:
        """
        Destroy a alowtape file

        :param alowtape: path of a alowtape file
        :type alowtape: should be a string
        """
        start_time = datetime.now()
        message = None
        code = None

        try:
            if tarfile.is_tarfile(alowtape):
                os.remove(alowtape)
                code = 200
                message = f"{alowtape} is destroy with succes"
            else:
                code = 403
                message = f"{alowtape} is not a tar archive."
        except FileNotFoundError as error:
            code = 404
            message = f"{error_code_to_text(error.errno)}: {alowtape}"
        except OSError as error:
            code = 405
            message = f"{error_code_to_text(error.errno)}: {alowtape}"

        return jsonify({
            "query_time": f"{datetime.now() - start_time}",
            "when": f"{datetime.now()}",
            "message": message,
        }), code

    def remove(self, alowtape: str = None, sources: list = None) -> tuple:
        # a counter fo know how many entry have to be remove
        start_time = datetime.now()
        message = None
        code = 200
        files_removed = []

        try:
            with tarfile.open(alowtape, "r") as input_archive:
                input_archive.close()
        except (tarfile.TarError, ValueError) as error:
            return jsonify({
                "query_time": f"{datetime.now() - start_time}",
                "when": f"{start_time}",
                "message": f"{error}: {alowtape}",
            }), 403
        except FileNotFoundError as error:
            return jsonify({
                "query_time": f"{datetime.now() - start_time}",
                "when": f"{start_time}",
                "message": f"{error_code_to_text(error.errno)}: {alowtape}",
            }), 404
        except OSError as error:
            return jsonify({
                "query_time": f"{datetime.now() - start_time}",
                "when": f"{start_time}",
                "message": f"{error_code_to_text(error.errno)}: {alowtape}",
            }), 405

        # The final output file come from temporaryfile output_tar_file
        # ALOWTAPE-INFO file is modify inside a temporary file temp_allowtape_info_file
        with (
            tempfile.NamedTemporaryFile(delete=False) as output_tar_file,
            tempfile.NamedTemporaryFile(mode="w", delete=False) as temp_allowtape_info_file,
        ):
            # Try is here for permit a finally block with delete of temporary file
            try:
                with tarfile.open(alowtape, "r") as input_archive:
                    # close the temporary file for permit to write inside
                    output_tar_file.close()

                    # get alowtape-info copntent
                    allowtape_content = yaml.safe_load(input_archive.extractfile("ALOWTAPE-INFO").read())
                    # update version
                    allowtape_content["version"] = str(
                        semver.Version.parse(allowtape_content["version"]).next_version("patch")
                    )
                    # write the mew ALOWTAPE-INFO file content into temp_allowtape_info_file
                    json.dump(
                        allowtape_content,
                        temp_allowtape_info_file,
                        ensure_ascii=False,
                        indent=4,
                    )
                    temp_allowtape_info_file.close()

                    # count and print files to remove and write archive inside temporary file
                    with tarfile.open(output_tar_file.name, "w") as output_archive:
                        # add fresh ALOWTAPE-INFO from temp_allowtape_info_file
                        output_archive.add(
                            temp_allowtape_info_file.name,
                            arcname="ALOWTAPE-INFO",
                            recursive=False,
                            filter=self.set_permissions,
                        )

                        # add all files execpt "ALOWTAPE-INFO" and listed in parsed.SOURCE
                        for name in input_archive.getnames():
                            info = input_archive.getmember(name)
                            file = input_archive.extractfile(name)

                            # ALOWTAPE-INFO has all ready been added
                            if not info.name.endswith("ALOWTAPE-INFO"):
                                if info.name not in sources:
                                    output_archive.addfile(info, file)
                                else:
                                    files_removed.append(f"remove {name} size {size_of(info.size)}")

                # if file have been remove then copy the temporary archive data into a new archive
                # the out put file is finaly the source parsed.ALOWTAPE
                if len(files_removed):
                    with (
                        tarfile.open(output_tar_file.name, "r") as input_archive,
                        tarfile.open(alowtape, "w") as output_archive,
                    ):
                        for name in input_archive.getnames():
                            output_archive.addfile(
                                tarinfo=input_archive.getmember(name), fileobj=input_archive.extractfile(name)
                            )

                    code = 200
                    message = "alowtape regenerate with success"
                else:
                    # here that is the best place to report about nothing have to be remove
                    code = 200
                    message = "nothing to remove"

            # block here because tempraryfile have delete disable
            finally:
                if os.path.exists(output_tar_file.name):
                    os.remove(output_tar_file.name)
                if os.path.exists(temp_allowtape_info_file.name):
                    os.remove(temp_allowtape_info_file.name)

        # Return a tuple
        return jsonify({
            "files_removed": files_removed,
            "query_time": f"{datetime.now() - start_time}",
            "when": f"{start_time}",
            "message": message,
        }), code


class AlowTapeManager:
    NANAI_ALOWTAPES_DIRECTORY = os.environ.get(
        "NANAI_ALOWTAPES_DIRECTORY", os.path.join(XdgBaseDirectory(resource="nanai").data_path, "alowtapes")
    )

    def install(self, sources: list = None) -> tuple:
        """
        Install a alowtape from a alowtape file of file object
        """
        start_time = datetime.now()
        message = None
        code = 200
        installed = []

        try:
            for alowtape in sources:
                with tarfile.open(alowtape, "r") as input_archive:
                    input_archive.close()
        except (tarfile.TarError, ValueError) as error:
            return jsonify({
                "query_time": f"{datetime.now() - start_time}",
                "when": f"{start_time}",
                "message": f"{error}: {alowtape}",
            }), 403
        except FileNotFoundError as error:
            return jsonify({
                "query_time": f"{datetime.now() - start_time}",
                "when": f"{start_time}",
                "message": f"{error_code_to_text(error.errno)}: {alowtape}",
            }), 404
        except OSError as error:
            return jsonify({
                "query_time": f"{datetime.now() - start_time}",
                "when": f"{start_time}",
                "message": f"{error_code_to_text(error.errno)}: {alowtape}",
            }), 405

        # Verify the installation directory
        if not os.path.exists(self.NANAI_ALOWTAPES_DIRECTORY):
            os.makedirs(self.NANAI_ALOWTAPES_DIRECTORY)

        # Start alowtapes installtion
        try:
            for alowtape in sources:
                need_install = True
                alowtape_info_dst = None
                alowtape_info_src = None
                alowtape_dst_version = None
                alowtape_src_version = None

                if tarfile.is_tarfile(alowtape):
                    # Determone if all ready have a version installed
                    alowtape_dst = os.path.join(self.NANAI_ALOWTAPES_DIRECTORY, os.path.basename(alowtape))
                    if os.path.exists(alowtape_dst) and tarfile.is_tarfile(alowtape_dst):
                        with tarfile.open(alowtape_dst, "r:*") as installed_tar:
                            if installed_tar.getmembers():
                                alowtape_info_dst = yaml.safe_load(installed_tar.extractfile("ALOWTAPE-INFO").read())
                                alowtape_dst_version = semver.Version.parse(alowtape_info_dst["version"])

                    # Determine information of package to install
                    with tarfile.open(alowtape, "r:*") as tf:
                        if tf.getmembers():
                            alowtape_info_src = yaml.safe_load(tf.extractfile("ALOWTAPE-INFO").read())
                            alowtape_src_version = semver.Version.parse(alowtape_info_src["version"])

                    # process data
                    # determine the message and if need to be install
                    if alowtape_dst_version:
                        if alowtape_dst_version < alowtape_src_version:
                            info = f"update from {alowtape_dst_version} to {alowtape_src_version}"
                        elif alowtape_dst_version > alowtape_src_version:
                            info = f"downgrade from {alowtape_dst_version} to {alowtape_src_version}"
                        else:
                            info = "all ready installed"
                            need_install = False

                    # defautl case
                    else:
                        info = f"install {alowtape_src_version}"

                    # install if needed
                    if need_install:
                        copy2(
                            alowtape,
                            alowtape_dst,
                        )

                    # save the status of what happen alowtape by alowtape
                    installed.append({
                        "name": alowtape_info_src["name"],
                        "version": alowtape_info_src["version"],
                        "description": alowtape_info_src["description"],
                        "license": alowtape_info_src["license"],
                        "authors": alowtape_info_src["authors"],
                        "info": info,
                    })

            code = 200
            message = "install finish with success"
        except Exception as error:
            code = 400
            message = f"{error}"

        return jsonify({
            "query_time": f"{datetime.now() - start_time}",
            "when": f"{start_time}",
            "message": message,
            "installed": installed,
        }), code

    def remove(self, sources: list = None) -> tuple:
        """
        Uninstall a alowtape by name
        """
        start_time = datetime.now()
        code = 200
        message = None
        removed = []
        try:
            for source in sources:
                tar_file = os.path.join(self.NANAI_ALOWTAPES_DIRECTORY, f"{source}.tar")
                if os.path.exists(tar_file) and tarfile.is_tarfile(tar_file):
                    os.remove(tar_file)
            message = "remove with succes"
        except Exception as error:
            code = 400
            message = f"{error}"
        return jsonify({
            "query_time": f"{datetime.now() - start_time}",
            "when": f"{start_time}",
            "message": message,
            "removed": removed,
        }), code

    def list(self):
        start_time = datetime.now()
        code = 200
        message = None
        alowtapes_list = []
        try:
            for dirpath, dirs, files in os.walk(self.NANAI_ALOWTAPES_DIRECTORY):
                for file in files:
                    if tarfile.is_tarfile(os.path.join(dirpath, file)):
                        with tarfile.open(os.path.join(dirpath, file), "r:*") as tf:
                            if tf.getmembers():
                                alowtape_info = yaml.safe_load(tf.extractfile("ALOWTAPE-INFO").read())
                                alowtapes_list.append({
                                    "name": alowtape_info["name"],
                                    "version": alowtape_info["version"],
                                    "description": alowtape_info["description"],
                                    "license": alowtape_info["license"],
                                    "authors": alowtape_info["authors"],
                                })
            message = "listed with succes"
        except Exception as error:
            code = 400
            message = f"{error}"
        return jsonify({
            "query_time": f"{datetime.now() - start_time}",
            "when": f"{start_time}",
            "message": message,
            "list": alowtapes_list,
        }), code

    def info(self, alowtape: str = None) -> tuple:
        """
        Return  information about a low tape file

        :param alowtape: path of a alowtape file, or file-like object
        :type alowtape: should be a string, file, or file-like object
        """
        # Default vars
        start_time = datetime.now()
        message = None
        code = 200
        alowtape_info = {}
        alowtape_stats = {}
        alowtape_members = []
        aiml_parser = create_parser()

        try:
            if tarfile.is_tarfile(alowtape):
                with tarfile.open(alowtape, "r:*") as tf:
                    if len(tf.getmembers()):
                        alowtape_info = yaml.safe_load(tf.extractfile("ALOWTAPE-INFO").read())

                    alowtape_stat = os_stat_to_dict(alowtape)
                    alowtape_stats = {
                        "file": os.path.basename(alowtape),
                        "size": alowtape_stat["size"],
                        "creation": alowtape_stat["ctime"],
                        "modified": alowtape_stat["modified_time"],
                        "accessed": alowtape_stat["accessed_time"],
                        "content": f"{len(tf.getmembers())} files",
                    }

                    for tarinfo in tf:
                        mode = "??????????" if tarinfo.mode is None else stat.filemode(tarinfo.mode)
                        owner = tarinfo.uname or tarinfo.uid
                        group = tarinfo.gname or tarinfo.gid
                        size = (
                            "%10s" % ("%d,%d" % (tarinfo.devmajor, tarinfo.devminor))
                            if tarinfo.ischr() or tarinfo.isblk()
                            else size_of(tarinfo.size)
                        )
                        date_and_time = (
                            "????-??-?? ??:??:??"
                            if tarinfo.mtime is None
                            else "%d-%02d-%02d %02d:%02d:%02d" % time.localtime(tarinfo.mtime)[:6]
                        )
                        pathname = tarinfo.name + ("/" if tarinfo.isdir() else "")

                        link_info = ""
                        if tarinfo.issym():
                            link_info = "-> " + tarinfo.linkname
                        if tarinfo.islnk():
                            link_info = "link to " + tarinfo.linkname

                        # Load and parse the AIML file.
                        error = None
                        if tarinfo.name.lower().endswith(".aiml"):
                            fake_stderr = StringIO()

                            with tempfile.NamedTemporaryFile(delete=False) as aiml_file:
                                try:
                                    aiml_file.write(tf.extractfile(tarinfo.name).read())
                                    aiml_file.close()

                                    with redirect_stderr(fake_stderr):
                                        aiml_parser.parse(aiml_file.name)

                                    error = (
                                        fake_stderr.getvalue().replace("PARSE ERROR: ", "")
                                        if fake_stderr.getvalue()
                                        else ""
                                    )
                                except (SAXParseException, AimlParserError, ParseError) as msg:
                                    error = f"{msg}"
                                finally:
                                    if os.path.exists(aiml_file.name):
                                        os.remove(aiml_file.name)

                        alowtape_members.append({
                            "mode": mode,
                            "owner": owner,
                            "group": group,
                            "size": size,
                            "date_and_time": date_and_time,
                            "pathname": pathname,
                            "link_info": link_info,
                            "error": error,
                        })

                message = f"{alowtape} get info with success"
            else:
                code = 403
                message = f"{alowtape} is not a tar archive."
        except FileNotFoundError as error:
            code = 404
            message = f"{error_code_to_text(error.errno)}: {alowtape}"
        except OSError as error:
            code = 405
            message = f"{error_code_to_text(error.errno)}: {alowtape}"

        # Return a tuple
        return jsonify({
            "alowtape_info": alowtape_info,
            "alowtape_stats": alowtape_stats,
            "alowtape_members": alowtape_members,
            "query_time": f"{datetime.now() - start_time}",
            "when": f"{datetime.now()}",
            "message": message,
        }), code
