import os
import datetime


def size_of(size, suffix="B") -> str:
    """
    Scale bytes to its proper format

    **Example:**
        1253656 => '1.20MB'
        1253656678 => '1.17GB'

    :param size: bytes size to convert
    :type size: int
    :param suffix: what suffix is add at the end of the returned value
    :type suffix: str
    :return: a human readable size
    :rtype: str
    """
    for unit in ("", "K", "M", "G", "T", "P", "E", "Z", "Y"):
        if size < 1024:
            return "{size:.2f}{unit}{suffix}".format(size=size, unit=unit, suffix=suffix)
        size /= 1024
    return "{size:.2f}".format(size=size)


def os_stat_to_dict(file) -> dict:
    stat_info = os.stat(file)

    return {
        "accessed_time": datetime.datetime.fromtimestamp(stat_info.st_atime).strftime("%Y-%m-%d %H:%M:%S"),
        "size": size_of(stat_info.st_size),
        "modified_time": datetime.datetime.fromtimestamp(stat_info.st_mtime).strftime("%Y-%m-%d %H:%M:%S"),
        "inode": stat_info.st_ino,
        "device": stat_info.st_dev,
        "mode": stat_info.st_mode,
        "links": stat_info.st_nlink,
        "uid": stat_info.st_uid,
        "gid": stat_info.st_gid,
        "ctime": datetime.datetime.fromtimestamp(stat_info.st_ctime).strftime("%Y-%m-%d %H:%M:%S"),
    }
