import sys
import os
import time
import asyncio
import logging

from flask import Flask
from attrs import define
from attrs import asdict
from argparse import ArgumentParser
from multiprocessing import Process

from nan.core.logger import CustomFormatter
from nan.core.tasks import TaskManager
from nan.core.eventbus import EventBus
from nan.api.brain import Brain
from nan.api.voice import Voice
from nan.api.ear import Ear
from nan.api.basics import size_of
from nan.ui.ncmd import NanUICmd
from nan.ui import WrapperCmdLineArgParser
from nan.core.errors import error_code_to_text

# https://fallout.fandom.com/wiki/Pip-OS_v7.1.0.8
# https://fallout.fandom.com/wiki/Terminal
# https://fallout.fandom.com/wiki/Unified_Operating_System
# https://fallout.fandom.com/wiki/RETROS_BIOS
# https://fallout.fandom.com/wiki/MF_Boot_Agent

logger = logging.getLogger("nameOfTheLogger")
ConsoleOutputHandler = logging.StreamHandler(sys.stdout)

logger.addHandler(ConsoleOutputHandler)
parser_gia_backbone = ArgumentParser(
    prog="gia-backbone",
    add_help=True,
)
parser_gia_backbone.add_argument(
    "command",
    nargs="?",
    help="optional commands or file to run, if no commands given, enter an interactive shell",
)
parser_gia_backbone.add_argument(
    "command_args",
    nargs="...",
    help="if commands is not a file use optional arguments for commands",
)


parser_status = ArgumentParser(prog="status", description="print backbone server status and exit")

parser_stop = ArgumentParser(prog="stop", description="stop backbone server")

parser_start = ArgumentParser(prog="start", description="start backbone server")

parser_ask = ArgumentParser(prog="ask", description="ask something to alice")

parser_exit = ArgumentParser(prog="exit", description="exit shell with a given exit code")
parser_exit.add_argument(
    "code",
    nargs="*",
    type=int,
    help="exit code",
)

parser_clear = ArgumentParser(
    prog="clear",
    description="Clear screen",
)

parser_set = ArgumentParser(prog="set", description="set settings")


class AppBackbone(NanUICmd):
    def __init__(self, **kwargs):
        super().__init__()
        # self.color_red = self.painter.color(
        #     fg=(230, 0, 0),
        #     bg=None,
        # )
        # self.color_yellow = self.painter.color(
        #     fg=(255, 255, 0),
        #     bg=None,
        # )
        # self.color_green = self.painter.color(
        #     fg=(0, 255, 0),
        #     bg=None,
        # )

        self.logger = logging.getLogger()
        self.logger.name = "LittleAlice"
        self.logger.setLevel(logging.DEBUG)

        # create console handler and set level to debug
        ch = logging.StreamHandler(sys.stdout)
        ch.setLevel(logging.ERROR)

        # add formatter to ch
        ch.setFormatter(CustomFormatter())

        # add ch to logger
        self.logger.addHandler(ch)

        file_handler = logging.FileHandler("logs.log")
        file_handler.setLevel(logging.DEBUG)
        file_handler.setFormatter(CustomFormatter())
        self.logger.addHandler(file_handler)

        # Default context
        self.storage = kwargs.get("storage", StorageBackBone())
        self.storage.app = kwargs.get("app", Flask(__name__))
        self.storage.app.app_context().push()

        self.bus = kwargs.get("bus", EventBus())

        self.storage.debug = True
        self.brain = self.storage.brains[0]
        self.brain.bus = self.bus
        self.brain.storage.debug = self.storage.debug

        self.voice = self.storage.voices[0]
        self.voice.bus = self.bus
        self.voice.storage.debug = self.storage.debug

        self.ear = self.storage.ears[0]
        self.ear.bus = self.bus
        self.ear.storage.debug = self.storage.debug

        # API Entry point
        # Must be done here where self.storage.app is define
        self.setup()
        # Must be done here where self.storage.app is define

    @property
    def prompt(self):
        return f"{self.rc}> " if self.rc else "> "

    @property
    def intro(self):
        import gc
        import sys
        import os

        loader_mpy = "MPY %s" % sys.implementation.mpy if hasattr(sys.implementation, "mpy") else ""

        if hasattr(gc, "mem_free") and hasattr(gc, "mem_alloc"):
            gc.collect()
            memory_total = "%s MEMORY SYSTEM\n" % str(size_of(gc.mem_free() + gc.mem_alloc())).upper()
            memory_free = "%s FREE\n" % str(size_of(gc.mem_free())).upper()
        elif hasattr(os, "sysconf"):
            memory_total = (
                "%s RAM SYSTEM\n" % str(size_of(os.sysconf("SC_PAGE_SIZE") * os.sysconf("SC_PHYS_PAGES"))).upper()
            )
            memory_free = "%s FREE\n" % str(size_of(os.sysconf("SC_PAGE_SIZE") * os.sysconf("SC_AVPHYS_PAGES"))).upper()
        else:
            memory_total = ""
            memory_free = ""

        header = " %s V%s " % (
            self.storage.application_name.upper(),
            self.storage.application_version.upper(),
        )

        header = header.center(self.painter.area.size.w, "*")

        return """%s

%s
LOADER %s %s %s
EXEC PYTHON V%s
%s%s""" % (
            header,
            self.storage.application_license.upper(),
            sys.implementation.name.upper(),
            ".".join(str(item).upper() for item in list(sys.implementation.version)),
            loader_mpy,
            sys.version.upper(),
            memory_total,
            memory_free,
        )

    def preloop(self):
        self.interactive = True

    def emptyline(self):
        pass

    def do_EOF(self, _):  # noqa: N802
        pass

    def setup(self):
        # api/status
        self.storage.app.add_url_rule("/api/status/brain", "status_brain", self.brain.brain_status)
        self.storage.app.add_url_rule("/api/status/session", "status_session", self.brain.session_status)
        # self.storage.app.add_url_rule("/api/status/voice", "status_voice", self.voice.status)
        # self.storage.app.add_url_rule("/api/status/ear", "status_ear", self.ear.status)
        # self.storage.app.add_url_rule("/api/status/eye", "status_eye", self.eye.status)

        # api/save
        self.storage.app.add_url_rule("/api/save/brain", "save_brain", self.brain.brain_save)
        self.storage.app.add_url_rule("/api/save/session", "save_session", self.brain.session_save)

        # api/load
        self.storage.app.add_url_rule("/api/load/brain", "load_brain", self.brain.brain_load)
        self.storage.app.add_url_rule("/api/load/session", "load_session", self.brain.session_load)
        self.storage.app.add_url_rule(
            "/api/load/alowtape/<string:alowtape_name>",
            "load_alowtape",
            self.brain.alowtape_load,
        )
        self.storage.app.add_url_rule(
            "/api/load/alowtape/all",
            "load_alowtape_all",
            self.brain.alowtapes_reload,
        )

        # api/delete
        self.storage.app.add_url_rule("/api/delete/brain", "delete_brain", self.brain.brain_reset)
        # self.storage.app.add_url_rule("/api/delete/session", "delete_session", self.brain.session_reset)

        # nothing
        self.storage.app.add_url_rule("/api/ask/<string:question>", "ask", self.brain.ask)
        self.storage.app.add_url_rule("/api/voice/speech/<string:text>", "speech", self.voice.speech)

    async def __async_cmdloop(self):
        await self.cmdloop()

    async def __async_earloop(self):
        await self.ear.earloop()

    def start(self):
        self.loop = asyncio.new_event_loop()
        asyncio.set_event_loop(self.loop)
        self.tasks = TaskManager(self.loop)

        self.tasks.add(self.__async_earloop(), "NanEar loop")
        self.tasks.add(self.__async_cmdloop(), "NanUI Commander getch()")

        # BRAIN
        self.bus.connect("BRAIN_UP", self.brain_up)
        self.bus.connect("BRAIN_DOWN", self.brain_down)
        self.bus.connect("BRAIN_PROCESS", self.brain_process)
        self.bus.connect("BRAIN_FINISH", self.brain_finish)

        self.bus.connect("BRAIN_ALOWTAPE_LOAD", self.brain_alowtape_load)
        self.bus.connect("BRAIN_ASKING", self.brain_asking)
        self.bus.connect("BRAIN_RESPONSE", self.brain_response)
        self.bus.connect("BRAIN_NO_RESPONSE", self.brain_no_response)

        # EAR
        self.bus.connect("EAR_UP", self.ear_up)
        self.bus.connect("EAR_DOWN", self.ear_down)
        self.bus.connect("EAR_PROCESS", self.ear_process)
        self.bus.connect("EAR_FINISH", self.ear_finish)
        self.bus.connect("EAR_RECOGNIZE", self.ear_recognize)

        # VOICE
        self.bus.connect("VOICE_UP", self.voice_up)
        self.bus.connect("VOICE_DOWN", self.voice_down)
        self.bus.connect("VOICE_PROCESS", self.voice_process)
        self.bus.connect("VOICE_FINISH", self.voice_finish)

        self.bus.connect("VOICE_PIPER_START", self.voice_piper_start)
        self.bus.connect("VOICE_PIPER_FINISH", self.voice_piper_finish)

        self.bus.connect("VOICE_PLAYER_START", self.voice_player_start)
        self.bus.connect("VOICE_PLAYER_FINISH", self.voice_player_finish)
        self.bus.connect("VOICE_PLAYER_OUTPUT_OVERFLOW", self.voice_player_output_overflow)
        self.bus.connect("VOICE_PLAYER_BUFFER_EMPTY", self.voice_player_buffer_empty)
        self.bus.connect("VOICE_PLAYER_QUEUE_FULL", self.voice_player_queue_full)
        self.bus.connect("VOICE_PLAYER_EXCEPTION", self.voice_player_exception)
        self.bus.connect("VOICE_PLAYER_KEYBOARD_INTERRUPT", self.voice_player_keyboard_interrupt)

        self.bus.connect("VOICE_SPEECH", self.voice.speech)

        # start the asyncio loop
        try:
            self.brain.setup()
            self.voice.setup()
            self.ear.setup()
            self.loop.run_forever()
        except KeyboardInterrupt:
            pass
        except EOFError:
            pass
        finally:
            self.ear.teardown()
            self.voice.teardown()
            self.brain.teardown()
            self.bus.clear()
            self.tasks.cancel()
            self.loop.stop()
            string_to_log = f"{self.__class__.__name__}: is close"
            logging.debug(string_to_log)

    def super_control(self, text):
        if f"{text}" in [
            "recharge les modules",
            "charge les modules",
            "recharge des modules",
        ]:
            self.brain.alowtapes_reload()
            msg = "C'est fait ..."
            self.print_output(f"{self.brain.storage.session_name}: {msg}\n")
            self.bus.emit("VOICE_SPEECH", f"{msg}")
        elif f"{text.lower()}" in [
            "ferme tout",
            "femme tout",
            "kit",
            "ferme toi",
            "ferme tôt",
        ]:
            self.stop()
        elif text in [
            "repart à zéro",
            "repart de zéro",
            "repars de zéro",
            "repars à zéro",
            "remise à zéro",
            "tout ceci n'a pas existé",
        ]:
            msg = "Remise à zéro de la session"
            self.print_output(f"{self.brain.storage.session_name}: {msg}\n")
            self.bus.emit("VOICE_SPEECH", f"{msg}")
            self.brain.brain_reset()
            self.brain.session_save()

    # brain
    def brain_up(self):
        self.storage.states.brain.up = True
        if self.storage.debug:
            self.print_output("Brain: up\n".upper())

    def brain_down(self):
        self.storage.states.brain.up = False
        if self.storage.debug:
            self.print_output("Brain: down\n".upper())

    def brain_process(self):
        self.storage.states.brain.process = True
        if self.storage.debug:
            self.print_output("Brain: process\n".upper())

    def brain_finish(self):
        self.storage.states.brain.process = False
        if self.storage.debug:
            self.print_output("Brain: finish\n".upper())

    def brain_response(self, text):
        if text:
            if self.storage.debug:
                self.print_output(f"BRAIN: RESPONSE -> {text}\n")
            self.print_output(f"{self.brain.storage.session_name}: {text}\n")
            self.bus.emit("VOICE_SPEECH", f"{text}")

    def brain_no_response(self, text):
        if text and self.storage.debug:
            self.print_output(f"BRAIN: {text.lstrip('\n')}\n")

    def brain_asking(self, text):
        if text and self.storage.debug:
            self.print_output(f"BRAIN: ASKING -> {text}\n")

    def brain_alowtape_load(self, text):
        if text and self.storage.debug:
            self.print_output(f"BRAIN: ALOWTAPE LOAD -> {text}\n")

    # Voice
    def voice_up(self):
        self.storage.states.voice.up = True
        if self.storage.debug:
            self.print_output("Voice: up\n".upper())

    def voice_down(self):
        self.storage.states.voice.up = False
        if self.storage.debug:
            self.print_output("Voice: down\n".upper())

    def voice_process(self):
        self.storage.states.voice.pause = True
        self.ear.storage.pause = True

    def voice_finish(self):
        self.storage.states.voice.pause = False
        self.ear.storage.pause = False

    def voice_speech(self, text):
        self.voice.speech(text)

    def voice_piper_start(self, text):
        if self.storage.debug:
            self.print_output("Voice: piper generate wave file\n".upper())
        # self.print_output(f"{text}\n")

    def voice_piper_finish(self, text):
        # self.print_output(f"{text}\n")
        if self.storage.debug:
            self.print_output("Voice: piper finish wave file generation\n".upper())

    def voice_player_start(self, text):
        if self.storage.debug:
            self.print_output(f"VOICE: PLAYER PLAY -> {text}\n")

    def voice_player_finish(self):
        if self.storage.debug:
            self.print_output("Voice: player have finish to play\n".upper())

    def voice_player_output_overflow(self, text):
        if self.storage.debug:
            self.print_error(f"VOICE: PLAYER OUTPUT OVERFLOW ERROR: {text}\n")

    def voice_player_buffer_empty(self, text):
        if self.storage.debug:
            self.print_error(f"VOICE: PLAYER BUFFER EMPTY ERROR: {text}\n")

    def voice_player_queue_full(self, text):
        if self.storage.debug:
            self.print_error(f"VOICE: PLAYER QUEUE FULL ERROR: {text}\n")

    def voice_player_keyboard_interrupt(self, text):
        if self.storage.debug:
            self.print_error(f"VOICE: PLAYER KEYBOARD INTERRUPTION: {text}\n")

    def voice_player_exception(self, text):
        if self.storage.debug:
            self.print_error(f"VOICE: PLAYER EXCEPTION: {text}\n")

    # ear
    def ear_up(self):
        self.storage.states.ear.up = True
        if self.storage.debug:
            self.print_output("Ear: up\n".upper())

    def ear_down(self):
        self.storage.states.ear.up = False
        if self.storage.debug:
            self.print_output("Ear: down\n".upper())

    def ear_process(self):
        self.storage.states.ear.process = True
        if self.storage.debug:
            self.print_output("Ear: LISTENING\n".upper())

    def ear_finish(self):
        self.storage.states.ear.process = False
        if self.storage.debug:
            self.print_output("Ear: DO NOT LISTEN ANY MORE\n".upper())

    def ear_recognize(self, text):
        self.super_control(text)
        self.do_ask(text)

    # Do Commands
    def default(self, line):
        self.do_ask(line)

    @WrapperCmdLineArgParser(parser_start)
    def do_start(self, _, __):
        if self.interactive:
            self.storage.proc = Process(
                target=self.storage.app.run,
                kwargs={
                    "host": self.storage.host,
                    "port": int(self.storage.port),
                    "debug": self.storage.debug,
                    "use_reloader": self.storage.use_reloader,
                },
                daemon=True,
            )
            self.storage.states.backbone.process = True
            self.storage.proc.start()

        else:
            self.storage.states.backbone.process = True
            self.storage.app.run(
                host=self.storage.host,
                port=int(self.storage.port),
                debug=self.storage.debug,
                use_reloader=True,
            )

    def help_start(self):
        parser_start.print_help()

    def do_ask(self, line):
        if line:
            self.print_output(f"Human: {line.capitalize()}\n")
            self.brain.ask(line)

    def help_ask(self):
        parser_ask.print_help()

    @WrapperCmdLineArgParser(parser_clear)
    def do_clear(self, _, __):
        rc = 0
        try:
            self.tty.clear()
        except (Exception, ArithmeticError) as error:  # pragma: no cover
            sys.stderr.write("clear: %s\n" % error)
            rc += 1

        self.rc = rc

    def help_clear(self):
        parser_clear.print_help()
        self.rc = 0

    @WrapperCmdLineArgParser(parser_exit)
    def do_exit(self, _, parsed):
        self.rc = 0 if parsed.code is None or not parsed.code else parsed.code[0]
        return True

    def help_exit(self):
        parser_exit.print_help()
        self.rc = 0

    def do_set(self, line):
        rc = 0
        if line:
            try:
                name, value = line.split()
                setattr(self.storage, name, value)
            except AttributeError:
                sys.stdout.write("%s\n" % error_code_to_text(6))
                rc += 1
            except ValueError:
                sys.stdout.write("%s\n" % error_code_to_text(22))
                rc += 1
        else:
            for key, value in asdict(self.storage).items():
                sys.stdout.write(f"{key}={value}\n")

        self.rc = rc

    def help_set(self):
        parser_set.print_help()
        self.rc = 0

    @WrapperCmdLineArgParser(parser_status)
    def do_status(self, _, __):
        if self.storage.proc and self.storage.proc.is_alive():
            pid_status_path = f"/proc/{self.storage.proc.pid}/status"

            last_time = os.path.getctime(pid_status_path)
            now = time.time() - last_time

            self.print_output(f"Backbone: Up ({int(now % 60)} secs)\n")
            self.print_output(f"Created: {time.asctime(time.localtime(os.path.getctime(pid_status_path)))} \n")
            with open(pid_status_path) as status_file:
                for line in status_file.readlines():
                    for searching in ["Name", "pid", "ppid", "State"]:
                        if line.lower().startswith(searching.lower()):
                            if line.lower().startswith("pid"):
                                to_output = line.strip("\n")
                                self.print_output(f"{to_output} (backbone)\n")
                            elif line.lower().startswith("ppid"):
                                to_output = line.strip("\n")
                                self.print_output(f"{to_output} (shell)\n")
                            else:
                                self.print_output(f"{line}")

            self.print_output(f"Host: {self.storage.host}\n")
            self.print_output(f"Port: {self.storage.port}\n")

        else:
            self.print_output("Server is a down\n")
            try:
                self.exit_code = self.storage.proc.exitcode or 0
            except AttributeError:
                self.exit_code = 0

    def help_status(self):
        parser_status.print_help()

    @WrapperCmdLineArgParser(parser_stop)
    def do_stop(self, _, __):
        self.rc = 0
        self.storage.states.backbone.process = False
        try:
            self.storage.proc.terminate()
            self.storage.proc.join()

        except AttributeError:
            self.print_output("Server not running")
        if hasattr(self.storage.proc, "exitcode") and self.storage.proc.exitcode is not None:
            self.rc += self.storage.proc.exitcode

    def help_stop(self):
        parser_stop.print_help()


@define
class StorageState:
    up: bool = False
    pause: bool = False
    process: bool = False


@define
class StorageStates:
    backbone: StorageState = StorageState()
    brain: StorageState = StorageState()
    ear: StorageState = StorageState()
    eye: StorageState = StorageState()
    voice: StorageState = StorageState()


@define
class StorageBackBone:
    proc: Process = None
    """the process object where is stored the web server"""

    app: AppBackbone = None
    """the flask application object where is stored AppBackbone"""

    host: str = "127.0.0.1"
    """the hostname to listen on."""

    port: str = "5000"
    """the port of the web server."""

    use_reloader: bool = False
    """should the server automatically restart the python process if modules were changed?"""

    brains: dict = {0: Brain()}
    eyes: list = []
    voices: list = {0: Voice()}
    ears: list = {0: Ear()}
    application_license: str = "License WTFPL v2"
    application_name: str = "galaxie-little-alice backbone"
    application_version: str = "0.1a"

    debug: bool = True
    """if given, enable or disable debug mode."""

    states: StorageStates = StorageStates()


def main():
    if len(sys.argv) > 1:
        args = parser_gia_backbone.parse_args(sys.argv[1:])
        # if args.help:
        #     parser_gia_backbone.print_help()
        #     return 0

        if os.path.isfile(args.command):
            try:
                with open(args.command) as rc_file:
                    exit_code = 0
                    for line in rc_file.readlines():
                        line = line.rstrip()
                        if len(line) > 0 and line[0] != "#":
                            exit_code = AppBackbone().onecmdhooks("%s" % line)
                        if exit_code != 0:
                            break
                    return exit_code
            except IOError:
                return 1
            else:
                if not os.isatty():
                    sys.stdout.write("Process are not connected to a terminal (TTY)")
                return 1

        else:
            return AppBackbone().onecmdhooks("%s %s" % (args.command, " ".join(args.command_args)))

    else:
        return AppBackbone().start()


if __name__ == "__main__":
    sys.exit(main())
