import asyncio

import logging
from asyncio import CancelledError
from asyncio import Task
from asyncio import coroutines
from collections import OrderedDict
from itertools import count


class TaskManager:
    def __init__(self, loop):
        self._loop = loop
        self._name = self.__class__.__name__
        self._tasks = OrderedDict()
        self._task_counter = count()

    def add(
        self,
        coro: coroutines,
        description: str = "No description",
        handle_exceptions: bool = True,
    ) -> Task:
        """
        Wrapper around asyncio.create_task - takes care of canceling tasks on shutdown

        :param coro: a coroutine
        :type: asyncio.coroutine
        :param description: A description usefully for logs and debug
        :type: str
        :param handle_exceptions: if True Handle error on logs else raise a Exception
        :type handle_exceptions: bool
        :return: The asyncio task freshly created
        :rtype: asyncio.Task
        """

        async def task_wrapper(task_id: int) -> None:  # pragma: no cover
            """
            The task wrapper

            Care about raise and logs error

            :param task_id: a unique ID it represents the task (Uee for debug and logs)
            :type task_id: int
            """
            try:
                if asyncio.iscoroutine(coro):
                    result = await coro
                    logging.debug(
                        "%s: finished task %d (%s)", self._name, task_id, description
                    )

            except CancelledError:
                if handle_exceptions:

                    logging.warning(
                        "%s: canceled task %d (%s)",
                        self._name,
                        task_id,
                        description,
                    )
                else:
                    raise
            except (Exception, BaseException):
                if handle_exceptions:
                    logging.exception(
                        "%s: exception raised in task %d",
                        self._name,
                        task_id,
                    )
                else:
                    raise
            finally:
                del self._tasks[task_id]

        task_id = next(self._task_counter)
        logging.debug("%s: creating task %d (%s)", self._name, task_id, description)
        self._tasks[task_id] = self._loop.create_task(
            task_wrapper(task_id), name=f"{task_id}"
        )
        return self._tasks[task_id]

    def cancel(self) -> int:
        """
        Try to cancel every tasks inside the TaskManager, the approch look naive ... In fact that is responssability of
        task wrapper to care about Cancel and Raise.

        It function send a cancel and dont wait or verify if the task is really cancel


        :return: The number of time a cancel have been send to tasks, 0 mean no task to cancel
        :rtype: int
        """
        task_count = 0
        for task in self._tasks.values():
            task.cancel()
            task_count += 1

        return task_count


# Be here for memory, but it can be delete as well
# async def wait(self) -> None:
#     """
#     Tasks can spawn other tasks inside the TaskManager context
#
#     Example:
#
#     :return: Nothing
#     :rtype: None
#     """
#
#     while True:
#         tasks = self._tasks.values()
#         if not tasks:
#             return
#         await gather(*tasks, return_exceptions=True)
