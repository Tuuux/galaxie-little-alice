import logging
from typing import Callable, Any
from attrs import define, field, validators


class InvalidEventError(Exception):
    def __init__(self, event: Any) -> None:
        super().__init__(f'Expected type "str" but got type "{type(event).__name__}" instead')


class InvalidCallbackError(Exception):
    def __init__(self, callback: Any) -> None:
        super().__init__(f'Expected type "Callable" but got type "{type(callback).__name__}" instead')


class InvalidPriorityError(Exception):
    def __init__(self, priority: Any) -> None:
        super().__init__(f'Expected type "int" but got type "{type(priority).__name__}" instead')


@define(slots=True)
class EventBus:
    Subscribers: dict[str, list[dict]] = field(validator=validators.instance_of(dict), default={})

    def connect(self, event: str, callback: Callable, /, *, priority: int = 1) -> None:
        """
        Subscribes a method to an event.

        Example
        =======

        .. code-block:: python
          :emphasize-lines: 2

          def on_exemple_event():
              print("Example event called")


          bus = EventBus()
          bus.connect("example_event", on_example_event)


        :param event: The event that the method will be subscribed to.
        :type event: str
        :param callback: The method that will be subscribed.
        :type callback: Callable
        :param priority: Priority of the callback. Higher values indicate higher priority. Defaults to 1.
        :type priority: int
        :raise InvalidEventError: When event argument is not str instance
        :raise InvalidCallbackError: When callback argument is not Callable instance
        :raise InvalidPriorityError: When priority argument is not int instance
        """
        if not isinstance(event, str):
            raise InvalidEventError(event)  # noqa: DOC501
        if not isinstance(callback, Callable):
            raise InvalidCallbackError(callback)  # noqa: DOC501
        if not isinstance(priority, int):
            raise InvalidPriorityError(priority)  # noqa: DOC501

        if event not in self.Subscribers:
            self.Subscribers[event] = []

        self.Subscribers[event].append({"callback": callback, "priority": priority})
        self.Subscribers[event].sort(key=lambda subscriber: subscriber["priority"], reverse=True)

        logging.debug(f'EventBus: subscription event: "{event}", callback: {callback}, priority: {priority}')

    def remove(self, event: str, callback: Callable, /) -> None:
        """
        Unsubscribes a method from an event.

        Example
        =======

        .. code-block:: python
          :emphasize-lines: 2

          def on_exemple_event():
              print("Example event called")


          bus = EventBus()
          bus.connect("example_event", on_example_event)
          bus.remove("example_event", on_example_event)


        :param event: The event that the method will be unsubscribed from.
        :type event: str
        :param callback: The method that will be unsubscribed.
        :type callback: Callable
        :raise InvalidEventException: When event argument is not str instance
        :raise InvalidCallbackException: When callback argument is not Callable instance
        """
        if not isinstance(event, str):
            raise InvalidEventError(event)  # noqa: DOC501
        if not isinstance(callback, Callable):
            raise InvalidCallbackError(callback)  # noqa: DOC501

        if event in self.Subscribers:
            subscribers = self.Subscribers[event]
            for subscriber in subscribers:
                if subscriber["callback"] == callback:
                    subscribers.remove(subscriber)
                    logging.debug(f'EventBus: remove subscription for "{event}": {callback}')
                    break

        # If a subscription have a empty callback list delete the subscription
        empty_keys = [k for k, v in self.Subscribers.items() if not v]
        if empty_keys:
            for k in empty_keys:
                del self.Subscribers[k]

    def emit(self, event: str, /, *args, **kwargs) -> int:
        """
        Calls an event with optional data, invoking subscribed callbacks.

        Example
        =======

        .. code-block:: python
          :emphasize-lines: 2

          def on_exemple_event():
              print("Example event called")


          bus = EventBus()
          bus.connect("example_event", on_example_event)
          count: int = bus.emit("example_event")
          print(f"{count} subscriber(s) were called for the event 'example_event'")


        :param event: The event to be called.
        :type event: str
        :param args: Additional positional arguments to be passed to the callbacks.
        :param kwargs: Additional keyword arguments to be passed to the callbacks.
        :return: The number of event subscribers that were successfully called.
        :rtype: int
        :raise InvalidEventException: When event argument is not str instance
        """
        if not isinstance(event, str):
            raise InvalidEventError(event)  # noqa: DOC501

        call_count: int = 0

        if event in self.Subscribers:
            for subscriber in self.Subscribers[event]:
                subscriber["callback"](*args, **kwargs)
                # if event != 'pixel-draw':
                #     logging.debug(
                #         f'EventBus: call {subscriber["callback"]}: with arg:"{args}", kwargs:"{kwargs}"'
                #     )
                call_count += 1

        return call_count

    def clear(self) -> int:
        """
        Clear the event bus by call remove for every subscription

        .. code-block:: python
          :emphasize-lines: 2

          >>> bus = EventBus()
          >>> def on_exemple_event_1():
          ...    pass
          ...
          >>> def on_exemple_event_2():
          ...     pass
          ...
          >>> bus.connect("example_event", on_exemple_event_1)
          >>> bus.connect("example_event", on_exemple_event_2)
          >>> count: int = bus.clear()
          >>> count
          1
          >>> bus.connect("example_event1", on_exemple_event_1)
          >>> bus.connect("example_event2", on_exemple_event_2)
          >>> count: int = bus.clear()
          >>> count
          2


        :return: The number of event subscribers that were successfully remove.
        :rtype: int
        """
        removed_count: int = 0
        subscriptions = dict(self.Subscribers)

        for event in subscriptions:
            for subscriber in self.Subscribers[event]:
                self.remove(event, subscriber["callback"])
            removed_count += 1
        return removed_count
