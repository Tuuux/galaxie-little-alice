#!/usr/bin/env python
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Little Alice Team, all rights reserved

import random
import re


def new_id():
    """
    Generate a ID like 'E59E8457', two chars by two chars it's a random HEX

    **Default size:** 8
    **Default chars:** 'ABCDEF0123456789'

    **Benchmark**
       +----------------+---------------+----------------------------------------------+
       | **Iteration**  | **Duration**  | **CPU Information**                          |
       +----------------+---------------+----------------------------------------------+
       | 10000000       | 99.114s       | Intel(R) Core(TM) i7-2860QM CPU @ 2.50GHz    |
       +----------------+---------------+----------------------------------------------+
       | 1000000        | 9.920s        | Intel(R) Core(TM) i7-2860QM CPU @ 2.50GHz    |
       +----------------+---------------+----------------------------------------------+
       | 100000         | 0.998s        | Intel(R) Core(TM) i7-2860QM CPU @ 2.50GHz    |
       +----------------+---------------+----------------------------------------------+
       | 10000          | 0.108s        | Intel(R) Core(TM) i7-2860QM CPU @ 2.50GHz    |
       +----------------+---------------+----------------------------------------------+

    :return: a string it represent a unique ID
    :rtype: str
    """
    return "%02x%02x%02x%02x".upper() % (
        random.randint(0, 255),
        random.randint(0, 255),
        random.randint(0, 255),
        random.randint(0, 255),
    )


def is_valid_id(value):
    """
    Check if it's a valid id

    :param value: a id to verify
    :return: bool
    """
    allowed = re.compile(
        r"""
                         (
                             ^([0-9A-F]{8})$
                         )
                         """,
        re.VERBOSE | re.IGNORECASE,
    )
    try:
        if allowed.match(value) is None:
            return False
        else:
            return True
    except TypeError:
        return False
