import logging


class CustomFormatter(logging.Formatter):
    """
    Custom Format
    """

    grey = "\x1b[38;20m"
    yellow = "\x1b[33;20m"
    red = "\x1b[31;20m"
    bold_red = "\x1b[31;1m"
    reset = "\x1b[0m"
    format = (
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s (%(filename)s:%(lineno)d)"
    )

    FORMATS = {
        logging.DEBUG: grey + format + reset,
        logging.INFO: grey + format + reset,
        logging.WARNING: yellow + format + reset,
        logging.ERROR: red + format + reset,
        logging.CRITICAL: bold_red + format + reset,
    }

    def format(self, record: logging.LogRecord) -> str:
        """
        The format

        :param record: The record
        :type record: logging.LogRecord
        :return: formatter
        :rtype: str
        """
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)


class CursesHandler(logging.Handler):
    def __init__(self, shell):
        logging.Handler.__init__(self)
        self.shell = shell

    def emit(self, record):
        msg = f"{self.format(record)}"
        msg = msg.strip()
        msg = msg.replace("^[31m", "")
        msg = msg.replace("^[1m", "")
        msg = msg.replace("^[33m", "")
        msg = msg.replace("^[0m", "")
        self.shell.print_output(f"{msg}\n")

        # return
