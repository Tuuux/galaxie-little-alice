import sys
import os
import asyncio
import logging

from tabulate import tabulate
from attrs import define
from argparse import ArgumentParser
from argparse import ONE_OR_MORE
from flask import Flask

from nan.core.tasks import TaskManager
from nan.ui.ncmd import NanUICmd
from nan.ui import WrapperCmdLineArgParser
from nan.api.alowtape import AlowTapeManager

# alowtape
parser_alowtape = ArgumentParser(
    prog="alowtape",
    description="A alowtape utility to manipulate alowtape file",
    add_help=True,
)
parser_alowtape.add_argument(
    "command", nargs="?", help="optional commands or file to run, if no commands given, enter an interactive shell"
)
parser_alowtape.add_argument(
    "command_args", nargs="...", help="if commands is not a file use optional arguments for commands"
)


# install
parser_install = ArgumentParser(
    prog="install",
    description="install a alowtape from file or file object",
    add_help=False,
)
parser_install.add_argument("FILE", nargs=ONE_OR_MORE, help="alowtape file or file object")

# remove
parser_remove = ArgumentParser(
    prog="remove",
    description="remove a alowtape by name",
    add_help=False,
)
parser_remove.add_argument("ALOWTAPE", nargs=ONE_OR_MORE, help="a alowtape name")

# info
parser_info = ArgumentParser(
    prog="info",
    description="print installed alowtape information",
    add_help=False,
)
parser_info.add_argument("ALOWTAPE", help="a alowtape name")

# list
parser_list = ArgumentParser(
    prog="list",
    description="list installed alowtape",
    add_help=False,
)


class AppAlowTapeManager(NanUICmd, AlowTapeManager):
    def __init__(self, **kwargs):
        super().__init__()
        self.storage = kwargs.get("storage", StorageAlowTapeManager())
        # Default context
        self.storage.app = kwargs.get("app", Flask(__name__))
        self.storage.app.app_context().push()

    @property
    def prompt(self):
        return f"{self.rc}> " if self.rc else "> "

    def preloop(self):
        self.interactive = True

    def emptyline(self):
        pass

    def default(self, line):
        self.print_output(f"{self.storage.application_name.lower()}: {line} unknow command\n")
        self.rc = 127

    async def __async_cmdloop(self):
        await self.cmdloop()

    def start(self):
        self.loop = asyncio.new_event_loop()
        asyncio.set_event_loop(self.loop)
        self.tasks = TaskManager(self.loop)

        self.tasks.add(self.__async_cmdloop(), "NanUI Commander getch()")

        # start the asyncio loop
        try:
            self.loop.run_forever()
        except KeyboardInterrupt:
            pass
        except EOFError:
            pass
        finally:
            self.bus.clear()
            self.tasks.cancel()
            self.loop.stop()
            string_to_log = f"{self.__class__.__name__}: is close"
            logging.debug(string_to_log)

    # Do Commands

    @WrapperCmdLineArgParser(parser_install)
    def do_install(self, _, parsed):
        response = self.install(sources=parsed.FILE)

        if response[1] == 200:
            self.rc = 0
            for added in response[0].json["installed"]:
                self.print_output(f"{added["name"]}: {added["info"]}\n")
        else:
            self.rc = 1
            self.print_error(f"{self.storage.application_name.lower()}: {response[0].json['message']}\n")

    def help_install(self):
        parser_install.print_help()

    @WrapperCmdLineArgParser(parser_info)
    def do_info(self, _, parsed):
        tabular_info = []
        tabular_data = []
        tabular_errors = []
        response = self.info(alowtape=parsed.ALOWTAPE)

        if response[1] == 200:
            for key, value in response[0].json["alowtape_info"].items():
                tabular_info.append([str(key).upper(), value])

            tabular_info.append(["FILE NAME", response[0].json["alowtape_stats"]["file"]])
            tabular_info.append(["FILE SIZE", response[0].json["alowtape_stats"]["size"]])
            tabular_info.append(["DATE CREATION", response[0].json["alowtape_stats"]["creation"]])
            tabular_info.append(["DATE MODIFIED", response[0].json["alowtape_stats"]["modified"]])
            tabular_info.append(["DATE ACCESSED", response[0].json["alowtape_stats"]["accessed"]])
            tabular_info.append(["MEMBERS", response[0].json["alowtape_stats"]["content"]])

            for member in response[0].json["alowtape_members"]:
                tabular_data.append([
                    member["mode"],
                    member["owner"],
                    member["group"],
                    member["size"],
                    member["date_and_time"],
                    member["pathname"],
                    member["link_info"],
                ])

            for member in response[0].json["alowtape_members"]:
                if member["error"]:
                    tabular_errors.append([
                        member["pathname"],
                        member["error"],
                    ])

            self.print_output(f"{
                tabulate(
                    tabular_data=tabular_info,
                    headers=[],
                    tablefmt="plain",
                    colalign=("left", "left"),
                )
            }\n")

            self.print_output(f"{
                tabulate(
                    tabular_data=tabular_data,
                    headers=[],
                    tablefmt="plain",
                    colalign=("left", "left", "left", "right", "right", "left", "left"),
                )
            }\n")

            if len(tabular_errors):
                self.print_output("ERRORS:\n")
                self.print_output(f"{
                    tabulate(
                        tabular_data=tabular_errors,
                        headers=[],
                        tablefmt="plain",
                        colalign=("left", "right"),
                    )
                }\n")

        else:
            self.print_error(f"{self.storage.application_name.lower()}: {response[0].json['message']}")

        self.rc = 0 if response[1] == 200 else 1

    def help_info(self):
        parser_info.print_help()

    @WrapperCmdLineArgParser(parser_remove)
    def do_remove(self, _, parsed):
        """
        Remove a entry inside a AlowTape
        """
        response = self.remove(sources=parsed.ALOWTAPE)

        if response[1] == 200:
            self.rc = 0
            for removed_entry in response[0].json["removed"]:
                self.print_output(f"{removed_entry}\n")
        else:
            self.rc = 1
            self.print_error(f"{self.storage.application_name.lower()}: {response[0].json['message']}\n")

    def help_remove(self):
        parser_remove.print_help()

    @WrapperCmdLineArgParser(parser_list)
    def do_list(self, _, parsed):
        response = self.list()

        if response[1] == 200:
            self.rc = 0
            if len(response[0].json["list"]):
                for installed in response[0].json["list"]:
                    self.print_output(f"{installed["name"]}-{installed["version"]}\n")
            else:
                self.print_output("no alowtape found\n")
        else:
            self.rc = 1
            self.print_error(f"{self.storage.application_name.lower()}: {response[0].json['message']}\n")

    def help_list(self):
        parser_list.print_help()


@define
class StorageAlowTapeManager:
    app: AppAlowTapeManager = None
    debug: bool = True
    process: bool = False
    up: bool = False
    application_license: str = "License WTFPL v2"
    application_name: str = "AlowTape"
    application_version: str = "0.1a"


def main():
    if len(sys.argv) > 1:
        args = parser_alowtape.parse_args(sys.argv[1:])
        # if args.help:
        #     parser_alowtape.print_help()
        #     return 0

        if os.path.isfile(args.command):
            try:
                with open(args.command) as rc_file:
                    exit_code = 0
                    for line in rc_file.readlines():
                        line = line.rstrip()
                        if len(line) > 0 and line[0] != "#":
                            exit_code = AppAlowTapeManager().onecmdhooks("%s" % line)
                        if exit_code != 0:
                            break
                    return exit_code
            except IOError:
                return 1
            else:
                if not os.isatty():
                    sys.stdout.write("Process are not connected to a terminal (TTY)")
                return 1

        else:
            return AppAlowTapeManager().onecmdhooks("%s %s" % (args.command, " ".join(args.command_args)))

    else:
        return AppAlowTapeManager().start()


if __name__ == "__main__":
    sys.exit(main())
