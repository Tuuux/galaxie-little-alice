import string
import os

import sys
import curses
import curses.ascii
import asyncio
import contextlib
import logging
from argparse import ArgumentParser

from nan.core.eventbus import EventBus
from nan.core.tasks import TaskManager

from nan.ui.screen import Screen
from nan.ui.painter import Painter
from nan.ui.nreadline import ReadLine
from nan.ui.ntty import TTY
from nan.ui.ntty import captured_stdout
from nan.ui.ntty import captured_stderr
from nan.ui import WrapperCmdLineArgParser

"""A generic class to build line-oriented command interpreters.

Interpreters constructed with this class obey the following conventions:

1. End of file on input is processed as the command 'EOF'.
2. A command is parsed out of each line by collecting the prefix composed
   of characters in the identchars member.
3. A command `foo' is dispatched to a method 'do_foo()'; the do_ method
   is passed a single argument consisting of the remainder of the line.
4. Typing an empty line repeats the last command.  (Actually, it calls the
   method `emptyline', which may be overridden in a subclass.)
5. There is a predefined `help' method.  Given an argument `topic', it
   calls the command `help_topic'.  With no arguments, it lists all topics
   with defined help_ functions, broken into up to three topics; documented
   commands, miscellaneous help topics, and undocumented commands.
6. The command '?' is a synonym for `help'.  The command '!' is a synonym
   for `shell', if a do_shell method exists.
7. If completion is enabled, completing commands will be done automatically,
   and completing of commands args is done by calling complete_foo() with
   arguments text, line, begidx, endidx.  text is string we are matching
   against, all returned matches must begin with it.  line is the current
   input line (lstripped), begidx and endidx are the beginning and end
   indexes of the text being matched, which could be used to provide
   different completion depending upon which position the argument is in.

The `default' method may be overridden to intercept commands for which there
is no do_ method.

The `completedefault' method may be overridden to intercept completions for
commands that have no complete_ method.

The data member `self.ruler' sets the character used to draw separator lines
in the help messages.  If empty, no ruler line is drawn.  It defaults to "=".

If the value of `self.intro' is nonempty when the cmdloop method is called,
it is printed out on interpreter startup.  This value may be overridden
via an optional argument to the cmdloop() method.

The data members `self.doc_header', `self.misc_header', and
`self.undoc_header' set the headers used for the help function's
listings of documented functions, miscellaneous topics, and undocumented
functions respectively.
"""


parser_help = ArgumentParser(
    prog="help",
    description="The help utility shall write information about each of the name operands.",
    add_help=True,
)
parser_help.add_argument(
    "name",
    nargs="?",
    help="A keyword or the name of a standard utility.",
)

PROMPT = "(Cmd) "
IDENTCHARS = string.ascii_letters + string.digits + "_"


class NoParsingFilter(logging.Filter):
    def filter(self, record):
        return not record.getMessage().startswith("parsing")


class NanUICmd:
    """A simple framework for writing line-oriented command interpreters.

    These are often useful for test harnesses, administrative tools, and
    prototypes that will later be wrapped in a more sophisticated interface.

    A Cmd instance or subclass instance is a line-oriented interpreter
    framework.  There is no good reason to instantiate Cmd itself; rather,
    it's useful as a superclass of an interpreter class you define yourself
    in order to inherit Cmd's methods and encapsulate action methods.

    """

    prompt = PROMPT
    identchars = IDENTCHARS
    ruler = "="
    lastcmd = ""
    intro = None
    doc_leader = ""
    doc_header = "Documented commands (type help <topic>):"
    misc_header = "Miscellaneous help topics:"
    undoc_header = "Undocumented commands:"
    nohelp = "*** No help on %s"
    use_rawinput = 1

    def __init__(self, completekey="tab", **kwargs):
        """Instantiate a line-oriented interpreter framework.

        The optional argument 'completekey' is the readline name of a
        completion key; it defaults to the Tab key. If completekey is
        not None, command completionis done automatically.

        """
        self.logger = logging.getLogger()
        self.logger.name = "microGUI"
        # self.logger.setLevel(logging.DEBUG)

        # create console handler and set level to debug
        # ch = logging.StreamHandler(sys.stdout)
        # ch.setLevel(logging.ERROR)

        # add formatter to ch
        # ch.setFormatter(CustomFormatter())

        # add ch to logger
        # self.logger.addHandler(ch)

        # file_handler = logging.FileHandler("logs.log")
        # file_handler.setLevel(logging.DEBUG)
        # file_handler.setFormatter(CustomFormatter())
        # self.logger.addHandler(file_handler)

        self.cmdqueue = []
        self.completekey = completekey

        # NanUICommander specific setting
        self.bus = kwargs.get("bus", EventBus())
        self.loop = None
        self.tasks = None

        self.tty = TTY()
        self.readline = ReadLine()

        self.interactive = False
        self.completion_matches = []

        # Return code
        self.rc = 0

        self.running = False

        self.rc_file = ".nanuishell"
        self.logout_file = ".nanuishell_logout"
        self.history_path = os.path.expanduser("~%s.nanuishell_history" % os.path.sep)

        # If the historyfile do not exist create it
        if not os.path.exists(self.history_path):
            with open(self.history_path, "a+") as file:
                file.close()
        # Load history
        self.readline.read_history_file(self.history_path)

    def start(self):
        self.loop = asyncio.get_event_loop()
        self.tasks = TaskManager(self.loop)
        self.tasks.add(self.__async_cmdloop(), "NanUI Commander getch()")

        # start the asyncio loop
        try:
            self.loop.run_forever()
        except KeyboardInterrupt:
            pass
        except EOFError:
            pass
        finally:
            self.stop()

    def stop(self):
        self.running = False
        if self.bus and hasattr(self.bus, "clear"):
            self.bus.clear()
        if self.tasks and hasattr(self.tasks, "cancel"):
            self.tasks.cancel()
        if self.loop and hasattr(self.loop, "stop"):
            self.loop.stop()
        if hasattr(self, "screen") and self.screen and hasattr(self.screen, "close"):
            self.screen.close()
        logging.debug(f"{self.__class__.__name__}: is close")

        return self.rc

    def paint(self):
        self.painter.subwin.erase()

        # Viewer
        for row, line in enumerate(self.tty.buffer[self.tty.pager.row : self.tty.pager.row + self.tty.pager.n_rows]):
            if row == self.tty.cursor.pos.y - self.tty.pager.row and self.tty.pager.col > 0:
                line = "«" + line[self.tty.pager.col :]
            if len(line) > self.tty.pager.n_cols:
                line = line[: self.tty.pager.n_cols] + "»"

            self.painter.text(
                x=0,
                y=row,
                text=f"{line[: self.tty.pager.n_cols]}",
                colour=None,
            )

            # Cursor line for TTY
            # for pos in range(0, self.tty.pager.n_cols):
            #     self.painter.stdscr.chgat(
            #         self.tty.pager.translate()[0],
            #         pos,
            #         curses.A_REVERSE | curses.A_NORMAL,
            #     )

        # Prompt
        line = self.readline.buffer
        if len(f"{self.prompt}{line}") > self.readline.w - 1:
            line = line[: self.readline.w - len(self.prompt) - 1] + "»"

        self.painter.text(
            x=self.readline.x,
            y=self.readline.y,
            text=f"{self.prompt}{line}",
            colour=None,
        )
        with contextlib.suppress(curses.error):
            self.screen.stdscr.move(
                self.readline.y,
                self.readline.cursor_pos + len(self.prompt),
            )

    def resize(self):
        self.painter.resize()
        self.tty.pager.n_rows = self.painter.area.size.h - 1
        self.tty.pager.n_cols = self.painter.area.size.w - 1
        self.readline.y = self.painter.area.size.h - 1
        self.readline.w = self.painter.area.size.w - 1

    def refresh(self):
        self.paint()
        self.painter.refresh()

    def handle_resize(self):
        self.resize()
        self.refresh()

    def handle_mouse(self, event: tuple) -> None:
        if event[4] == 524288 or event[4] == 65536:
            [self.tty.up() for _ in range(3)]
            self.refresh()

        if event[4] == 134217728 or event[4] == 2097152:
            [self.tty.down() for _ in range(3)]
            self.refresh()

    def handle_curses(self, char: int) -> None:
        # in can_exit mode, Ctrl-D is hardwired to quit
        if char == 4:
            self.rc = -1
            self.stop()

        if char == curses.KEY_EXIT:  # Assuming KEY_EXIT is the equivalent for "\cG"
            self.buffer = None

        # if char == curses.KEY_ENTER or c == 10:  # Assuming 10 is the newline character
        #     break

        if char == curses.KEY_LEFT:
            self.readline.left()

        elif char == curses.KEY_RIGHT:
            self.readline.right()

        elif char in (curses.KEY_HOME, 1):  # Assuming 1 is the equivalent for "\cA"
            self.readline.home()

        elif char in (curses.KEY_END, 5):  # Assuming 1 is the equivalent for "\cE"
            self.readline.end()

        elif char == curses.KEY_UP:
            self.readline.up()

        elif char == curses.KEY_DOWN:
            self.readline.down()

        elif char == curses.KEY_PPAGE:
            self.tty.up()

        elif char == curses.KEY_NPAGE:
            self.tty.down()

        elif char in (curses.KEY_HOME, 1):  # Assuming 1 is the equivalent for "\cA"
            self.readline.home()

        elif char == curses.KEY_DC:
            self.readline.delete()

        elif char in (curses.KEY_BACKSPACE, "\x7f"):
            self.readline.backspace()

        elif char == curses.KEY_SR:  # Shift + UP
            self.tty.top()

        elif char == curses.KEY_SF:  # Shift + Down
            self.tty.bottom()

        # elif char == curses.KEY_SLEFT:  # Shift + Left
        #     pass
        # elif char == curses.KEY_SRIGHT:  # Shift + Right
        #     pass
        # elif char == 9:  # Tab
        #     pass
        # elif char == curses.KEY_IC:  # Insert
        #     pass
        # elif char == 575:  # Ctrl + Up
        #     pass
        # elif char == 534:  # Ctrl + Down
        #     pass
        # elif char == 554:  # Ctrl + Left
        #     pass
        # elif char == 569:  # Ctrl + Right
        #     pass
        # elif char == 564:  # Ctrl + PageUp
        #     pass
        # elif char == 559:  # Ctrl + PageDown
        #     pass

        else:
            self.readline.insert(char)

            self.refresh()

    async def __async_cmdloop(self):
        self.cmdloop()
        await asyncio.sleep(0.1)

    def print_output(self, text):
        if hasattr(self, "screen"):
            self.tty.insert(text)
            self.tty.bottom()
            self.refresh()
        else:
            sys.stdout.write(text)

    def print_error(self, text):
        if hasattr(self, "screen"):
            self.tty.insert(text)
            self.tty.bottom()
            self.refresh()
        else:
            sys.stderr.write(text)

    # CMD Clone
    async def cmdloop(self, intro=None, **kwargs):
        self.interactive = True
        self.running = True
        self.screen = kwargs.get("screen", Screen())
        self.painter = Painter(self.screen.stdscr, self.bus)
        curses.mousemask(curses.ALL_MOUSE_EVENTS | curses.REPORT_MOUSE_POSITION)

        self.bus.connect("CURSES", self.handle_curses)
        self.bus.connect("KEY_MOUSE", self.handle_mouse)
        self.bus.connect("KEY_RESIZE", self.handle_resize)

        self.resize()
        self.refresh()

        # enable to see cursor
        with contextlib.suppress(curses.error):
            curses.curs_set(1)

        # PRELOOP
        with captured_stdout() as stdout, captured_stderr() as stderr:
            self.preloop()
            if stdout.getvalue():
                self.print_output(stdout.getvalue())
            if stderr.getvalue():
                self.print_error(stderr.getvalue())

        try:
            # Print intro if found one
            if self.intro:
                self.print_output(self.intro)

            stop = None

            while not stop and self.running:
                if self.cmdqueue:
                    line = self.cmdqueue.pop(0)
                else:
                    event = self.screen.stdscr.getch()

                    if event == curses.ERR:
                        await asyncio.sleep(0.1)

                    # raises KeyboardInterrupt [raise KeyboardInterrupt] if h is 3 (^C)
                    if event == 3:
                        raise KeyboardInterrupt

                    # raises EOFError [raise EOFError] if h is 26 (^Z)
                    if event == 26:
                        raise EOFError

                    if event in (curses.KEY_ENTER, 10, 13):
                        line = self.readline.enter()
                        # if not len(line):
                        #     line = "EOF"
                        self.print_output(f"{self.prompt}{line}\n")

                        # PRECMD
                        with captured_stdout() as stdout, captured_stderr() as stderr:
                            try:
                                line = self.precmd(line)
                            except Exception as error:
                                sys.stderr.write(f"{error}\n")
                            if stdout.getvalue():
                                self.print_output(stdout.getvalue())
                            if stderr.getvalue():
                                self.print_error(stderr.getvalue())

                        # ONECMD
                        with captured_stdout() as stdout, captured_stderr() as stderr:
                            try:
                                stop = self.onecmd(line)
                            except Exception as error:
                                sys.stderr.write(f"{error}\n")
                            if stdout.getvalue():
                                self.print_output(stdout.getvalue())
                            if stderr.getvalue():
                                self.print_error(stderr.getvalue())

                        # POSTCMD
                        with captured_stdout() as stdout, captured_stderr() as stderr:
                            try:
                                stop = self.postcmd(stop, line)
                            except Exception as error:
                                sys.stderr.write(f"{error}\n")
                            if stdout.getvalue():
                                self.print_output(stdout.getvalue())
                            if stderr.getvalue():
                                self.print_error(stderr.getvalue())

                        if line:
                            self.readline.add_history(line)

                    if event == curses.KEY_MOUSE:  # pragma: no cover
                        mouse = curses.getmouse()
                        self.bus.emit("KEY_MOUSE", mouse)

                    elif event == curses.KEY_RESIZE:  # pragma: no cover
                        self.bus.emit("KEY_RESIZE")

                    else:  # pragma: no cover
                        self.bus.emit("CURSES", event)

            # POSTLOOP
            with captured_stdout() as stdout, captured_stderr() as stderr:
                try:
                    self.postloop()
                except Exception as error:
                    sys.stderr.write(f"{error}\n")
                if stdout.getvalue():
                    self.print_output(stdout.getvalue())
                if stderr.getvalue():
                    self.print_error(stderr.getvalue())
                self.postloop()
        except KeyboardInterrupt:
            self.print_output("^C")
        except EOFError:
            self.print_output("^Z")
            raise EOFError
        finally:
            self.readline.write_history_file(self.history_path)
            self.stop()

    def precmd(self, line):
        """Hook method executed just before the command line is
        interpreted, but after the input prompt is generated and issued.

        """
        return line

    def postcmd(self, stop, line):
        """Hook method executed just after a command dispatch is finished."""
        return stop

    def preloop(self):
        """Hook method executed once when the cmdloop() method is called."""
        pass

    def postloop(self):
        """Hook method executed once when the cmdloop() method is about to
        return.

        """
        pass

    def parseline(self, line):
        """Parse the line into a command name and a string containing
        the arguments.  Returns a tuple containing (command, args, line).
        'command' and 'args' may be None if the line couldn't be parsed.
        """
        line = line.strip()
        if not line:
            return None, None, line
        if line[0] == "?":
            line = "help " + line[1:]
        elif line[0] == "!":
            if hasattr(self, "do_shell"):
                line = "shell " + line[1:]
            else:
                return None, None, line
        i, n = 0, len(line)
        while i < n and line[i] in self.identchars:
            i = i + 1
        cmd, arg = line[:i], line[i:].strip()
        return cmd, arg, line

    def onecmd(self, line):
        """Interpret the argument as though it had been typed in response
        to the prompt.

        This may be overridden, but should not normally need to be;
        see the precmd() and postcmd() methods for useful execution hooks.
        The return value is a flag indicating whether interpretation of
        commands by the interpreter should stop.

        """
        cmd, arg, line = self.parseline(line)
        if not line:
            return self.emptyline()
        if cmd is None:
            return self.default(line)
        self.lastcmd = line
        if line == "EOF":
            self.lastcmd = ""
        if cmd == "":
            return self.default(line)
        try:
            func = getattr(self, "do_" + cmd)
        except AttributeError:
            return self.default(line)
        return func(arg)

    def onecmdhooks(self, line):
        self.onecmd(line)
        return self.rc

    def emptyline(self):
        """Called when an empty line is entered in response to the prompt.

        If this method is not overridden, it repeats the last nonempty
        command entered.

        """
        if self.lastcmd:
            return self.onecmd(self.lastcmd)
        return None

    def default(self, line):
        """Called on an input line when the command prefix is not recognized.

        If this method is not overridden, it prints an error message and
        returns.

        """
        sys.stdout.write("*** Unknown syntax: %s\n" % line)

    def default_completer(self, *ignored):
        """
        Method called to complete an input line when no command-specific
        complete_*() method is available.
        By default, it returns an empty list.
        """
        return []

    def completenames(self, text, *ignored):
        dotext = "do_" + text
        return [a[3:] for a in self.get_names() if a.startswith(dotext)]

    def complete(self, text, state):
        """Return the next possible completion for 'text'.

        If a command has not been entered, then complete against command list.
        Otherwise try to call complete_<command> to get list of completions.
        """
        if state == 0:
            import readline

            origline = readline.get_line_buffer()
            line = origline.lstrip()
            stripped = len(origline) - len(line)
            begidx = readline.get_begidx() - stripped
            endidx = readline.get_endidx() - stripped
            if begidx > 0:
                cmd, _args, _foo = self.parseline(line)
                if cmd == "":
                    compfunc = self.completedefault
                else:
                    try:
                        compfunc = getattr(self, "complete_" + cmd)
                    except AttributeError:
                        compfunc = self.completedefault
            else:
                compfunc = self.completenames
            self.completion_matches = compfunc(text, line, begidx, endidx)
        try:
            return self.completion_matches[state]
        except IndexError:
            return None

    def get_names(self):
        # This method used to pull in base class attributes
        # at a time dir() didn't do it yet.
        return dir(self.__class__)

    def complete_help(self, *args):
        commands = set(self.completenames(*args))
        topics = set(a[5:] for a in self.get_names() if a.startswith("help_" + args[0]))
        return list(commands | topics)

    def help_help(self):
        parser_help.print_help()
        self.rc = 0

    @WrapperCmdLineArgParser(parser_help)
    def do_help(self, arg, parsed):
        'List available commands with "help" or detailed help with "help cmd".'

        if arg:
            # XXX check arg syntax
            try:
                func = getattr(self, "help_" + arg)
            except AttributeError:
                try:
                    doc = getattr(self, "do_" + arg).__doc__
                    if doc:
                        sys.stdout.write("%s\n" % str(doc))
                        self.rc = 0
                        return
                except AttributeError:
                    pass
                sys.stdout.write("%s\n" % str(self.nohelp % (arg,)))
                self.rc = 1
                return
            func()

        else:
            names = self.get_names()
            cmds_doc = []
            cmds_undoc = []
            topics = set()
            for name in names:
                if name[:5] == "help_":
                    topics.add(name[5:])
            names.sort()
            # There can be duplicates if routines overridden
            prevname = ""
            for name in names:
                if name[:3] == "do_":
                    if name == prevname:
                        continue
                    prevname = name
                    cmd = name[3:]
                    if cmd in topics:
                        cmds_doc.append(cmd)
                        topics.remove(cmd)
                    elif getattr(self, name).__doc__:
                        cmds_doc.append(cmd)
                    else:
                        if cmd != "EOF":
                            cmds_undoc.append(cmd)
            sys.stdout.write("%s\n" % str(self.doc_leader))
            if os.environ.get("COLUMNS"):
                maxcol = int(os.environ.get("COLUMNS"))
            elif hasattr(self, "painter") and self.painter:
                maxcol = self.painter.area.size.w
            else:
                maxcol = 79
            self.print_topics(self.doc_header, cmds_doc, 15, maxcol)
            self.print_topics(self.misc_header, sorted(topics), 15, maxcol)
            self.print_topics(self.undoc_header, cmds_undoc, 15, maxcol)

            self.rc = 0

    def print_topics(self, header, cmds, cmdlen, maxcol):
        if cmds:
            sys.stdout.write("%s\n" % str(header))
            if self.ruler:
                sys.stdout.write("%s\n" % str(self.ruler * len(header)))
            self.columnize(cmds, maxcol - 1)
            sys.stdout.write("\n")

    def columnize(self, alist, displaywidth=80):
        """Display a list of strings as a compact set of columns.

        Each column is only as wide as necessary.
        Columns are separated by two spaces (one was not legible enough).

        """
        if not alist:
            sys.stdout.write("<empty>\n")
            return

        nonstrings = [i for i in range(len(alist)) if not isinstance(alist[i], str)]
        if nonstrings:
            raise TypeError(  # noqa: DOC501
                "alist[i] not a string for i in %s" % ", ".join(map(str, nonstrings))
            )
        size = len(alist)
        if size == 1:
            sys.stdout.write("%s\n" % str(alist[0]))
            return
        # Try every row count from 1 upwards
        for nrows in range(1, len(alist)):
            ncols = (size + nrows - 1) // nrows
            colwidths = []
            totwidth = -2
            for col in range(ncols):
                colwidth = 0
                for row in range(nrows):
                    i = row + nrows * col
                    if i >= size:
                        break
                    x = alist[i]
                    colwidth = max(colwidth, len(x))
                colwidths.append(colwidth)
                totwidth += colwidth + 2
                if totwidth > displaywidth:
                    break
            if totwidth <= displaywidth:
                break
        else:
            nrows = len(alist)
            ncols = 1
            colwidths = [0]
        for row in range(nrows):
            texts = []
            for col in range(ncols):
                i = row + nrows * col
                x = "" if i >= size else alist[i]
                texts.append(x)
            while texts and not texts[-1]:
                del texts[-1]
            for col in range(len(texts)):
                texts[col] = texts[col].ljust(colwidths[col])
            sys.stdout.write("%s\n" % str("  ".join(texts)))


if __name__ == "__main__":
    shell = NanUICmd()
    try:
        shell.start()
    finally:
        shell.stop()
