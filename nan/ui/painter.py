import curses
import pybresenham
import contextlib

from nan.ui.colors import NCursesColors
from nan.ui.nativetypes import Area_t
from nan.ui.nativetypes import Point_t
from nan.ui.nativetypes import Dim_t


class Painter(NCursesColors):
    def __init__(self, stdscr, bus):
        NCursesColors.__init__(self)

        self.stdscr = stdscr
        self.bus = bus
        self.area = Area_t()

        self.subwin = self.stdscr.derwin(0, 0, 0, 0)
        self.resize()

        self.bus.connect("draw-pixel", self.pixel)

    def circle(self, centerx, centery, radius, colour=None):
        """
        Returns a generator that produces the (x, y) tuples for the outline of a circle.

        :param centerx: x coordinate of the center circle
        :param centery: y coordinate of the center circle
        :param radius: size of the circle
        """
        for point in list(pybresenham.circle(centerx, centery, radius)):
            self.bus.emit("draw-pixel", x=point[0], y=point[1], ch="█", colour=colour)

    def clear(self):
        self.subwin.clear()

    def draw_background(self, **kwargs):
        colour = kwargs.get("colour") or None

        for y_inc in range(self.area.pos.y, self.area.size.h):
            for x_inc in range(self.area.pos.x, self.area.size.w):
                self.bus.emit("draw-pixel", x=x_inc, y=y_inc, ch=" ", colour=colour)

    def line(self, start: Point_t, end: Point_t, symbol: int or str or None, colour):
        """
        Draw line segment, given two points

        From Bresenham's line algorithm
        http://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm

        :param start: A point from where start the line
        :type start: PointType
        :param end: A point from where is the end the line
        :type end: PointType
        :param symbol: A symbol use for draw the line
        :return:
        """
        # line_chars = " ▘▝▀▖▌▞▛▗▚▐▜▄▙▟█"

        for point in list(pybresenham.line(start.x, start.y, end.x, end.y)):
            self.bus.emit("draw-pixel", x=point[0], y=point[1], ch="█", colour=colour)

    def pixel(self, **kwargs):
        x = kwargs.get("x", 0)
        y = kwargs.get("y", 0)
        ch = kwargs.get("ch", "█")
        colour = kwargs.get("colour") or curses.color_pair(0)

        # Don't bother drawing anything if we're guaranteed to be off-screen
        if self.area.size.w == 0 or self.area.size.h == 0:
            return

        if not (self.area.pos.x <= x <= self.area.size.w - 1 and self.area.pos.y <= y <= self.area.size.h - 1):
            return

        with contextlib.suppress(curses.error):
            self.subwin.delch(y, x)

        with contextlib.suppress(curses.error):
            self.subwin.insstr(y, x, ch, colour)

    def box(self, x: int, y: int, w: int, h: int, c: int = 0, s: str = ""):
        area = Area_t(
            Point_t(
                x=x,
                y=y,
            ),
            Dim_t(
                w=w,
                h=h,
            ),
        )
        colour = c
        style = s

        ULCORNER = "┌"  # noqa: N806
        URCORNER = "┐"  # noqa: N806
        LRCORNER = "┘"  # noqa: N806
        LLCORNER = "└"  # noqa: N806
        if "dash" in str(style).lower():
            HLINE = "╌"  # noqa: N806
            VLINE = "┊"  # noqa: N806
        else:
            HLINE = "─"  # noqa: N806
            VLINE = "│"  # noqa: N806

        if isinstance(style, str):
            if "round" in str(style).lower():
                ULCORNER = "╭"  # noqa: N806
                URCORNER = "╮"  # noqa: N806
                LRCORNER = "╯"  # noqa: N806
                LLCORNER = "╰"  # noqa: N806
                if "dash" in str(style).lower():
                    HLINE = "╌"  # noqa: N806
                    VLINE = "┊"  # noqa: N806
                else:
                    HLINE = "─"  # noqa: N806
                    VLINE = "│"  # noqa: N806
            if "heavy" in str(style).lower():
                ULCORNER = "┏"  # noqa: N806
                URCORNER = "┓"  # noqa: N806
                LRCORNER = "┛"  # noqa: N806
                LLCORNER = "┗"  # noqa: N806
                if "dash" in str(style).lower():
                    HLINE = "╍"  # noqa: N806
                    VLINE = "┋"  # noqa: N806
                else:
                    HLINE = "━"  # noqa: N806
                    VLINE = "┃"  # noqa: N806
            if "double" in str(style).lower():
                ULCORNER = "╔"  # noqa: N806
                URCORNER = "╗"  # noqa: N806
                LRCORNER = "╝"  # noqa: N806
                LLCORNER = "╚"  # noqa: N806
                HLINE = "═"  # noqa: N806
                VLINE = "║"  # noqa: N806

        # Top/Left - corner
        self.bus.emit(
            "draw-pixel",
            x=area.pos.x,
            y=area.pos.y,
            ch=ULCORNER,
            colour=colour,
        )

        # Right - line
        for j in range(0, area.size.w):
            self.bus.emit(
                "draw-pixel",
                x=area.pos.x + 1 + j,
                y=area.pos.y,
                ch=HLINE,
                colour=colour,
            )

        # Top/Right - corner
        self.bus.emit(
            "draw-pixel",
            x=area.pos.x + 1 + area.size.w,
            y=area.pos.y,
            ch=URCORNER,
            colour=colour,
        )

        # Left - line
        for j in range(0, area.size.h):
            self.bus.emit(
                "draw-pixel",
                x=area.pos.x,
                y=area.pos.y + 1 + j,
                ch=VLINE,
                colour=colour,
            )
            self.bus.emit(
                "draw-pixel",
                x=area.pos.x + area.size.w + 1,
                y=area.pos.y + 1 + j,
                ch=VLINE,
                colour=colour,
            )

        # Bottom/Left = corner
        self.bus.emit(
            "draw-pixel",
            x=area.pos.x,
            y=area.pos.y + area.size.h + 1,
            ch=LLCORNER,
            colour=colour,
        )

        # Bottom - line
        for j in range(0, area.size.w):
            self.bus.emit(
                "draw-pixel",
                x=area.pos.x + 1 + j,
                y=area.pos.y + area.size.h + 1,
                ch=HLINE,
                colour=colour,
            )

        # Bottom/Right - corner
        self.bus.emit(
            "draw-pixel",
            x=area.pos.x + area.size.w + 1,
            y=area.pos.y + area.size.h + 1,
            ch=LRCORNER,
            colour=colour,
        )

    def text(self, x: int, y: int, text: str, colour: int):
        for ch in str(text):
            self.bus.emit("draw-pixel", x=x, y=y, ch=ch, colour=colour)
            x += 1

    def resize(self):
        self.area.size.h, self.area.size.w = self.stdscr.getmaxyx()
        self.subwin.resize(self.area.size.h, self.area.size.w)

    def refresh(self):
        self.subwin.touchwin()
        self.subwin.refresh()
