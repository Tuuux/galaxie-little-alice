import curses
import math
import logging
from attrs import define
from attrs import field
from attrs import validators

# https://www.linuxjournal.com/content/about-ncurses-colors-0
# https://gist.github.com/ifonefox/6046257
# https://invisible-island.net/ncurses/ncurses-slang.html
# xterm (8-color with 64-color pairs and 16-color with 256-color pairs) and non-color vt100/vt220.


@define(slots=True)
class ColorBase:
    """
    Internal storage for store what is a color
    """

    R: int = field(validator=validators.instance_of(int), default=0)
    """
    Amount of Red color from 0 to 255
    """

    G: int = field(validator=validators.instance_of(int), default=0)
    """
    Amount of Green color from 0 to 255
    """

    B: int = field(validator=validators.instance_of(int), default=0)
    """
    Amount of Blue color from 0 to 255
    """

    @R.validator
    @G.validator
    @B.validator
    def fits_bytes(self, attribute, value):
        if not 0 <= value < 256:
            raise ValueError(f"'{attribute.name}' value out of bounds, allowed 0 to 255")


class Color:
    """
    Represents an RGB (red, green, blue) color.
    """

    ColorBase = ColorBase()

    def __init__(self, rgb: int = None):
        self.ColorBase = ColorBase()
        if rgb:
            self.ColorBase.R = (rgb >> 16) & 255
            self.ColorBase.G = (rgb >> 8) & 255
            self.ColorBase.B = rgb & 255

    @classmethod
    def FromRGB(cls, *rgb):  # noqa: N802
        """
        **Options**
         - Creates a Color structure from a 32-bit int RGB
         - Creates a Color structure from a list like (r, g, b) in that case valid int values are 0 through 255

        The alpha value is implicitly 255 (fully opaque).

        .. seealso::
         - :py:func:`~microGUI.Drawing.Color.Color.FromCMYK()`
         - :py:func:`~microGUI.Drawing.Color.Color.FromHex()`
         - :py:func:`~microGUI.Drawing.Color.Color.FromARGB()`

        :param rgb: The 16-bit int value or arguments args like r,g,b
        :type rgb: args or int
        :return: The :class:`~microGUI.Drawing.Color.Color` that this method creates.
        :rtype: :class:`~microGUI.Drawing.Color.Color`
        :raise SyntaxError: When 'FromRGB' function cant determine rgb format
        """

        if len(rgb) == 1 and isinstance(rgb[0], int):
            cls.ColorBase.R = (rgb[0] >> 16) & 255
            cls.ColorBase.G = (rgb[0] >> 8) & 255
            cls.ColorBase.B = rgb[0] & 255

            return cls(cls.To32bitInt())

        if len(rgb) == 3 and isinstance(rgb[0], int) and isinstance(rgb[1], int) and isinstance(rgb[2], int):
            cls.ColorBase.R = rgb[0]
            cls.ColorBase.G = rgb[1]
            cls.ColorBase.B = rgb[2]

            return cls(cls.To32bitInt())

        raise SyntaxError("'FromRGB' function cant determine rgb format")  # noqa: DOC501

    @classmethod
    def FromHex(cls, color: str):  # noqa: N802
        """
        Creates a Color class from the specified HTML HEX palette.
        The alpha value is implicitly 255 (fully opaque).

        .. seealso::
         - :py:func:`~microGUI.Drawing.Color.Color.FromARGB()`
         - :py:func:`~microGUI.Drawing.Color.Color.FromCMYK()`

        :param color: HTML color HEX
        :type color: str
        :return: The :py:class:`~microGUI.Drawing.Color.Color` that this method creates.
        :rtype: :py:class:`~microGUI.Drawing.Color.Color`
        """
        tmp_tuple = tuple(int(color.lstrip("#")[i : i + 2], 16) for i in (0, 2, 4))
        cls.ColorBase = ColorBase(
            B=tmp_tuple[2],
            G=tmp_tuple[1],
            R=tmp_tuple[0],
        )
        return cls(cls.To32bitInt())

    @classmethod
    def To32bitInt(cls):  # noqa: N802
        """
        Gets the 16-bit RGB value of this Color class.

        :return: The 32-bit ARGB value :py:class:`~microGUI.Type.Color.ColorType` of \
                 this :py:class:`~microGUI.Drawing.Color.Color`.
        :rtype: int
        """
        return int(
            "{:02x}{:02x}{:02x}".format(cls.ColorBase.R, cls.ColorBase.G, cls.ColorBase.B),
            16,
        )

    @classmethod
    def ToRGB(cls):  # noqa: N802
        """
        Gets the RGB (0-255, 0-255, 0-255) tuple value of this Color class.

        :return: The RGB value :py:class:`~microGUI.Type.Color.ColorType` of \
                 this :py:class:`~microGUI.Drawing.Color.Color`.
        :rtype: tuple
        """

        return (
            cls.ColorBase.R,
            cls.ColorBase.G,
            cls.ColorBase.B,
        )

    @classmethod
    def ToHEX(cls):  # noqa: N802
        """
        Gets the 16-bit RGB value of this Color class.

        :return: The 16-bit RGB value :py:class:`~microGUI.Type.Color.ColorType` of \
                 this :py:class:`~microGUI.Drawing.Color.Color`.
        :rtype: str
        """

        return "#{:02x}{:02x}{:02x}".format(cls.ColorBase.R, cls.ColorBase.G, cls.ColorBase.B)

    @classmethod
    def ToNCurses(cls):  # noqa: N802
        """
        Gets the RGB (0-1000) tuple value of this Color class.

        :return: The 16-bit RGB value :py:class:`~microGUI.Type.Color.ColorType` of \
                 this :py:class:`~microGUI.Drawing.Color.Color`.
        :rtype: tuple
        """
        ratio = 1000 / 255

        return (
            int(cls.ColorBase.R * ratio),
            int(cls.ColorBase.G * ratio),
            int(cls.ColorBase.B * ratio),
        )

    @classmethod
    def To_ANSI16(cls):  # noqa: N802
        """
        Gets the ANSI16

        :return: ANSI16
        :rtype: int
        """
        return (round(cls.ColorBase.B / 255) << 2) | (round(cls.ColorBase.G / 255) << 1) | round(cls.ColorBase.R / 255)


class NCursesColors(object):
    def __init__(self):
        self.__itu_recommendation = None
        self.itu_recommendation = "BT.601"

        self.curses_color_pairs_init()

    @property
    def itu_recommendation(self):
        """
        Get ``itu_recommendation`` property value

        Where:

        https://en.wikipedia.org/wiki/ITU-R

        https://en.wikipedia.org/wiki/Rec._601

        https://en.wikipedia.org/wiki/Rec._709

        https://en.wikipedia.org/wiki/Rec._2100

        Allowed Value: 'BT.601', 'BT.709', 'BT.2100'
        Default Value: 'BT.601'

        :return: itu_recommendation property value
        :rtype: str
        """
        return self.__itu_recommendation

    @itu_recommendation.setter
    def itu_recommendation(self, value=None):
        if value is None:
            value = "BT.601"
        if not isinstance(value, str):
            raise TypeError("'itu_recommendation' property value must be a str or None")
        if value not in ["BT.601", "BT.709", "BT.2100", "CUSTOM"]:
            raise ValueError("'itu_recommendation' property value must be 'BT.601', 'BT.709' or 'BT.2100'")

        if self.itu_recommendation != value:
            self.__itu_recommendation = value

    @property
    def color_detection_value(self):
        return {
            curses.COLOR_BLACK: {"itu": "BT.601", "dim": 0.06, "normal": 0.33},
            curses.COLOR_BLUE: {"itu": "CUSTOM", "dim": 0.45, "normal": 0.6},
            curses.COLOR_GREEN: {"itu": "BT.601", "dim": 0.45, "normal": 0.75},
            curses.COLOR_CYAN: {"itu": "BT.709", "dim": 0.777, "normal": 0.894},
            curses.COLOR_RED: {"itu": "CUSTOM", "dim": 0.45, "normal": 0.6},
            curses.COLOR_MAGENTA: {"itu": "CUSTOM", "dim": 0.45, "normal": 0.69},
            curses.COLOR_YELLOW: {"itu": "BT.709", "dim": 0.715, "normal": 0.921},
            curses.COLOR_WHITE: {"itu": "BT.601", "dim": 0.561, "normal": 0.891},
        }

    @staticmethod
    def curses_color(color):
        """
        A "translation" function that converts standard-intensity CGA color numbers (0 to 7) to curses color numbers,
        using the curses constant names like COLOR_BLUE or COLOR_RED

        :param color:
        :return: curses.COLOR
        """
        return 7 & color

    @staticmethod
    def curses_color_pair_number(fg, bg):
        """
        A function to set an integer bit pattern based on the classic color byte

        :param fg: Foreground color
        :type fg: int
        :param bg: Background color
        :type bg: int
        """
        return 1 << 7 | (7 & bg) << 4 | 7 & fg

    def curses_color_pairs_init(self):
        """
        It functions create 64 colors pairs

        :return:
        """
        pair_count = 0
        if curses.has_colors():
            logging.debug(f"{self.__class__.__name__}: Using colors")
            curses.start_color()
            curses.use_default_colors()
        try:
            for bg in [0, 1, 2, 3, 4, 5, 6, 7]:
                for fg in [0, 1, 2, 3, 4, 5, 6, 7]:
                    curses.init_pair(
                        self.curses_color_pair_number(fg, bg),
                        self.curses_color(fg),
                        self.curses_color(bg),
                    )
                    pair_count += 1
        except curses.error:  # pragma: no cover
            pass
        logging.debug(f"{self.__class__.__name__}: create : {pair_count} color pairs")

    def get_luma_component_rgb(self, r, g, b):
        # HSP  where the P stands for Perceived brightness
        # http://alienryderflex.com/hsp.html
        # Back to double
        r /= 255.0
        g /= 255.0
        b /= 255.0
        if self.itu_recommendation == "BT.2100":
            # BT.2100
            return math.sqrt(r * r * 0.2627 + g * g * 0.6780 + b * b * 0.0593)
        if self.itu_recommendation == "BT.709":
            # BT.709
            return math.sqrt(r * r * 0.2126 + g * g * 0.7152 + b * b * 0.0722)
        if self.itu_recommendation == "BT.601":
            # BT.601
            return math.sqrt(r * r * 0.299 + g * g * 0.587 + b * b * 0.114)
        if self.itu_recommendation == "CUSTOM":
            # More Blue
            return math.sqrt(r * r * 0.2627 + g * g * 0.7152 + b * b * 0.246)
        return None  # pragma: no cover

    def rgb_to_curses_attributes(self, r, g, b):
        ansi_color = Color.FromRGB(r, g, b).To_ANSI16()
        self.itu_recommendation = self.color_detection_value[ansi_color]["itu"]
        luma_component = self.get_luma_component_rgb(r, g, b)

        if luma_component <= self.color_detection_value[ansi_color]["dim"]:
            return curses.A_DIM
        if luma_component <= self.color_detection_value[ansi_color]["normal"]:
            return curses.A_NORMAL
        return curses.A_BOLD

    # Entry point
    def color(self, fg: tuple = None, bg: tuple = None, attributes: bool = True) -> curses.color_pair:
        """
        Convert a RGB value to a directly usable curses color

        draw(y, x, "Hello", color) where the return of it function is directly usable

        :param bg: RGB color
        :type bg: tuple
        :param fg: RGB color
        :type fg: tuple
        :param attributes: If True use got 256 colors
        :type attributes: bool
        :return: curses.color_pair | curses.Attribute
        :rtype: curses.color_pair
        """

        if fg is not None and not isinstance(fg, tuple):
            raise TypeError("'fg' argument value must be a tuple or int instance")  # noqa: DOC501

        if bg is not None and not isinstance(bg, tuple):
            raise TypeError("'bg' argument value must be a tuple or int instance")  # noqa: DOC501

        if not isinstance(attributes, bool):
            raise TypeError("'attributes' argument value must be a bool type or None")  # noqa: DOC501

        if fg is None:
            fg = -1
        if bg is None:
            bg = -1

        if isinstance(bg, tuple):
            bg_color = Color.FromRGB(bg[0], bg[1], bg[2]).To_ANSI16()
        else:
            bg_color = -1

        if isinstance(fg, tuple):
            fg_color = Color.FromRGB(fg[0], fg[1], fg[2]).To_ANSI16()
        else:
            fg_color = -1

        if fg == -1 and bg == -1:
            return curses.color_pair(0)

        if attributes:
            return curses.color_pair(
                self.curses_color_pair_number(fg=fg_color, bg=bg_color)
            ) | self.rgb_to_curses_attributes(fg[0], fg[1], fg[2])

        return curses.color_pair(
            self.curses_color_pair_number(
                fg=fg_color,
                bg=bg_color,
            )
        )
