from attr import define
from attr import field
from attr import validators


# NativeTypes

# Widget (Not implemented, use native python object)


# Point_t
@define(slots=True)
class Point_t:
    """
    Coordinates of a single point.

    The :py:attr:`~microGUI.WidgetsNativeTypes.Point_t` structure describes the coordinates of a single point.

    .. seealso::
     - :py:attr:`~microGUI.Widgets.Widget.Location`
     - :py:attr:`~microGUI.Widgets.Widget.Position`
    """

    x: int = field(validator=validators.instance_of(int), default=0)
    """
    X-axis coordinate.
    """

    y: int = field(validator=validators.instance_of(int), default=0)
    """
     Y-axis coordinate.
    """


@define(slots=True)
class Dim_t:
    """
    Dimensions of an area.

    The :py:attr:`~microGUI.WidgetsNativeTypes.Dim_t` structure defines the dimensions of an area.

    .. seealso::
     - :py:attr:`~microGUI.Widgets.Widget.Size`
    """

    w: int = field(validator=validators.instance_of(int), default=0)
    """
    Width of the area.
    """

    h: int = field(validator=validators.instance_of(int), default=0)
    """
    Height of the area.
    """


@define(slots=True)
class Area_t:
    """
    Position and dimensions of a rectangular area.

    The :py:attr:`~microGUI.WidgetsNativeTypes.Area_t` structure describes the position and dimensions of a
    rectangular area. It's used extensively by the :py:class:`~microGUI.Widgets.Widget.Widget`

    .. seealso::
     - :py:attr:`~microGUI.Widgets.Widget.Widget.Bounds`
    """

    pos: Point_t = field(validator=validators.instance_of(Point_t), default=Point_t())
    """
    Upper-left corner of the area.
    """

    size: Dim_t = field(validator=validators.instance_of(Dim_t), default=Dim_t())
    """
    Size of the area.
    """


class Color_t(int):
    """
    Composite color value.

    The Color_t type definition describes a composite color value.
    The interpretation of the color depends on the current color model.

    .. note :: Use Widgets.Drawing.Color and Widgets.Drawing.Colors to manipulate colors of the widget.
    """

    pass


# Rect_t
@define(slots=True)
class Rect_t:
    """
    Coordinates of a rectangle.

    The :py:attr:`~microGUI.WidgetsNativeTypes.PhRect_t` structure describes the coordinates of a rectangle.

    .. seealso::
     - :py:attr:`~microGUI.Widgets.Widget.Arguments.extent`
    """

    ul: Point_t = field(validator=validators.instance_of(Point_t), default=Point_t())
    """
    Upper-left corner.
    """

    lr: Point_t = field(validator=validators.instance_of(Point_t), default=Point_t())
    """
    Lower-right corner.
    """
