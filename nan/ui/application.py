import curses
import logging
import asyncio
import sys

from nan.core.logger import CustomFormatter
from nan.core.eventbus import EventBus
from nan.core.tasks import TaskManager

from nan.ui.nativetypes import Area_t
from nan.ui.screen import Screen
from nan.ui.painter import Painter


class Singleton(type):
    def __init__(cls, name, bases, dict):
        super(Singleton, cls).__init__(name, bases, dict)
        cls.instance = None

    def __call__(cls, *args, **kw):
        if cls.instance is None:
            cls.instance = super(Singleton, cls).__call__(*args)
        return cls.instance


class Application(metaclass=Singleton):
    def __init__(self, **kwargs):
        self.logger = logging.getLogger()
        self.logger.name = "microGUI"
        self.logger.setLevel(logging.DEBUG)

        # create console handler and set level to debug
        ch = logging.StreamHandler(sys.stdout)
        ch.setLevel(logging.ERROR)

        # add formatter to ch
        ch.setFormatter(CustomFormatter())

        # add ch to logger
        self.logger.addHandler(ch)

        file_handler = logging.FileHandler("logs.log")
        file_handler.setLevel(logging.DEBUG)
        file_handler.setFormatter(CustomFormatter())
        self.logger.addHandler(file_handler)

        self.loop = kwargs.get("loop", asyncio.get_event_loop())
        self.bus = kwargs.get("bus", EventBus())
        self.tasks = kwargs.get("tasks", TaskManager(self.loop))
        self.screen = kwargs.get("screen", Screen())
        self.area = Area_t()
        self.painter = kwargs.get("painter", Painter(self.screen.stdscr, self.bus))

        self.fps = 20
        self.__init_signals()

    def __init_signals(self):
        self.tasks.add(self.__loop_updater(), "NCURSES Screen auto refresh()")
        self.tasks.add(self.__loop_input_event(), "NCURSES Screen getch()")
        self.tasks.add(self.__loop_paint(), "Application Paint")

        self.bus.connect("CURSES", self.handle_curses)
        self.bus.connect("KEY_MOUSE", self.handle_mouse)
        self.bus.connect("KEY_RESIZE", self.handle_resize)

    def start(self):
        self.loop.run_forever()

    def stop(self):
        self.bus.clear()
        self.tasks.cancel()
        self.loop.stop()
        self.screen.close()
        logging.debug(f"{self.__class__.__name__}: is close")

    def refresh(self):
        # Touch before anything
        self.screen.stdscr.touchwin()

        # Do stuff here
        self.painter.refresh()

        # Last refresh
        self.screen.refresh()

    async def __loop_updater(self):  # pragma: no cover
        while await asyncio.sleep(1 / self.fps, self.loop.is_running()):
            self.refresh()

    async def __loop_input_event(self):
        while self.loop.is_running():
            event = self.screen.stdscr.getch()
            if event == curses.ERR:
                await asyncio.sleep(0.1)

            elif event == curses.KEY_RESIZE:  # pragma: no cover
                self.bus.emit("KEY_RESIZE")

                logging.debug(f"{self.__class__.__name__}: KEY_RESIZE event")

            elif event == curses.KEY_MOUSE:  # pragma: no cover
                mouse = curses.getmouse()
                self.bus.emit("KEY_MOUSE", mouse)

                logging.debug(f"{self.__class__.__name__}: KEY_MOUSE event {mouse}")
            else:  # pragma: no cover
                self.bus.emit("CURSES", event)

                logging.debug(f"{self.__class__.__name__}: CURSES event {event}")

    def handle_resize(self):
        self.painter.resize()

    def handle_mouse(self, mouse: tuple) -> None:
        pass

    def handle_curses(self, char: int) -> None:
        pass

    def paint(self):
        pass

    async def __loop_paint(self):  # pragma: no cover
        while await asyncio.sleep(0, self.loop.is_running()):
            self.painter.draw_background()
            self.paint()
