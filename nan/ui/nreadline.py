from attrs import define
import os
import curses
import contextlib


@define
class ReadLine:
    x: int = 0
    y: int = 0
    h: int = 1
    w: int = 0

    history_length: int = 100
    history_filename: str = "~/.history"
    history_list: list = []
    history_pos: int = 0

    buffer: str = ""
    cursor_pos: int = 0
    buffer_offset: int = 0

    def write_history_file(self, filename=None):
        """
        Save the history list to a readline history file, overwriting any existing file.
        The default filename is `~/.history`. This calls `write_history()` in the underlying library.

        :param filename: history file
        :type filename: str or None
        """
        if filename:
            self.history_filename = filename
        self.write_history()

    def write_history(self):
        """
        Save the `history_list` content inside `history_filename`.
        The default filename is `~/.history`.
        """
        with open(self.history_filename, "w") as history_file:
            try:
                for line in self.history_list[-self.history_length]:
                    history_file.write(f"{line}\n")
            except IndexError:
                for line in self.history_list:
                    history_file.write(f"{line}\n")

    def read_history_file(self, filename=None):
        """
        Load a readline history file, and append it to the history list.
        The default filename is ~/.history.
        This calls read_history() in the underlying library.

        :param filename: history file
        :type filename: str or None
        """
        if filename:
            self.history_filename = filename
        self.read_history()

    def read_history(self):
        """
        Load a readline history file, and append it to the history list.
        The default filename is ~/.history.
        """
        if os.path.exists(self.history_filename):
            with open(self.history_filename, "r") as history_file:
                last_line = history_file.readlines()
                try:
                    for line in last_line[-self.history_length]:
                        self.add_history(line)
                except IndexError:
                    for line in last_line:
                        self.add_history(line)

    def clear_history(self):
        """
        Clear the current history.
        """
        self.history_list = []

    def add_history(self, line):
        """
        Append line to the history buffer, as if it was the last line typed.

        :param line: a line without prompt and return
        :type line: str
        """
        if self.history_list:
            if self.history_list[-1] != line.rstrip():
                self.history_list.append(line.rstrip())
        else:
            self.history_list = [line.rstrip()]

    def left(self):
        if self.cursor_pos > 0:
            self.cursor_pos -= 1

    def right(self):
        if self.cursor_pos != len(self.buffer) - self.buffer_offset:
            self.cursor_pos += 1
        # if self.cursor_pos < len(self.prefix) - 1 + len(self.buffer) - 1:
        #     self.cursor_pos += 1

    def up(self):
        if self.history_list:
            self.buffer = self.history_list[::-1][self.history_pos]
            self.cursor_pos = len(self.buffer)
            if self.history_pos < len(self.history_list) - 1:
                self.history_pos += 1

    def down(self):
        if self.history_list:
            self.buffer = self.history_list[::-1][self.history_pos]
            self.cursor_pos = len(self.buffer)
            if self.history_pos >= 1:
                self.history_pos -= 1

    def backspace(self):
        if self.cursor_pos > 0:
            self.left()
            self.buffer = self.buffer[: self.cursor_pos] + self.buffer[self.cursor_pos + 1 :]

    def delete(self):
        self.buffer = self.buffer[: self.cursor_pos] + self.buffer[self.cursor_pos + 1 :]

    def home(self):
        self.cursor_pos = 0
        self.buffer_offset = 0

    def end(self):
        l = len(self.buffer)
        if l >= self.w:
            self.buffer_offset = l - self.w + 2
            self.cursor_pos = self.w - 2
        else:
            self.cursor_pos = l

    def insert(self, char):
        with contextlib.suppress(ValueError):
            if curses.ascii.isprint(char):
                self.buffer = (
                    self.buffer[: self.buffer_offset + self.cursor_pos]
                    + chr(char)
                    + self.buffer[self.buffer_offset + self.cursor_pos :]
                )
                self.cursor_pos += 1

    def enter(self):
        old_buffer = self.buffer
        self.buffer = ""
        self.history_pos = 0
        self.cursor_pos = 0

        return old_buffer
