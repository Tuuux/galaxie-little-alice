import os
import logging
import locale
import contextlib

locale.setlocale(locale.LC_ALL, "")
code = locale.getpreferredencoding()
import curses  # noqa: E402


class SuspendCurses:
    def __enter__(self):  # pragma: no cover
        with contextlib.suppress(curses.error):
            curses.endwin()

    def __exit__(self, exc_type, exc_val, tb):  # pragma: no cover
        with contextlib.suppress(curses.error):
            new_screen = curses.initscr()
            new_screen.refresh()
            curses.doupdate()


class Screen:
    """
    In charge to init the terminal

    Operating systems often have mode distinctions, and the raw/rare/cooked way of describing them has spread widely
    along with the C language and other Unix exports. Most generally, "cooked mode" may refer to any mode of a system
    that does extensive preprocessing before presenting data to a program.

    **cooked mode**
     The normal character-input mode of a Unix terminal device: with interrupts enabled and with erase, kill and other
     special-character interpretations performed directly by the tty driver.

    .. attention::
     Opposite of raw mode

    .. seealso::
     rare mode

    **rare mode**
     Unix CBREAK mode (character-by-character with keyboard interrupts enabled).

    **Half-delay mode**

    **Raw mode**
     A mode that allows a program to transfer bits directly to or from an I/O device without any processing,
     abstraction, or interpretation by the operating system.
    """

    def __init__(self):
        self.__stdscr = None
        self.__cbreak = None
        self.__echo = None
        self.__halfdelay = None
        self.__intrflush = None
        self.__keypad = None
        self.__meta = None
        self.__nodelay = None
        self.__raw = None
        self.__qiflush = None
        self.__timeout = None

        # During initialization, the ncurses library checks for special cases where VT100 line-drawing
        # (and the corresponding alternate character set capabilities) described in the terminfo are known to be
        # missing. Specifically, when running in a UTF-8 locale, the Linux console emulator and the GNU stdscr
        # program ignore these.
        # Ncurses checks the TERM environment variable for these. For other special cases, you should set this
        # environment variable.
        # Doing this tells ncurses to use Unicode values which correspond
        # to the VT100 line-drawing glyphs. That works for the special cases cited, and is likely to work for terminal
        # emulators.
        # When setting this variable, you should set it to a nonzero value. Setting it to zero (or to a nonnumbe
        # disables the special check for Linux and stdscr.
        # if not os.environ.get('TERM'):
        #     os.environ['TERM'] = 'linux'
        os.environ["NCURSES_NO_UTF8_ACS"] = "1"

        self.check_terminal()

        self.stdscr = curses.initscr()

        # Specifies the total time, in milliseconds, for which ncurses will await a character sequence, e.g.,
        # a function key. The default value, 1000 milliseconds, is enough for most uses. However, it is made a
        # variable to accommodate unusual applications. The most common instance where you may wish to change this
        # value is to work with slow hosts, e.g., running on a network. If the host cannot read characters rapidly
        # enough, it will have the same effect as if the terminal did not send characters rapidly enough. The library
        # will still see a timeout.
        #
        # Note that xterm mouse events are built up from character sequences received from the xterm. If your
        # application makes heavy use of multiple-clicking, you may wish to lengthen this default value because the
        # timeout applies to the composed multi-click event as well as the individual clicks.
        #
        # In addition to the environment variable, this implementation provides a global variable with the same name.
        # Portable applications should not rely upon the presence of ESCDELAY in either form, but setting the
        # environment variable rather than the global variable does not create problems when compiling an application.
        os.environ["ESCDELAY"] = "200"

        # curses remembers the "in-program" modes after this call
        # try:
        #     curses.def_prog_mode()
        # except curses.error:   # pragma: no cover
        #     pass

        # Turn off echoing of keys, and enter cbreak mode,
        # where no buffering is performed on keyboard input
        self.echo = False
        self.cbreak = True

        self.intrflush = False

        # In keypad mode, escape sequences for special keys
        # (like the cursor keys) will be interpreted and
        # a special value like curses.KEY_LEFT will be returned
        self.keypad = True

        curses.mousemask(-1)
        curses.mouseinterval(200)
        self.nodelay = True
        self.meta = True

        # Access ^c before shell does.
        # self.raw = True

        # Hide cursor
        with contextlib.suppress(curses.error):
            curses.curs_set(0)

    @property
    def stdscr(self) -> curses.window:
        return self.__stdscr

    @stdscr.setter
    def stdscr(self, stdscr=None):
        if stdscr != self.stdscr:
            self.__stdscr = stdscr
            logging.debug(f"{self.__class__.__name__}: create a stdscr: {stdscr}")

    @property
    def cbreak(self):
        """
        Put the terminal into and out of cbreak mode

        The :py:attr:`~microGUI.Drivers.NCurses.Screen.cbreak` property put the terminal into and out
        of ``cbreak mode``.

        In this mode, characters typed by the user are immediately available to the program,
        and erase/kill  character-processing is not performed.

        When out of this mode, the tty driver buffers the typed characters until a newline or
        carriage return is typed.

        Interrupt and flow control characters are unaffected by this mode.

        Initially the terminal may or may not be in ``cbreak mode``, as the mode is inherited; therefore, a program
        should call curses.cbreak() or curses.nocbreak() explicitly.
        Most interactive programs using curses set the ``cbreak mode``.

        .. note::
         - When True sets the input mode for the current terminal to cbreak mode and overrides
           a call to raw().
         - When False sets the input mode for the current terminal to Cooked Mode without
           changing the state of ISIG and IXON.

        """
        return self.__cbreak

    @cbreak.setter
    def cbreak(self, cbreak: None or bool):
        if cbreak is None:
            cbreak = False

        if not isinstance(cbreak, bool):
            raise TypeError('"cbreak" must be a bool instance or None')

        if self.cbreak != cbreak:
            self.__cbreak = cbreak
            with contextlib.suppress(curses.error):
                if self.cbreak:
                    curses.cbreak()
                else:
                    curses.nocbreak()

    @property
    def echo(self):
        """
        Control whether characters typed by the user are echoed by getch as they are typed.

        Echoing by the tty driver is always disabled, but initially getch is in echo mode, so characters typed
        are echoed.

        Authors of most interactive programs prefer to do their own echoing in a controlled area of the
        screen, or not to echo at all, so they disable echoing by calling noecho.

        [See curs_getch(3X) for a discussion of how these routines interact with cbreak and nocbreak.]

        :return the echo property value
        :rtype: bool
        """
        return self.__echo

    @echo.setter
    def echo(self, echo=None):
        """
        Set the echo property.

         * If ``True``: curses.echo()
         * If ``False``: curses.noecho()
         * If ``None``: curses.noecho()

        :param echo: accept ``True``, ``False``, ``None`` as value
        :type echo: bool or None
        :raise ValueError: when ``echo`` is not bool type or None
        """
        if echo is None:
            echo = False

        if not isinstance(echo, bool):
            raise TypeError('"echo" must be a bool instance or None')

        if self.echo != echo:
            self.__echo = echo
            with contextlib.suppress(curses.error):
                if self.echo:
                    curses.echo()
                else:
                    curses.noecho()

    @property
    def halfdelay(self):
        """
        The ``halfdelay`` property is used for half-delay mode, which is similar to cbreak mode in that characters
        typed by the user are immediately available to the program.

        However, after blocking for ``tenths`` tenths of seconds, ERR is returned if nothing has been typed.

        :return:
        """
        return self.__halfdelay

    @halfdelay.setter
    def halfdelay(self, tenths=None):
        """
        If ``tenths`` is set to ``None``, 0, -1 ``halfdelay`` and ``cbreak`` will be set to ``False``

        Use nocbreak to leave half-delay mode.

        :param tenths: The value of tenths must be a number between 1 and 255.
        :type: int or None
        """
        if tenths is not None and not isinstance(tenths, int):
            raise TypeError('"tenths" must be in int type or None')

        if tenths is not None:
            if self.halfdelay != max(1, min(tenths, 255)):
                self.__halfdelay = max(1, min(tenths, 255))
        else:
            if self.halfdelay is not None:
                self.__halfdelay = None

        with contextlib.suppress(curses.error):
            if self.halfdelay is None:
                curses.nocbreak()
                self.cbreak = False  # pragma: no cover
            else:
                curses.halfdelay(self.halfdelay)

    @property
    def intrflush(self):
        """
        If the intrflush property is enabled, (bf is TRUE), when an interrupt key is pressed on the keyboard
        (interrupt, break, quit) all output in the tty driver queue will be flushed, giving the effect of
        faster response to the interrupt, but causing curses to have the wrong idea of what is on the screen.

        Disabling (bf is FALSE), the option prevents the flush.

        The default for the option is inherited from the tty driver settings. The window argument is ignored.

        Note: That return None only if property have never been set

        :return: The ``intrflush`` property value
        :rtype: bool or None
        """
        return self.__intrflush

    @intrflush.setter
    def intrflush(self, bf=None):
        """
        Set the ``intrflush`` property.

         * If ``True``: curses.intrflush(True)
         * If ``False``: curses.intrflush(False)
         * If ``None``: curses.intrflush(False)

        :param bf: accept ``True``, ``False``, ``None`` as value
        :type bf: bool or None
        :raise ValueError: When ``bf`` is not a bool or None type
        """
        if bf not in [True, False, None]:
            raise ValueError('"intrflush" must be a bool value or None')

        if bf != self.intrflush:
            self.__intrflush = bf
            with contextlib.suppress(curses.error):
                if self.intrflush:
                    curses.intrflush(True)
                else:
                    curses.intrflush(False)

    @property
    def keypad(self):
        """
        The ``keypad`` property enables the keypad of the user's terminal.

        If enabled (bf is TRUE), the user can press a function key (such as an arrow key) and wgetch returns a single
        value representing the function key, as in KEY_LEFT. If disabled (bf is FALSE), curses does not treat
        function keys specially and the program has to interpret the escape sequences itself.

        If the keypad in the terminal can be turned on (made to transmit) and off (made to work locally), turning on
        this option causes the terminal keypad to be turned on when wgetch is called.

        The default value for ``keypad`` is ``True``.

        Note: That return ``None`` if ``keypad`` have never been set.

        :return: The ``keypad`` property value
        :rtype: bool or None
        """
        return self.__keypad

    @keypad.setter
    def keypad(self, value=None):
        """
        Set the ``keypad`` property.

         * If ``True``: self.stdscr.keypad(1)
         * If ``False``: self.stdscr.keypad(0)
         * If ``None``: self.stdscr.keypad(0)

        :param value: accept ``True``, ``False``, ``None`` as value
        :type value: bool or None
        """
        if value not in [True, False, None]:
            raise ValueError('"keypad" property value must be a bool value or None')

        if self.keypad != value:
            self.__keypad = value

            with contextlib.suppress(curses.error):
                if self.keypad is True:
                    self.stdscr.keypad(True)
                else:
                    self.stdscr.keypad(False)

    @property
    def meta(self):
        """
        Initially, whether the terminal returns def_prog_mode7 or 8 significant bits on input depends on the control
        mode of the tty driver [see termio(7)].

        To force 8 bits to be returned, invoke ``meta``=``True`` this is equivalent,
        under POSIX, to setting the CS8 flag on the terminal.

        To force 7 bits to be returned, invoke ``meta``=``False`` this is equivalent, under POSIX, to setting the CS7
        flag on the terminal.

        If the terminfo capabilities smm (meta_on) and rmm (meta_off) are defined for the terminal, smm is sent to the
        terminal when ``meta``=``True`` is called and rmm is sent when ``meta``=``False`` is called.

        Note: That return ``None`` when the property have never been set

        :return: The ``meta`` property value
        :rtype: bool or None
        """
        return self.__meta

    @meta.setter
    def meta(self, value=None):
        """
        Set the ``meta`` property.

         * If ``True``: curses.meta(True)
         * If ``False``: curses.meta(False)
         * If ``None``: curses.meta(False)

        :param value: accept ``True``, ``False``, ``None`` as value
        :type value: bool or None
        """
        if value not in [True, False, None]:
            raise ValueError('"meta" property value must be a bool instance or None')

        if self.meta != value:
            self.__meta = value

            with contextlib.suppress(curses.error):
                if self.meta:
                    curses.meta(True)
                else:
                    curses.meta(False)

    @property
    def nodelay(self):
        """
        The nodelay option causes getch to be a non-blocking call.
        If no input is ready, getch returns ERR. If disabled (bf is FALSE), getch waits until a key is pressed.

        While interpreting an input escape sequence, wgetch sets a timer while waiting for the next character.
        If notimeout(win, TRUE) is called, then wgetch does not set a timer. The purpose of the timeout is to
        differentiate between sequences received from a function key and those typed by a user.

        :return: The ``nodelay`` property value
        :rtype: bool or None
        """
        return self.__nodelay

    @nodelay.setter
    def nodelay(self, value=None):
        """
        Set the ``nodelay`` property.

         * If ``True``: self.stdscr.nodelay(True)
         * If ``False``: self.stdscr.nodelay(False)
         * If ``None``: self.stdscr.nodelay(False)

        :param value: accept ``True``, ``False``, ``None`` as value
        :type value: bool or None
        """
        if value is not None and not isinstance(value, bool):
            raise ValueError('"nodelay" must be a bool value or None')

        if self.nodelay != value:
            self.__nodelay = value
            with contextlib.suppress(curses.error):
                if self.nodelay:
                    self.stdscr.nodelay(True)
                else:
                    self.stdscr.nodelay(False)

    @property
    def raw(self) -> bool:
        """
        The ``raw`` property place the terminal into or out of raw mode.

        Raw mode is similar to cbreak mode, in that characters typed are immediately passed through to the user program.
        The differences are that in raw mode, the interrupt, quit, suspend, and flow control characters are all
        passed through uninterpreted, instead of generating a signal.

        The behavior of the BREAK key depends on other bits in the tty driver that are not set by curses.

        :return: The ``property`` value
        :rtype: bool or None
        """
        return self.__raw

    @raw.setter
    def raw(self, value: bool or None = None):
        """
        Set the raw property.

         * If ``True``: curses.raw()
         * If ``False``: curses.noraw()
         * If ``None``: curses.noraw()

        :param value: accept ``True``, ``False``, ``None`` as value
        :type value: bool or None
        """

        if value is not None and not isinstance(value, bool):
            raise ValueError('"raw" must be a bool value or None')

        if self.raw != value:
            self.__raw = value

            with contextlib.suppress(curses.error):
                if self.raw:
                    curses.raw()
                else:
                    curses.noraw()
                    self.cbreak = True  # pragma: no cover

    @property
    def qiflush(self):
        """
        When (qiflush is False) normal flush of input and output queues associated with the
        INTR, QUIT and SUSP characters will not be done [see termio(7)].

        When (qiflush is True) is called, the queues will be flushed when these control characters are read.

        You may want use (qiflush is False) in a signal handler if you want output to continue as though the interrupt
        had not occurred, after the handler exits.

        :getter: The ``qiflush`` property value
        :rtype: bool or None
        """
        return self.__qiflush

    @qiflush.setter
    def qiflush(self, value=None):
        """
        Set the qiflush property.

         * If ``True``: curses.qiflush()
         * If ``False``: curses.noqiflush()
         * If ``None``: curses.noqiflush()

        :param value: accept ``True``, ``False``, ``None`` as value
        :type value: bool or None
        """

        if value is not None and not isinstance(value, bool):
            raise ValueError('"qiflush" must be a bool value or None')

        if self.qiflush != value:
            self.__qiflush = value

            with contextlib.suppress(curses.error):
                if self.qiflush:
                    curses.qiflush()
                else:
                    curses.noqiflush()

    @property
    def timeout(self):
        """
        The timeout and wtimeout routines set blocking or non-blocking read for a given window.

        If delay is negative, blocking read is used (i.e., waits indefinitely for input).

        If delay is zero, then non-blocking read is used (i.e., read returns ERR if no input is waiting).

        If delay is positive, then read blocks for delay milliseconds, and returns ERR if there is still no input.

        Hence, these routines provide the same functionality as nodelay, plus the additional capability of
        being able to block for only delay milliseconds (where delay is positive).

        :return:
        """
        return self.__timeout

    @timeout.setter
    def timeout(self, value: int or None = None):
        """
        Set the ``timeout`` property

        If ``delay`` is negative, blocking read is used (i.e., waits indefinitely for input).

        If ``delay`` is zero, then non-blocking read is used (i.e., read returns ERR if no input is waiting).

        If ``delay`` is positive, then read blocks for delay milliseconds, and returns ERR if there is still no input.

        :param value: negative , zero, positive int or None for 0
        :type value: int or None
        :raise TypeError: When value is not an int instance
        """
        if value is None:
            value = 0

        if not isinstance(value, int):
            raise TypeError('"delay" must be a int type')

        if self.timeout != value:
            self.__timeout = value
            with contextlib.suppress(curses.error):
                self.stdscr.timeout(self.timeout)

    # Function
    def close(self):
        """
        A Application must be close properly for permit to Curses to clean up everything and get back the tty \
        in startup condition

        Generally that is follow  by a sys.exit(0) for generate a exit code.
        """
        with contextlib.suppress(curses.error):
            curses.curs_set(1)
        self.keypad = False
        self.echo = True
        self.cbreak = False
        with contextlib.suppress(curses.error):
            curses.endwin()

    def refresh(self):
        # Update the screen return by init
        self.stdscr.refresh()

        # Update the hardware console screen
        # curses.doupdate()

    @staticmethod
    def check_terminal(force_xterm=False):
        term_value = os.environ.get("TERM")

        if not term_value:
            raise EnvironmentError("'TERM' variable is not set")
        if term_value == "xterm":
            # activate 256 colors
            term_value += "-256color"
            os.environ["TERM"] = term_value
        # xdisplay = os.environ.get('DISPLAY')

        return (
            force_xterm
            or "xterm" in term_value
            or "xterm-256color" in term_value
            or "xterm-color" in term_value
            or "konsole" in term_value
            or "rxvt" in term_value
            or "Eterm" in term_value
            or "dtterm" in term_value
            or "screen" in term_value
            and os.environ.get("DISPLAY")
        )
