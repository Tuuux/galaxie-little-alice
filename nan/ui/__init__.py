class WrapperCmdLineArgParser:
    def __init__(self, parser):
        """Init decorator with an argparse parser to be used in parsing cmd-line options"""
        self.parser = parser

    def __call__(self, func):
        """Decorate 'func' to parse 'line' and pass options to decorated function"""
        if not self.parser:
            self.parser = func(None, None, None, True)

        def wrapped_function(*args):
            line = args[1].split()
            try:
                parsed = self.parser.parse_args(line)
            except SystemExit:
                return None
            return func(*args, parsed)

        return wrapped_function
