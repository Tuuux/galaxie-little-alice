import sys
import contextlib

from nan.ui.nativetypes import Point_t


@contextlib.contextmanager
def captured_output(stream_name):
    """
    Return a context manager used by captured_stdout and captured_stdin
    that temporarily replaces the sys stream *stream_name* with a StringIO.

    """
    from io import StringIO

    orig_stdout = getattr(sys, stream_name)
    setattr(sys, stream_name, StringIO())
    try:
        yield getattr(sys, stream_name)  # noqa: DOC402
    finally:
        setattr(sys, stream_name, orig_stdout)


def captured_stdout():
    """
    Capture the output of sys.stdout:

    with captured_stdout() as s: print "hello"

    self.assertEqual(s.getvalue(), "hello")

    """
    return captured_output("stdout")


def captured_stderr():
    return captured_output("stderr")


def captured_stdin():
    return captured_output("stdin")


class Pager:
    def __init__(self, **kwargs):
        self.n_rows = kwargs.get("n_rows", 0)
        self.n_cols = kwargs.get("n_cols", 0)
        self.row = kwargs.get("row", 0)
        self.col = kwargs.get("col", 0)
        self.cursor = kwargs.get("cursor", Cursor())
        self.buffer = kwargs.get("buffer", Buffer())

    @property
    def bottom(self):
        return self.row + self.n_rows - 1

    def up(self):
        if self.cursor.pos.y == self.row - 1 and self.row > 0:
            self.row -= 1

    def down(self):
        if self.cursor.pos.y == self.bottom + 1 and self.bottom < self.buffer.bottom:
            self.row += 1

    def translate(self):
        return self.cursor.pos.y - self.row, self.cursor.pos.x - self.col

    def horizontal_scroll(self, left_margin=5, right_margin=2):
        n_pages = self.cursor.pos.x // (self.n_cols - right_margin)
        self.col = max(n_pages * self.n_cols - right_margin - left_margin, 0)

    def clear(self):
        self.row = 0
        self.col = 0


class Buffer:
    def __init__(self, **kwargs):
        self.lines = kwargs.get("lines", [""])
        self.history_length = kwargs.get("history_length", 1000)
        self.cursor = kwargs.get("cursor", None)

    def __len__(self):
        return len(self.lines)

    def __getitem__(self, index):
        return self.lines[index]

    @property
    def bottom(self):
        return len(self) - 1

    def append(self, item):
        for line in str(item).splitlines():
            self.lines.append(line)

    def insert(self, string):
        row, col = self.cursor.pos.y, self.cursor.pos.x
        current = self.lines.pop(row)
        new = current[:col] + string + current[col:]
        self.lines.insert(row, new)

    def split(self):
        row, col = self.cursor.pos.y, self.cursor.pos.x
        current = self.lines.pop(self.cursor.pos.y)
        self.lines.insert(self.cursor.pos.y, current[:col])
        self.lines.insert(row + 1, current[col:])

    def delete(self):
        row, col = self.cursor.pos.y, self.cursor.pos.x
        if (row, col) < (self.bottom, len(self[row])):
            current = self.lines.pop(row)
            if col < len(self[row]):
                self.lines.insert(row, current[:col] + current[col + 1 :])
            else:
                self.lines.insert(row, current + self.lines.pop(row))

    def clear(self):
        self.lines = [""]


class Cursor:
    def __init__(self, row=0, col=0, col_hint=None, **kwargs):
        self.pos = Point_t(x=col, y=row)
        self._col_hint = self.pos.x if col_hint is None else col_hint
        self.buffer = kwargs.get("buffer", Buffer())

    @property
    def col(self):
        return self.pos.x

    @col.setter
    def col(self, col):
        self.pos.x = col
        self._col_hint = col

    def up(self):
        if self.pos.y > 0:
            self.pos.y -= 1
            self._clamp_col()

    def down(self):
        if self.pos.y < self.buffer.bottom:
            self.pos.y += 1
            self._clamp_col()

    def left(self):
        if self.col > 0:
            self.col -= 1
        elif self.pos.y > 0:
            self.pos.y -= 1
            self.col = len(self.buffer[self.pos.y])

    def right(self):
        if self.pos.x < len(self.buffer[self.pos.y]):
            self.col += 1
        elif self.pos.y < self.buffer.bottom:
            self.pos.y += 1
            self.col = 0

    def _clamp_col(self):
        self.pos.x = min(self._col_hint, len(self.buffer[self.pos.y]))

    def clear(self):
        self.pos = Point_t(x=0, y=0)
        self._col_hint = self.pos.x


class TTY:
    def __init__(self):
        self.pager = Pager()
        self.buffer = Buffer()
        self.cursor = Cursor()

        self.buffer.cursor = self.cursor
        self.pager.cursor = self.cursor
        self.cursor.buffer = self.buffer
        self.pager.buffer = self.buffer

    def up(self):
        self.cursor.up()
        self.pager.up()
        self.pager.horizontal_scroll()

    def down(self):
        self.cursor.down()
        self.pager.down()
        self.pager.horizontal_scroll()

    def right(self):
        self.cursor.right()
        self.pager.down()
        self.pager.horizontal_scroll()

    def left(self):
        self.cursor.left()
        self.pager.up()
        self.pager.horizontal_scroll()

    def insert(self, text):
        for char in text:
            if char == "\n":
                self.buffer.append("\n")
                self.down()
            else:
                self.buffer.insert(char)
                self.cursor.right()

    def bottom(self):
        for _ in range(self.pager.row, self.buffer.bottom):
            self.down()

    def top(self):
        for _ in range(self.pager.row + self.pager.n_rows, 0, -1):
            self.up()

    def clear(self):
        self.buffer.clear()
        self.pager.clear()
        self.cursor.clear()
