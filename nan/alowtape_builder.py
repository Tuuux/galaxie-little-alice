import sys
import os
import asyncio
import logging

from attrs import define
from argparse import ArgumentParser
from argparse import ZERO_OR_MORE
from flask import Flask

from nan.core.tasks import TaskManager
from nan.ui.ncmd import NanUICmd
from nan.ui import WrapperCmdLineArgParser
from nan.api.alowtape import AlowTapeBuilder


# alowtape
parser_alowtape = ArgumentParser(
    prog="alowtape",
    description="A alowtape utility to manipulate alowtape file",
    add_help=True,
)
parser_alowtape.add_argument(
    "command", nargs="?", help="optional commands or file to run, if no commands given, enter an interactive shell"
)
parser_alowtape.add_argument(
    "command_args", nargs="...", help="if commands is not a file use optional arguments for commands"
)

# create
parser_create = ArgumentParser(
    prog="create",
    description="create a new alowtape",
    add_help=False,
)
parser_create.add_argument(
    "--meta",
    dest="META",
    default="ALOWTAPE-INFO",
    help="The meta file it content alowtape information's, default is ./ALOWTAPE-INFO",
)
parser_create.add_argument("ALOWTAPE", help="a alowtape file")
parser_create.add_argument("FILE", nargs=ZERO_OR_MORE, help="files or directories")

# destroy
parser_destroy = ArgumentParser(
    prog="destroy",
    description="delete an existing or unexisting alowtape file",
    add_help=False,
)
parser_destroy.add_argument("ALOWTAPE", help="existing or unexisting alowtape file path")

# add
parser_add = ArgumentParser(
    prog="add",
    description="add a file to the alowtape",
    add_help=False,
)
parser_add.add_argument("ALOWTAPE", help="a tar archive")
parser_add.add_argument("FILE", nargs=ZERO_OR_MORE, help="files or directories")

# remove
parser_remove = ArgumentParser(
    prog="remove",
    description="remove a file from a alowtape",
    add_help=False,
)
parser_remove.add_argument("ALOWTAPE", help="a tar archive")
parser_remove.add_argument("FILE", nargs=ZERO_OR_MORE, help="files or directories")


class AppAlowTapeBuilder(NanUICmd, AlowTapeBuilder):
    def __init__(self, **kwargs):
        super().__init__()
        self.storage = kwargs.get("storage", StorageAlowTapeBuilder())
        # Default context
        self.storage.app = kwargs.get("app", Flask(__name__))
        self.storage.app.app_context().push()

    @property
    def prompt(self):
        return f"{self.rc}> " if self.rc else "> "

    def preloop(self):
        self.interactive = True

    def emptyline(self):
        pass

    def default(self, line):
        self.print_output(f"{self.storage.application_name.lower()}: {line} unknow command\n")
        self.rc = 127

    async def __async_cmdloop(self):
        await self.cmdloop()

    def start(self):
        self.loop = asyncio.new_event_loop()
        asyncio.set_event_loop(self.loop)
        self.tasks = TaskManager(self.loop)

        self.tasks.add(self.__async_cmdloop(), "NanUI Commander getch()")

        # start the asyncio loop
        try:
            self.loop.run_forever()
        except KeyboardInterrupt:
            pass
        except EOFError:
            pass
        finally:
            self.bus.clear()
            self.tasks.cancel()
            self.loop.stop()
            string_to_log = f"{self.__class__.__name__}: is close"
            logging.debug(string_to_log)

    # Do Commands

    @WrapperCmdLineArgParser(parser_add)
    def do_add(self, _, parsed):
        """
        Add a entry inside a AlowTape
        """
        response = self.add(alowtape=parsed.ALOWTAPE, sources=parsed.FILE)

        if response[1] == 200:
            self.rc = 0
            for added in response[0].json["added"]:
                self.print_output(f"{added}\n")
        else:
            self.rc = 1
            self.print_error(f"{self.storage.application_name.lower()}: {response[0].json['message']}\n")

    def help_add(self):
        parser_add.print_help()

    @WrapperCmdLineArgParser(parser_destroy)
    def do_destroy(self, _, parsed):
        """
        Delete a alowtape file is that a alowtape file
        """
        response = self.destroy(alowtape=parsed.ALOWTAPE)

        if response[1] == 200:
            self.rec = 0
        else:
            self.rec = 1
            self.print_error(f"{self.storage.application_name.lower()}: {response[0].json['message']}")

    def help_destroy(self):
        parser_destroy.print_help()

    @WrapperCmdLineArgParser(parser_create)
    def do_create(self, _, parsed):
        """
        Create a AlowTape
        """
        response = self.create(meta=parsed.META, alowtape=parsed.ALOWTAPE, sources=parsed.FILE)

        if response[1] == 200:
            self.rc = 0
        else:
            self.rc = 1
            self.print_error(f"{self.storage.application_name.lower()}: {response[0].json['message']}")

    def help_create(self):
        parser_create.print_help()

    @WrapperCmdLineArgParser(parser_remove)
    def do_remove(self, _, parsed):
        """
        Remove a entry inside a AlowTape
        """
        response = self.remove(alowtape=parsed.ALOWTAPE, sources=parsed.FILE)

        if response[1] == 200:
            self.rc = 0
            for removed_entry in response[0].json["files_removed"]:
                self.print_output(f"{removed_entry}\n")
        else:
            self.rc = 1
            self.print_error(f"{self.storage.application_name.lower()}: {response[0].json['message']}\n")

    def help_remove(self):
        parser_remove.print_help()


@define
class StorageAlowTapeBuilder:
    app: AppAlowTapeBuilder = None
    debug: bool = True
    process: bool = False
    up: bool = False
    application_license: str = "License WTFPL v2"
    application_name: str = "AlowTape"
    application_version: str = "0.1a"


def main():
    if len(sys.argv) > 1:
        args = parser_alowtape.parse_args(sys.argv[1:])
        # if args.help:
        #     parser_alowtape.print_help()
        #     return 0

        if os.path.isfile(args.command):
            try:
                with open(args.command) as rc_file:
                    exit_code = 0
                    for line in rc_file.readlines():
                        line = line.rstrip()
                        if len(line) > 0 and line[0] != "#":
                            exit_code = AppAlowTapeBuilder().onecmdhooks("%s" % line)
                        if exit_code != 0:
                            break
                    return exit_code
            except IOError:
                return 1
            else:
                if not os.isatty():
                    sys.stdout.write("Process are not connected to a terminal (TTY)")
                return 1

        else:
            return AppAlowTapeBuilder().onecmdhooks("%s %s" % (args.command, " ".join(args.command_args)))

    else:
        return AppAlowTapeBuilder().start()


if __name__ == "__main__":
    sys.exit(main())
