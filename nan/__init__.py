#!/usr/bin/env python
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie LittleAlice Team, all rights reserved

# from glxalice.Config import Config

APPLICATION_AUTHORS = ["Tuuuux", "Mo"]
APPLICATION_NAME = "Galaxie Little Alice"
APPLICATION_COPYRIGHT = "2015-2024 - Galaxie Little Alice Team all right reserved"

__version__ = "0.1.1"
